import axios from 'axios';

import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router';
import UpdateProfile from '../../components/updateprofile/updateprofile'
import RouteConfig from '../../routeconfig'



interface ISTATE {
    LoginUserDetails: any,
    date: any
}

type Props = Partial<{
    location: any,
    params: any
}>

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const PAYSLIPAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/payslip` });

PAYSLIPAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class UpdateProfileCon extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            LoginUserDetails: undefined,
            date: undefined
        }
    }

    public ChangeRouteToAttendance = (value: any) => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")

        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
        browserHistory.push({
            pathname: `/employees/${userid}/attendance/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })

    }

    public componentDidMount() {
        this.setState({
            date: this.props.params.date ? this.props.params.date : undefined
        })
        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
                        })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const employeeID: any = this.props.params.employeeId
                    EmployeeDetailAPI.get(employeeID).then((success2: any) => {
                        const user = {
                            employeeName: success2.data[0],
                            name: success.data.body.name,
                            role: success.data.role,
                        }

                        if (this.props.params.date && this.props.params.employeeId) {
                            PAYSLIPAPI.get(`/${this.props.params.employeeId}/${this.props.params.date}`).then((success3: any) => {
                                user.employeeName.salary = success3.data[0].currentMonth.currentSalary
                                user.employeeName.bonus = success3.data[0].currentMonth.totalBonus
                                
                                this.setState({
                                    LoginUserDetails: user,
                                })

                            }).catch((error: any) => {
                                if (error.response) {
                                    if (error.response.data.message === 'No record found') {
                                        user.employeeName.salary = 0
                                        this.setState({
                                            LoginUserDetails: user,
                                        })
                                    }
                                }
                            })
                        }
                        else {

                            this.setState({
                                LoginUserDetails: user
                            })
                        }
                    })
                        .catch((err: any) => {

                            // log(err.response, 'att error')
                        })

                }
                else if (success.data.role === 'employee') {
                    browserHistory.replace('/')
                    localStorage.clear()
                    sessionStorage.clear()
                }
            }).catch((err: any) => {
                if (err.response.status === 403) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            })
        }
    }

    public render() {
        return (
            <UpdateProfile RoleProp={this.state.LoginUserDetails}
                ChangeRouteToAttendanceComponentFunc={this.ChangeRouteToAttendance}
                dateProp={this.state.date}
            />
        );
    }
}

export default UpdateProfileCon;
