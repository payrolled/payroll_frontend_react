import axios from 'axios';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';

import EmployeeDetailComponent from '../../components/employeeDetail/employeeDetails'
import RouteConfig from '../../routeconfig'




type Props = Partial<{
    location: any,
    params: any,
    RoleProp: any
}>
const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

const ChangeProfilePic: any = axios.create({ baseURL: `${RouteConfig}/api/users/employee/` });

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });


const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

ChangeProfilePic.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

interface ISTATE {
    employeeData: any,
    initialvalue: any,
    year: any,
    LoginUserDetails: any,
    MonthName: any,
}
class EmployeeDetailContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            LoginUserDetails: undefined,
            MonthName: undefined,
            employeeData: undefined,
            initialvalue: undefined,
            year: undefined,

        }
    }

    public componentDidMount() {
        const date: any = moment().get('month');
        const year: any = moment().get('year');

        switch (date) {
            case 0:
                this.setState({
                    MonthName: 'JAN',
                    initialvalue: 0,
                    year,
                })
                break;
            case 1:
                this.setState({

                    MonthName: 'FEB',
                    initialvalue: 1,
                    year
                })
                break;
            case 2:
                this.setState({

                    MonthName: 'MAR',
                    initialvalue: 2,
                    year
                })
                break;

            case 3:

                this.setState({

                    MonthName: 'APR',
                    initialvalue: 3,
                    year
                })
                break;

            case 4:
                this.setState({
                    MonthName: 'MAY',
                    initialvalue: 4,
                    year
                })
                break;

            case 5:
                this.setState({
                    MonthName: 'JUNE',
                    initialvalue: 5,
                    year
                })
                break;
            case 6:
                this.setState({

                    MonthName: 'JULY',
                    initialvalue: 6,
                    year
                })
                break;

            case 7:
                this.setState({
                    MonthName: 'AUG',
                    initialvalue: 7,
                    year
                })
                break;

            case 8:
                this.setState({
                    MonthName: 'SEP',
                    initialvalue: 8,
                    year
                })
                break;

            case 9:
                this.setState({
                    MonthName: 'OCT',
                    initialvalue: 9,
                    year
                })
                break;
            case 10:
                this.setState({
                    MonthName: 'NOV',
                    initialvalue: 10,
                    year
                })
                break;

            case 11:
                this.setState({
                    MonthName: 'DEC',
                    initialvalue: 11,
                    year
                })
                break;
            default:
                break;
        }





        if (this.props.location.state) {
            if (this.props.location.state.role === 'employee') {
                MyProfileAPI.get('me').then((success: any) => {

                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user

                    this.setState({
                        employeeData: EmpDetailArray
                    })

                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }
                })
            }
            else if (this.props.location.state.role === 'company') {
                const employeeID: any = this.props.params.employeeId;


                EmployeeDetailAPI.get(employeeID).then((success2: any) => {
                    debug.log(success2.data,'====>')
                    this.setState({
                        employeeData: success2.data,
                    })
                })
                    .catch((err: any) => {
                        if (err.response) {
                            if (err.response.status === 401) {
                                browserHistory.replace('/')
                            }
                        }
                    })
            }
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const employeeID: any = this.props.params.employeeId;
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    EmployeeDetailAPI.get(employeeID).then((success2: any) => {
                        this.setState({
                            LoginUserDetails: user,
                            employeeData: success2.data,
                        })
                    })
                        .catch((err: any) => {
                            if (err.response) {
                                if (err.response.status === 401) {
                                    browserHistory.replace('/')
                                }
                            }
                        })
                }
                else if (success.data.role === 'employee') {
                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user
                    debug.log(EmpDetailArray, 'asdasd')
                    const user = {
                        imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user,
                        employeeData: EmpDetailArray

                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
    }

    public ChangeRouteToAttendance = () => {
        const CurrentMonthFormat = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.state.LoginUserDetails
        const userid = this.props.params.employeeId;
        if (userid) {

            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/employees/${userid}/attendance/${CurrentMonthFormat}`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/attendance/${CurrentMonthFormat}`,
                state: AttendanceRouteObject
            })
        }
    }


    public ChangeRouteToPayslip = (value: any) => {
        const CurrentMonthFormat = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")

        const userid = this.props.params.employeeId;

        const AttendanceRouteObject = this.state.LoginUserDetails
        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/employees/${userid}/payslip`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/payslip`,
                state: AttendanceRouteObject
            })
        }

    }

    public changeRouteToEditP = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        AttendanceRouteObject.employeeName = this.state.employeeData[0]

        browserHistory.push({
            pathname: `/employees/${userid}/editprofile`,
            state: AttendanceRouteObject
        })
    }


    public DecDateFuncContainer = () => {
        let updatedDate: any = this.state.initialvalue - 1;
        let year = this.state.year
        if (updatedDate >= 0) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            updatedDate = 11
            year = year - 1
            this.setState({
                MonthName: 'DEC',
                initialvalue: 11,
                year: this.state.year - 1,
            })
        }


    }
    public IncDateFuncContainer = () => {

        const updatedDate: any = this.state.initialvalue + 1;

        if (updatedDate <= 11) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({
                MonthName: 'JAN',
                initialvalue: 0,
                year: this.state.year + 1
            })
        }
    }
    public TabChDash = () => {
        const CurrentMonthFormat = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")

        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/employees/${userid}/dashboard`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/home`,
                state: AttendanceRouteObject
            })
        }
    }
    public setImage = (e: React.ChangeEvent<any>) => {
        const reader = new FileReader();
        const empObject = this.state.employeeData ? this.state.employeeData : null
        reader.onload = (e2: any) => {
            empObject[0].imageUrl = e2.target.result
            debug.log('done')
            const imageObj = {
                imagePath: e2.target.result
            }
            ChangeProfilePic.put('/changepicture', imageObj).then((success: any) => {
                this.setState({ employeeData: empObject });
            }).catch((err: any) => {
                if (err.response) {
                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        };
        if (e.target.files[0] === undefined) {
            this.setState({
                employeeData: this.state.employeeData ? this.state.employeeData[0].imageUrl : null
            })
        }
        else {
            reader.readAsDataURL(e.target.files[0]);
        }

        debug.log(this.state.employeeData.imageUrl, 'asdsd')
    }
    public RemovePhoto = () => {
        ChangeProfilePic.put('/removepicture').then((success: any) => {
            const empObject = this.state.employeeData ? this.state.employeeData : null
            delete empObject[0].imageUrl
            this.setState({ employeeData: empObject });
        }).catch((err: any) => {
            if (err.response) {
                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })


    }
    public render() {
        return (
            <EmployeeDetailComponent employeeDataProp={this.state.employeeData}
                RoleProp={this.state.LoginUserDetails}
                ChangeRouteToAttendanceComponentFunc={this.ChangeRouteToAttendance}
                ChangeRouteToPayslipComponentFunc={this.ChangeRouteToPayslip}
                changeRouteToEditP={this.changeRouteToEditP}
                Month={this.state.MonthName}
                Year={this.state.year}
                IncDateFunc={this.IncDateFuncContainer}
                DecDateFunc={this.DecDateFuncContainer}
                TabChDash={this.TabChDash}
                setImageProp={this.setImage}
                RemovePhoto={this.RemovePhoto}

            />
        );
    }
}

export default EmployeeDetailContainer;