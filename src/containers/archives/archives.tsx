import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'
import Archives from '../../components/archives/archives'
import RouteConfig from '../../routeconfig'
const EmployeesRenderAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/employeesByCompany` });

const ArchiveAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/employee/restore` });



const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });


ArchiveAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeesRenderAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

type Props = Partial<{
    location: any
}>
interface ISTATE {
    EmployeeStateArray: any,
    LoginUserDetails: any,
    InviteBtnDisProp: any,
    archiveLoading: any
}
class ArchivesCon extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = {
            EmployeeStateArray: undefined,
            InviteBtnDisProp: false,
            LoginUserDetails: undefined,
            archiveLoading: false
        }
    }
    public componentDidMount() {
        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {

                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'company') {

                    const user = {
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 403) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }


        EmployeesRenderAPI.get().then((success: any) => {
            const unsortArray = success.data.filter((value: any) => {
                return value.archived === true;
            });
            unsortArray.sort((a: any, b: any) => {
                const nameA = a.name.toLowerCase()
                const nameB = b.name.toLowerCase()
                if (nameA < nameB) {

                    return -1
                }
                if (nameA > nameB) {
                    return 1
                }
                return 0
            })

            this.setState({
                EmployeeStateArray: unsortArray
            })
        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })

    }
    public changeRToArch = () => {
        browserHistory.push({
            pathname: '/archives'
        })
    }
    public ArchiveFunc = (id: any, InviteIndex: any) => {
        const ArrayCopy = this.state.EmployeeStateArray;
        const EmpId = id;
        const Index = InviteIndex;

        ArrayCopy[Index].archiveLoading = true


        this.setState({
            EmployeeStateArray: ArrayCopy,
            InviteBtnDisProp: true,
        })

        ArchiveAPI.put(`/${EmpId}`).then((success: any) => {
            ArrayCopy.splice(Index, 1)
        }).then((success2: any) => {
            this.setState({
                EmployeeStateArray: ArrayCopy,
                InviteBtnDisProp: false,
            })
        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })

    }
    public render() {
        return (

            <Archives EmployeeArray={this.state.EmployeeStateArray} RoleProp={this.state.LoginUserDetails}
                changeRToArch={this.changeRToArch}
                ArchiveFunc={this.ArchiveFunc}
                archiveLoading={this.state.archiveLoading}
                InviteBtnDisProp={this.state.InviteBtnDisProp}
            />
        );
    }
}

export default ArchivesCon;
