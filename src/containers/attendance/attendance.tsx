import axios from 'axios'
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import Attendance from '../../components/attendance/attendance'
import RouteConfig from '../../routeconfig'
const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const TakeAttendance: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/all/` });
const TakeRangeAttendance: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/range/` });
const Config = { isProd: false }; // example config in your application
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');
TakeRangeAttendance.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

TakeAttendance.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

type Props = Partial<{
    location: any
}>
interface ISTATE {
    AttBtnDisState: any,
    AttErrTextFlagState: any,
    AttErrTextState: any,
    DateChangeState: any,
    DateChangeState2: any,
    LoginUserDetails: any,
    doubleAtt: any,
    toggText: any,
    showPerLoa: any
    percentage: any
}
class AttendanceCon extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = {
            AttBtnDisState: true,
            AttErrTextFlagState: false,
            AttErrTextState: undefined,
            DateChangeState: null,
            DateChangeState2: null,
            LoginUserDetails: undefined,
            doubleAtt: true,
            percentage: 0,
            showPerLoa: false,
            toggText: 'Multiple'
        }
    }

    public componentDidMount() {
        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'company') {

                    const user = {
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })

        }
    }

    public DateChangeConFunc = (event: any, date: any) => {
        this.setState({
            AttBtnDisState: false,
            AttErrTextFlagState: false,
            AttErrTextState: undefined,
            DateChangeState: date
        })
    }
    public DateChangeConFunc2 = (event: any, date: any) => {
        this.setState({
            AttBtnDisState: false,
            AttErrTextFlagState: false,
            AttErrTextState: undefined,
            DateChangeState2: date
        })
    }

    public FinalAddAttFunc = () => {
        if (this.state.doubleAtt) {
            const date1 = moment(new Date(this.state.DateChangeState)).format("YYYY-MM-DD")
            const date2 = moment(new Date(this.state.DateChangeState2)).format("YYYY-MM-DD")
            debug.log(date1, date2, ' ok ok ok ')
            if (this.state.DateChangeState && this.state.DateChangeState2) {
                this.setState({
                    AttBtnDisState: true,
                    AttErrTextFlagState: false,
                    AttErrTextState: undefined,
                    percentage: 0,
                    showPerLoa: true
                })
                const consume = (reader: any) => {
                    return new Promise((resolve: any, reject: any) => {
                        const pump = () => {
                            reader.read().then(({ done, value }: { done: any, value: any }) => {
                                debug.log(done, '===done')
                                if (done) {
                                    setTimeout(() => {
                                        this.setState({
                                            AttBtnDisState: false,
                                            AttErrTextFlagState: true,
                                            AttErrTextState: "Attendances have been taken on these dates",
                                            percentage: 0,
                                            showPerLoa: false,
                                        })
                                        resolve()
                                        return
                                    }, 2000)
                                } else {
                                    const stream = new TextDecoder('utf-8').decode(value);
                                    const data = JSON.parse(stream);
                                    const percentage = Math.ceil(((data.count / data.total) * 100));
                                    this.setState({
                                        percentage
                                    })
                                    debug.log(percentage, 'debug.log perce')
                                    debug.log(JSON.parse(stream), '=======asdlasjdl')
                                    pump()
                                }
                            }).catch(reject)
                        }
                        pump()
                    }).then((data) => {
                        debug.log(data, '===data')
                    })
                }

                fetch(`${RouteConfig}/api/attendances/range/${date1}/${date2}`, {
                    headers: new Headers({
                        'Authorization': 'Bearer ' + localStorage.getItem('user'),
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }),
                    method: 'post',
                }).then((response) => {
                    if (response.status === 403) {
                        this.setState({
                            AttBtnDisState: false,
                            AttErrTextFlagState: true,
                            AttErrTextState: response.statusText,
                            percentage: 0,
                            showPerLoa: false
                        })
                    } else {
                        const res: any = response.body;
                        consume(res.getReader()).then((data) => {
                            debug.log(data, 'data===llpasdasdj')
                        })
                    }
                })



                // TakeRangeAttendance({
                //     method: 'POST',
                //     onDownloadProgress: (progressEvent: any) => {
                //         debug.log(progressEvent, 'onDownloadProgress-------')
                //         const percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
                //         debug.log(percentCompleted, 'percentCompleted')
                //         // Do whatever you want with the native progress event
                //     },
                //     responseType: 'stream', // default,
                //     transformResponse: [(data: any) => {
                //         // Do whatever you want to transform the data
                //         debug.log('transformResponse: ', data)
                //         return data;
                //     }],
                //     url: `${date1}/${date2}`,
                // }).then((success: any) => {


                //     this.setState({
                //         AttBtnDisState: false,
                //         AttErrTextFlagState: true,
                //         AttErrTextState: "Attendances have been taken on these dates",
                //     })
                // }).catch((err: any) => {
                //     debug.log(err, 'er--ppp');
                //     debug.log(err.response, 'er--ppp');
                //     if (err.response) {
                //         if (err.response.status === 403) {
                //             if (err.response.data.error === "Attendance can't be made on the provided date.") {
                //                 this.setState({
                //                     AttBtnDisState: false,
                //                     AttErrTextFlagState: true,
                //                     AttErrTextState: "Attendance can't be made on the provided date.",
                //                 })
                //             }
                //         }
                //         else if (err.response.status === 400) {
                //             if (err.response.data.error === "Attendance cant be made on the provided date.") {
                //                 this.setState({
                //                     AttBtnDisState: false,
                //                     AttErrTextFlagState: true,
                //                     AttErrTextState: "Attendance can't be made on the provided date.",
                //                 })
                //             }
                //         }
                //         else if (err.response.status === 500) {
                //             this.setState({
                //                 AttBtnDisState: false,
                //                 AttErrTextFlagState: true,
                //                 AttErrTextState: "Network Error",
                //             })
                //         }
                //     }
                //     else if (err.response === undefined) {
                //         this.setState({
                //             AttBtnDisState: false,
                //             AttErrTextFlagState: true,
                //             AttErrTextState: "Network Error",
                //         })
                //     }
                // })
            }
            else {
                this.setState({
                    AttBtnDisState: false,
                    AttErrTextFlagState: true,
                    AttErrTextState: "Please provide complete date ranges",
                })
            }
        }
        else {
            if (this.state.DateChangeState) {
                this.setState({
                    AttBtnDisState: true,
                    AttErrTextFlagState: false,
                    AttErrTextState: undefined,
                })
                const FinalDate = moment(new Date(this.state.DateChangeState)).format("YYYY-MM-DD")
                TakeAttendance.post(FinalDate).then((success: any) => {
                    this.setState({
                        AttBtnDisState: false,
                        AttErrTextFlagState: true,
                        AttErrTextState: "Attendance has been taken on this date",
                    })
                }).catch((err: any) => {
                    if (err.response) {
                        if (err.response.status === 403) {
                            if (err.response.data.error === "Attendance can't be made on the provided date.") {
                                this.setState({
                                    AttBtnDisState: false,
                                    AttErrTextFlagState: true,
                                    AttErrTextState: "Attendance can't be made on the provided date.",
                                })
                            }
                        }
                        else if (err.response.status === 400) {
                            if (err.response.data.error === "Attendance cant be made on the provided date.") {
                                this.setState({
                                    AttBtnDisState: false,
                                    AttErrTextFlagState: true,
                                    AttErrTextState: "Attendance can't be made on the provided date.",
                                })
                            }
                        }
                        else if (err.response.status === 500) {
                            this.setState({
                                AttBtnDisState: false,
                                AttErrTextFlagState: true,
                                AttErrTextState: "Network Error",
                            })
                        }
                    }
                    else if (err.response === undefined) {
                        this.setState({
                            AttBtnDisState: false,
                            AttErrTextFlagState: true,
                            AttErrTextState: "Network Error",
                        })
                    }
                })
            }
        }
    }
    public ToggAttFunc = () => {
        if (this.state.doubleAtt) {
            this.setState({
                AttErrTextFlagState: false,
                AttErrTextState: undefined,
                doubleAtt: false,
                toggText: 'Single'
            })
        }
        else {
            this.setState({
                AttErrTextFlagState: false,
                AttErrTextState: undefined,
                doubleAtt: true,
                toggText: 'Multiple'
            })
        }
    }
    public render() {
        return (
            <Attendance RoleProp={this.state.LoginUserDetails}
                DateChangeFunc={this.DateChangeConFunc}
                DateChangeValue={this.state.DateChangeState}
                FinalAddAttFunc={this.FinalAddAttFunc}
                AttBtnDisProp={this.state.AttBtnDisState}
                AttErrTextFlag={this.state.AttErrTextFlagState}
                AttErrText={this.state.AttErrTextState}
                DateChangeFunc2={this.DateChangeConFunc2}
                DateChangeValue2={this.state.DateChangeState2}
                doubleAtt={this.state.doubleAtt}
                ToggAttFunc={this.ToggAttFunc}
                toggText={this.state.toggText}
                percentage={this.state.percentage}
                showPerLoa={this.state.showPerLoa}
            />
        );
    }
}

export default AttendanceCon;
