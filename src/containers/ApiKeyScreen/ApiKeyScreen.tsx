
import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'
import ApiKeyScreen from '../../components/ApiKeyScreen/ApiKeyScreen'
import RouteConfig from '../../routeconfig'


interface ISTATE {
    apikeyvalue: any,
    errordisplayState: any,
    errorVisibilityState: any,
    CardMainDivOpacityState: any,
    LoaderDisplayState: any
}
type Props = Partial<{}>
const ProviderAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users`});

ProviderAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class ApiKeyScreenContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            CardMainDivOpacityState: '1',
            LoaderDisplayState: 'hidden',
            apikeyvalue: undefined,
            errorVisibilityState: 'hidden',
            errordisplayState: 'default',
        }
    }
    public apikeySateFunc = (value: any) => {
        this.setState({
            apikeyvalue: value.target.value
        })
    }

    public apikeysubmitbuttonStateFunc = () => {
        this.setState({

            CardMainDivOpacityState: '1',
            LoaderDisplayState: 'hidden',
            errorVisibilityState: 'hidden',
        })
        const apikey: any = this.state.apikeyvalue

        if (apikey) {
            this.setState({
                CardMainDivOpacityState: '0.5',
                LoaderDisplayState: 'unset',
            })
            ProviderAPI.put('/apikey', { apikey })
                .then((success2: any) => {
                    setTimeout(() => {

                        this.setState({
                            CardMainDivOpacityState: '1',
                            LoaderDisplayState: 'hidden',
                        })
                        browserHistory.push('/employees')
                    }, 3000)
                })
                .catch((error: any) => {
                    if (error.response) {
                        if (error.response.status === 403) {

                            setTimeout(() => {
                                this.setState({
                                    CardMainDivOpacityState: '1',
                                    LoaderDisplayState: 'hidden',
                                    errorVisibilityState: 'unset',
                                    errordisplayState: `* ${error.response.data.message}`,
                                })
                            }, 3000)
                        }
                        else if (error.response.status === 401) {
                            browserHistory.replace('/register')
                        }
                    }
                    else {
                        if (!error.status) {
                            setTimeout(() => {
                                this.setState({
                                    CardMainDivOpacityState: '1',
                                    LoaderDisplayState: 'hidden',
                                    errorVisibilityState: 'unset',
                                    errordisplayState: '* Network Error',
                                })
                            }, 3000)
                        }
                    }

                })
        }
        else {
            this.setState({
                CardMainDivOpacityState: '1',
                LoaderDisplayState: 'hidden',
                errorVisibilityState: 'unset',
                errordisplayState: '* Please provide api key',
            })
        }
    }
    public render() {
        return (
            <ApiKeyScreen apikeyPropsFunc={this.apikeySateFunc} apikeysubmitbuttonProp={this.apikeysubmitbuttonStateFunc} LoaderDisplayProp={this.state.LoaderDisplayState} CarMainDivOpacityProp={this.state.CardMainDivOpacityState} errordisplayProp={this.state.errordisplayState} errorVisibilityProp={this.state.errorVisibilityState} />
        );
    }
}

export default ApiKeyScreenContainer;
