import axios from 'axios'
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';

import DashboardComponent from '../../components/dashboard/dashboard'
import RouteConfig from '../../routeconfig'
const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });





const GetSingleEmployeeSalaryAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/dashboard/` });
const GetAllEmployeeSalaryAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/salaries/` });
const SalariesGraphApi: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/` });

const SalariesGraphApiForEmployee: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/employee/` });




const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');


SalariesGraphApiForEmployee.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

SalariesGraphApi.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


GetAllEmployeeSalaryAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


GetSingleEmployeeSalaryAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


interface ISTATE {
    initialvalue: any,
    year: any,
    EmpAttStatus: any,
    DashboardSalary: any,
    LoginUserDetails: any,
    MonthName: any,
    MonthDetailObject: any,
    display: any,
    display2: any,
    display3: any,
    visibility: any,
    completed: any,
    SalariesComMonth: any,
    SalariesData: any,
    SalariesGraphComparisonYearCon: any,
    SalariesComYear: any,
    SalariesComInitialvalue: any,
}
type Props = Partial<{
    location: any
}>

class DashboardContainer extends React.Component<Props, ISTATE>  {
    constructor(props: any) {

        super(props)
        this.state = {
            DashboardSalary: undefined,
            EmpAttStatus: undefined,
            LoginUserDetails: undefined,
            MonthDetailObject: undefined,
            MonthName: undefined,
            SalariesComInitialvalue: undefined,
            SalariesComMonth: undefined,
            SalariesComYear: undefined,
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: undefined,
            completed: 0,
            display: 'unset',
            display2: 'unset',
            display3: 'unset',
            initialvalue: undefined,
            visibility: 'hidden',
            year: undefined,

        }
    }

    public componentDidMount() {
        const MomentCurrentMonthFormat: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")

        const date: any = moment(MomentCurrentMonthFormat).get('month');
        const year: any = moment(MomentCurrentMonthFormat).get('year');
        const month = date + 1

        this.setState({
            EmpAttStatus: MomentCurrentMonthFormat,
            SalariesGraphComparisonYearCon: year
        })

        switch (date) {
            case 0:
                this.setState({
                    MonthName: 'JAN',
                    SalariesComInitialvalue: 0,
                    SalariesComMonth: 'JAN',
                    SalariesComYear: year,
                    initialvalue: 0,
                    year,
                })
                break;
            case 1:
                this.setState({

                    MonthName: 'FEB',
                    SalariesComInitialvalue: 1,

                    SalariesComMonth: 'FEB',
                    SalariesComYear: year,
                    initialvalue: 1,

                    year
                })
                break;
            case 2:
                this.setState({

                    MonthName: 'MAR',
                    SalariesComInitialvalue: 2,

                    SalariesComMonth: 'MAR',
                    SalariesComYear: year,

                    initialvalue: 2,

                    year
                })
                break;

            case 3:

                this.setState({

                    MonthName: 'APR',
                    SalariesComInitialvalue: 3,

                    SalariesComMonth: 'APR',
                    SalariesComYear: year,
                    initialvalue: 3,

                    year
                })
                break;

            case 4:
                this.setState({

                    MonthName: 'MAY',
                    SalariesComInitialvalue: 4,

                    SalariesComMonth: 'MAY',
                    SalariesComYear: year,
                    initialvalue: 4,

                    year
                })
                break;

            case 5:
                this.setState({

                    MonthName: 'JUNE',
                    SalariesComInitialvalue: 5,

                    SalariesComMonth: 'JUNE',
                    SalariesComYear: year,
                    initialvalue: 5,

                    year
                })
                break;
            case 6:
                this.setState({

                    MonthName: 'JULY',
                    SalariesComInitialvalue: 6,

                    SalariesComMonth: 'JULY',
                    SalariesComYear: year,
                    initialvalue: 6,
                    year
                })
                break;

            case 7:
                this.setState({

                    MonthName: 'AUG',
                    SalariesComInitialvalue: 7,

                    SalariesComMonth: 'AUG',
                    SalariesComYear: year,
                    initialvalue: 7,

                    year
                })
                break;

            case 8:
                this.setState({

                    MonthName: 'SEP',
                    SalariesComInitialvalue: 8,

                    SalariesComMonth: 'SEP',
                    SalariesComYear: year,
                    initialvalue: 8,

                    year
                })
                break;

            case 9:
                this.setState({

                    MonthName: 'OCT',
                    SalariesComInitialvalue: 9,

                    SalariesComMonth: 'OCT',
                    SalariesComYear: year,
                    initialvalue: 9,

                    year
                })
                break;
            case 10:
                this.setState({

                    MonthName: 'NOV',
                    SalariesComInitialvalue: 10,

                    SalariesComMonth: 'NOV',
                    SalariesComYear: year,
                    initialvalue: 10,

                    year
                })
                break;

            case 11:
                this.setState({
                    MonthName: 'DEC',
                    SalariesComInitialvalue: 11,

                    SalariesComMonth: 'DEC',
                    SalariesComYear: year,
                    initialvalue: 11,

                    year
                })
                break;
            default:
                break;
        }


        if (this.props.location.state) {
            if (this.props.location.state.role === 'employee') {
                GetSingleEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    debug.log(success, 'test')
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })


                SalariesGraphApiForEmployee.get(`/chart/${year.toString()}`).then((success: any) => {
                    const DateForGraph: any = [];

                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    debug.log(DateForGraph, 'ok ok')

                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    debug.log(err.response, 'xzczx')

                })


            }

            else if (this.props.location.state.role === 'company') {
                GetAllEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    debug.log(success.data, 'asdasd')
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })


                SalariesGraphApi.get(`/chart/${year.toString()}`).then((success: any) => {
                    const DateForGraph: any = [];

                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    debug.log(DateForGraph, 'ok ok')

                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    // debug.log(err.response,'xzczx')

                })
            }


            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'employee') {

                    const user = {
                        imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).then((success2: any) => {
                if (this.state.LoginUserDetails.role === 'employee') {
                    GetSingleEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                        debug.log(success, 'test')
                        this.setState({
                            DashboardSalary: success.data,
                            display: 'hidden',
                            display2: 'hidden',
                            display3: 'hidden',
                            visibility: 'unset'
                        })
                    }).catch((err: any) => {
                        if (err.response) {

                            if (err.response.data.message === 'This month attendance has no record') {
                                this.setState({
                                    DashboardSalary: undefined,
                                    display: 'hidden',
                                    display2: 'hidden',
                                    display3: 'hidden',
                                    visibility: 'unset'
                                })
                            }
                        }
                    })

                    SalariesGraphApiForEmployee.get(`/chart/${year.toString()}`).then((success: any) => {
                        const DateForGraph: any = [];
    
                        success.data.map((value: any, index: any) => {
                            const MonthObject = {
                                PKR: value,
                                name: moment().month(index).format('MMMM').slice(0, 3)
                            }
                            DateForGraph.push(MonthObject)
                        })
                        debug.log(DateForGraph, 'ok ok')
    
                        this.setState({
                            SalariesData: DateForGraph
                        })
                    }).catch((err: any) => {
                        debug.log(err.response, 'xzczx')
    
                    })
                }
                else if (this.state.LoginUserDetails.role === 'company') {
                    GetAllEmployeeSalaryAPI.get().then((success: any) => {
                        this.setState({
                            DashboardSalary: success.data,
                            display: 'hidden',
                            display2: 'hidden',
                            display3: 'hidden',
                            visibility: 'unset'
                        })
                    }).catch((err: any) => {
                        if (err.response) {

                            if (err.response.data.message === 'This month attendance has no record') {
                                this.setState({
                                    DashboardSalary: undefined,
                                    display: 'hidden',
                                    display2: 'hidden',
                                    display3: 'hidden',
                                    visibility: 'unset'
                                })
                            }
                        }
                    })





                    SalariesGraphApi.get(`/chart/${year.toString()}`).then((success: any) => {
                        const DateForGraph: any = [];
                        success.data.map((value: any, index: any) => {
                            const MonthObject = {
                                PKR: value,
                                name: moment().month(index).format('MMMM').slice(0, 3)
                            }
                            DateForGraph.push(MonthObject)
                        })
                        this.setState({
                            SalariesData: DateForGraph
                        })
                    }).catch((err: any) => {
                        // debug.log(err.response,'xzczx')

                    })

                }
            })
                .catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 403) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }
                })
        }



    }


    public DecDateFuncContainer = () => {
        this.setState({
            display: 'unset',
        })
        let updatedDate: any = this.state.initialvalue - 1;
        let year = this.state.year
        if (updatedDate >= 0) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            updatedDate = 11
            year = year - 1
            this.setState({
                MonthName: 'DEC',
                initialvalue: 11,
                year: this.state.year - 1,
            })
        }

        if (this.props.location.state) {
            if (this.props.location.state.role === 'employee') {

                const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(-1, 'months').format('YYYY-MM-DD')
                this.setState({
                    EmpAttStatus: momentIncDate,
                })

                const month = updatedDate + 1

                GetSingleEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })

            }
            else if (this.props.location.state.role === 'company') {
                const month = updatedDate + 1

                GetAllEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
        }
        else {
            if (this.state.LoginUserDetails.role === 'employee') {
                const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(-1, 'months').format('YYYY-MM-DD')
                this.setState({
                    EmpAttStatus: momentIncDate,
                })
                GetSingleEmployeeSalaryAPI.get(`${year}-${updatedDate + 1}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
            else if (this.state.LoginUserDetails.role === 'company') {
                GetAllEmployeeSalaryAPI.get(`${year}-${updatedDate + 1}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
        }

    }
    public IncDateFuncContainer = () => {
        this.setState({
            display: 'unset',
        })

        const updatedDate: any = this.state.initialvalue + 1;
        const year = this.state.year

        if (updatedDate <= 11) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({
                MonthName: 'JAN',
                initialvalue: 0,
                year: this.state.year + 1
            })
        }

        if (this.props.location.state) {
            if (this.props.location.state.role === 'employee') {
                const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(+1, 'months').format('YYYY-MM-DD')

                this.setState({
                    EmpAttStatus: momentIncDate,
                })

                const month = updatedDate + 1

                GetSingleEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })

            }
            else if (this.props.location.state.role === 'company') {
                const month = updatedDate + 1

                GetAllEmployeeSalaryAPI.get(`${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
        }
        else {
            if (this.state.LoginUserDetails.role === 'employee') {
                const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(+1, 'months').format('YYYY-MM-DD')

                this.setState({
                    EmpAttStatus: momentIncDate,
                })
                GetSingleEmployeeSalaryAPI.get(`${year}-${updatedDate + 1}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
            else if (this.state.LoginUserDetails.role === 'company') {
                GetAllEmployeeSalaryAPI.get(`${year}-${updatedDate + 1}-01`).then((success: any) => {
                    this.setState({
                        DashboardSalary: success.data,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.data.message === 'This month attendance has no record') {
                            this.setState({
                                DashboardSalary: undefined,
                                display: 'hidden',
                                display2: 'hidden',
                                display3: 'hidden',
                                visibility: 'unset'
                            })
                        }
                    }
                })
            }
        }
    }
    public SalariesIncDateFuncContainer = () => {
        const updatedDate: any = this.state.SalariesComInitialvalue + 1;
        if (updatedDate <= 11) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JAN',
                    })
                    break;
                case 1:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'FEB',

                    })
                    break;
                case 2:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'MAR',
                    })
                    break;

                case 3:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'APR',

                    })
                    break;

                case 4:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'MAY',

                    })
                    break;

                case 5:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JUNE',

                    })
                    break;
                case 6:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JULY',
                    })
                    break;

                case 7:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'AUG',
                    })
                    break;

                case 8:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'SEP',

                    })
                    break;

                case 9:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'OCT',
                    })
                    break;
                case 10:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'NOV',
                    })
                    break;

                case 11:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'DEC',

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({
                SalariesComInitialvalue: 0,
                SalariesComMonth: 'JAN',
                SalariesComYear: this.state.SalariesComYear + 1,
            })
        }
    }
    public SalariesDecDateFuncContainer = () => {
        const updatedDate: any = this.state.SalariesComInitialvalue - 1;
        if (updatedDate >= 0) {
            switch (updatedDate) {
                case 0:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JAN',
                    })
                    break;
                case 1:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'FEB',

                    })
                    break;
                case 2:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'MAR',
                    })
                    break;

                case 3:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'APR',

                    })
                    break;

                case 4:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'MAY',

                    })
                    break;

                case 5:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JUNE',

                    })
                    break;
                case 6:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'JULY',
                    })
                    break;

                case 7:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'AUG',
                    })
                    break;

                case 8:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'SEP',

                    })
                    break;

                case 9:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'OCT',
                    })
                    break;
                case 10:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'NOV',
                    })
                    break;

                case 11:
                    this.setState({
                        SalariesComInitialvalue: updatedDate,
                        SalariesComMonth: 'DEC',

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({
                SalariesComInitialvalue: 11,
                SalariesComMonth: 'DEC',
                SalariesComYear: this.state.SalariesComYear - 1,
            })

        }
    }

    public SalariesGraphDecDateFuncCon = () => {
        this.setState({
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: this.state.SalariesGraphComparisonYearCon - 1
        })
        if (this.state.LoginUserDetails) {
            if (this.state.LoginUserDetails.role === 'employee') {
                SalariesGraphApiForEmployee.get(`/chart/${(this.state.SalariesGraphComparisonYearCon - 1).toString()}`).then((success: any) => {
                    const DateForGraph: any = [];
                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }

                })
            }
            else {
                SalariesGraphApi.get(`/chart/${(this.state.SalariesGraphComparisonYearCon - 1).toString()}`).then((success: any) => {
                    const DateForGraph: any = [];
                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }

                })
            }
        }
    }

    public SalariesGraphIncDateFuncCon = () => {
        this.setState({
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: this.state.SalariesGraphComparisonYearCon + 1,
        })
        if (this.state.LoginUserDetails) {
            if (this.state.LoginUserDetails.role === 'employee') {
                SalariesGraphApiForEmployee.get(`/chart/${(this.state.SalariesGraphComparisonYearCon + 1).toString()}`).then((success: any) => {
                    const DateForGraph: any = [];
                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    debug.log(
                        err.response
                    )
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }

                })
            }
            else {
                SalariesGraphApi.get(`/chart/${(this.state.SalariesGraphComparisonYearCon + 1).toString()}`).then((success: any) => {
                    const DateForGraph: any = [];
                    success.data.map((value: any, index: any) => {
                        const MonthObject = {
                            PKR: value,
                            name: moment().month(index).format('MMMM').slice(0, 3)
                        }
                        DateForGraph.push(MonthObject)
                    })
                    this.setState({
                        SalariesData: DateForGraph
                    })
                }).catch((err: any) => {
                    debug.log(
                        err.response
                    )
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }

                })

            }
        }
    }

    public chngRtToPayEmp = () => {
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = this.state.EmpAttStatus
        }
        browserHistory.push({
            pathname: `/payslip`,
            state: AttendanceRouteObject
        })
    }
    public chngRtToProfileEmp = () => {
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = this.state.EmpAttStatus
        }
        browserHistory.push({
            pathname: `/profile`,
            state: AttendanceRouteObject
        })
    }
    public chngRtToAttEmp=()=>{
        const CurrentMonthFormat = moment(this.state.EmpAttStatus).startOf('month').format('YYYY-MM-DD')
        
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
        }
debug.log(AttendanceRouteObject)
        browserHistory.push({
            pathname: `/attendance/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public render() {
        return (
            <DashboardComponent
                DecDateFunc={this.DecDateFuncContainer}
                IncDateFunc={this.IncDateFuncContainer}
                Year={this.state.year} 
                Month={this.state.MonthName}
                MonthDetailObject={this.state.DashboardSalary} 
                Loader={this.state.display}
                Loader2={this.state.display2} 
                Loader3={this.state.display3}
                SalaryDivDisplay={this.state.visibility} 
                LinerProps={this.state.completed}
                SalariesDecDateFunc={this.SalariesDecDateFuncContainer} 
                SalariesIncDateFunc={this.SalariesIncDateFuncContainer}
                SalariesComparisonMonth={this.state.SalariesComMonth}
                SalariesComparisonYear={this.state.SalariesComYear} 
                SalariesGraphIncDateFunc={this.SalariesGraphIncDateFuncCon}
                SalariesGraphDecDateFunc={this.SalariesGraphDecDateFuncCon} 
                SalariesGraphComparisonYear={this.state.SalariesGraphComparisonYearCon}
                RoleProp={this.state.LoginUserDetails}
                Salariesdata={this.state.SalariesData}
                chngRtToPayEmp={this.chngRtToPayEmp}
                chngRtToProfileEmp={this.chngRtToProfileEmp}
                chngRtToAttEmp={this.chngRtToAttEmp}
            />
        );
    }
}

export default DashboardContainer;
