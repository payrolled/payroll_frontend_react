import axios from 'axios';
import { saveAs } from 'file-saver'
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router';
import { Debugger } from 'ts-debug';

import SettingsComponent from '../../components/settings/settings'
import RouteConfig from '../../routeconfig'


const Config = { isProd: false }; // example config in youog(r application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');


interface ISTATE {
    LoginUserDetails: any,
    MonthName: any
    MonthValueInNo: any,
    NextDisableState: any,
    PreviousDisableState: any,
    SettingLoaderState: any,
    SettingEmptyTable: any,
    momentIncrementValue: any,
    settingsArray: any,
    settingsArrayTotal: any,
    dowData: any,
    year: any,
}

type Props = Partial<{
    location: any,
    params: any,
    routeParams: any

}>

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const DownloadAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/getpayslip/` });




const SettingsAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/settings` });

const SettingsTotalAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/totals` });



const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

SettingsTotalAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


DownloadAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

SettingsAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class SettingsContainer extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            LoginUserDetails: undefined,
            MonthName: '',
            MonthValueInNo: '',
            NextDisableState: true,
            PreviousDisableState: false,
            SettingEmptyTable: false,
            SettingLoaderState: true,
            dowData: undefined,
            momentIncrementValue: 0,
            settingsArray: undefined,
            settingsArrayTotal: undefined,
            year: '',
        }
    }
    public DecrementMonthContainerFunc = () => {
        this.setState({
            NextDisableState: true,
            PreviousDisableState: true,
            SettingEmptyTable: false,
            SettingLoaderState: true,

        })
        const IncMonthValue: any = this.state.MonthValueInNo - 1;
        const momentCurrentDate: any = moment().format();
        const momentIncValue: any = this.state.momentIncrementValue - 1;
        const momentIncDate: any = moment(momentCurrentDate).add(momentIncValue, 'months').toDate()
        const MomentCurrentMonthFormat: any = moment(momentIncDate).startOf('month').format("YYYY-MM-DD")



        const momentIncDateTochangeRoute: any = moment(momentIncDate).format("YYYY-MM-DD")
        browserHistory.push({
            pathname: `/salarysheet/${momentIncDateTochangeRoute}`,
        })
        SettingsAPI.get(`/${MomentCurrentMonthFormat}`).then((success: any) => {
            const unsortArray = success.data;
            unsortArray.sort((a: any, b: any) => {
                const nameA = a.name.toLowerCase()
                const nameB = b.name.toLowerCase()
                if (nameA < nameB) {

                    return -1
                }
                if (nameA > nameB) {
                    return 1
                }
                return 0
            })



            this.setState({
                NextDisableState: false,
                PreviousDisableState: false,
                momentIncrementValue: momentIncValue,
                settingsArray: unsortArray,
            })

        }).then((success2: any) => {
            SettingsTotalAPI.get(`/${MomentCurrentMonthFormat}`).then((success3: any) => {
                this.setState({
                    SettingLoaderState: false,
                    settingsArrayTotal: success3.data,
                })
            })
        })
            .catch((error: any) => {
                if (error.response) {
                    if (error.response.data.message === 'This Month has no record') {
                        this.setState({
                            NextDisableState: false,
                            PreviousDisableState: false,
                            SettingEmptyTable: true,
                            SettingLoaderState: false,
                            momentIncrementValue: momentIncValue,
                            settingsArray: undefined,
                            settingsArrayTotal: undefined,

                        })
                    }
                }
            })

        if (IncMonthValue >= 0) {

            switch (IncMonthValue) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        MonthValueInNo: IncMonthValue,
                    })
                    break;
                case 1:
                    this.setState({

                        MonthName: 'FEB',
                        MonthValueInNo: IncMonthValue,


                    })
                    break;
                case 2:
                    this.setState({

                        MonthName: 'MAR',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 3:

                    this.setState({

                        MonthName: 'APR',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 7:
                    this.setState({
                        MonthName: 'AUG',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 8:
                    this.setState({
                        MonthName: 'SEP',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 10:
                    this.setState({
                        MonthName: 'NOV',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 11:
                    this.setState({
                        MonthName: 'DEC',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({

                MonthName: 'DEC',
                MonthValueInNo: 11,
                year: this.state.year - 1




            })
        }

        DownloadAPI.get(`${MomentCurrentMonthFormat}`).then((success: any) => {
            this.setState({ dowData: success.data })
        }).catch((err: any) => { debug.log('err') })

    }

    public IncrementMonthContainerFunc = () => {
        this.setState({
            NextDisableState: true,
            PreviousDisableState: true,
            SettingEmptyTable: false,
            SettingLoaderState: true,
        })
        const IncMonthValue: any = this.state.MonthValueInNo + 1;
        const momentCurrentDate: any = moment().format();
        const momentIncValue: any = this.state.momentIncrementValue + 1;
        const momentIncDate: any = moment(momentCurrentDate).add(momentIncValue, 'months').toDate()
        const MomentCurrentMonthFormat: any = moment(momentIncDate).startOf('month').format("YYYY-MM-DD")

        // log(MomentCurrentMonthFormat, 'muhazzib uddin ')


        const momentIncDateTochangeRoute: any = moment(momentIncDate).format("YYYY-MM-DD")

        // // log(value,"for date")
        // log(userid, 'adalkdjas')
        browserHistory.push({
            pathname: `/salarysheet/${momentIncDateTochangeRoute}`,
        })

        SettingsAPI.get(`/${MomentCurrentMonthFormat}`).then((success: any) => {
            const unsortArray = success.data;
            unsortArray.sort((a: any, b: any) => {
                const nameA = a.name.toLowerCase()
                const nameB = b.name.toLowerCase()
                if (nameA < nameB) {

                    return -1
                }
                if (nameA > nameB) {
                    return 1
                }
                return 0
            })





            this.setState({
                momentIncrementValue: momentIncValue,
                settingsArray: unsortArray,
            })




        }).then((success2: any) => {
            SettingsTotalAPI.get(`/${MomentCurrentMonthFormat}`).then((success3: any) => {
                if (momentIncValue === 0) {
                    this.setState({
                        NextDisableState: true,
                        PreviousDisableState: false
                    })
                }
                else {
                    this.setState({
                        NextDisableState: false,
                        PreviousDisableState: false
                    })
                }
                this.setState({
                    SettingLoaderState: false,
                    settingsArrayTotal: success3.data,
                })
            })
        })
            .catch((error: any) => {
                if (momentIncValue === 0) {
                    this.setState({
                        NextDisableState: true,
                        PreviousDisableState: false
                    })
                }
                else {
                    this.setState({
                        NextDisableState: false,
                        PreviousDisableState: false
                    })
                }
                if (error.response) {
                    if (error.response.data.message === 'This Month has no record') {

                        this.setState({
                            SettingEmptyTable: true,
                            SettingLoaderState: false,
                            momentIncrementValue: momentIncValue,
                            settingsArray: undefined,
                            settingsArrayTotal: undefined,
                        })
                    }
                }
            })




        if (IncMonthValue <= 11) {

            switch (IncMonthValue) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        MonthValueInNo: IncMonthValue,
                    })
                    break;
                case 1:
                    this.setState({

                        MonthName: 'FEB',
                        MonthValueInNo: IncMonthValue,


                    })
                    break;
                case 2:
                    this.setState({

                        MonthName: 'MAR',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 3:

                    this.setState({

                        MonthName: 'APR',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 7:
                    this.setState({
                        MonthName: 'AUG',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 8:
                    this.setState({
                        MonthName: 'SEP',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 10:
                    this.setState({
                        MonthName: 'NOV',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 11:
                    this.setState({
                        MonthName: 'DEC',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({

                MonthName: 'JAN',
                MonthValueInNo: 0,
                year: this.state.year + 1
            })
        }

        DownloadAPI.get(`${MomentCurrentMonthFormat}`).then((success: any) => {
            this.setState({ dowData: success.data })
        }).catch((err: any) => { debug.log('err') })
    }


    public componentDidMount() {

        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }


                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'employee') {
                    browserHistory.replace('/')
                    localStorage.clear()
                    sessionStorage.clear()
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        browserHistory.replace('/')
                    }
                }
            })
        }
        let MomentCurrentMonthFormatWithCurrentDate = this.props.params.date;
        MomentCurrentMonthFormatWithCurrentDate = moment(MomentCurrentMonthFormatWithCurrentDate).startOf('month').format("YYYY,MM,DD")
        const MomentCurrentMonthFormatWithCurrentDateFullYear = new Date(MomentCurrentMonthFormatWithCurrentDate).getFullYear()
        const MomentCurrentMonthFormatWithCurrentDateMonth = new Date(MomentCurrentMonthFormatWithCurrentDate).getMonth()
        const MomentCurrentMonthFormatWithCurrentDateDay = new Date(MomentCurrentMonthFormatWithCurrentDate).getDate()





        const MomentCurrentMonthFormatWithFirstDate = moment().startOf('month').format("YYYY,MM,DD")
        const MomentCurrentMonthFormatWithFirstDateFullYear = new Date(MomentCurrentMonthFormatWithFirstDate).getFullYear()
        const MomentCurrentMonthFormatWithFirstDateMonth = new Date(MomentCurrentMonthFormatWithFirstDate).getMonth()
        const MomentCurrentMonthFormatWithFirstDateDay = new Date(MomentCurrentMonthFormatWithFirstDate).getDate()






        const DateDifferenceElement1 = moment([MomentCurrentMonthFormatWithCurrentDateFullYear, MomentCurrentMonthFormatWithCurrentDateMonth, MomentCurrentMonthFormatWithCurrentDateDay]);
        const DateDifferenceElement2 = moment([MomentCurrentMonthFormatWithFirstDateFullYear, MomentCurrentMonthFormatWithFirstDateMonth, MomentCurrentMonthFormatWithFirstDateDay]);

        const DifferenceResult = DateDifferenceElement1.diff(DateDifferenceElement2, 'month')
        const MomentCurrentMonthFormatWithCurrentDateForAPI = moment(MomentCurrentMonthFormatWithCurrentDate).startOf('month').format("YYYY-MM-DD")

        this.setState({
            MonthValueInNo: DifferenceResult,
            momentIncrementValue: DifferenceResult,
        })


        if (DifferenceResult < 0) {
            this.setState({
                NextDisableState: false,
                PreviousDisableState: false
            })
        }
        else {
            this.setState({
                PreviousDisableState: false
            })
        }

        SettingsAPI.get(`/${MomentCurrentMonthFormatWithCurrentDateForAPI}`).then((success: any) => {
            const unsortArray = success.data;
            debug.log(unsortArray,'asdasd')
            unsortArray.sort((a: any, b: any) => {
                const nameA = a.name.toLowerCase()
                const nameB = b.name.toLowerCase()
                if (nameA < nameB) {

                    return -1
                }
                if (nameA > nameB) {
                    return 1
                }
                return 0
            })


            this.setState({
                settingsArray: unsortArray,
            })

        }).then((success2: any) => {
            SettingsTotalAPI.get(`/${MomentCurrentMonthFormatWithCurrentDateForAPI}`).then((success3: any) => {
                debug.log(success3.data,'muhazzi')
                this.setState({
                    SettingLoaderState: false,
                    settingsArrayTotal: success3.data,
                })
            })
        }).catch((error: any) => {
            if (error.response) {
                if (error.response.data.message === 'This Month has no record') {
                    this.setState({
                        NextDisableState: false,
                        PreviousDisableState: false,
                        SettingEmptyTable: true,
                        SettingLoaderState: false,
                        settingsArray: undefined
                    })
                }
            }
        })




        switch (MomentCurrentMonthFormatWithCurrentDateMonth) {
            case 0:
                this.setState({
                    MonthName: 'JAN',
                    MonthValueInNo: 0,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            case 1:
                this.setState({

                    MonthName: 'FEB',
                    MonthValueInNo: 1,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;
            case 2:
                this.setState({

                    MonthName: 'MAR',
                    MonthValueInNo: 2,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;

            case 3:

                this.setState({

                    MonthName: 'APR',
                    MonthValueInNo: 3,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;

            case 4:
                this.setState({

                    MonthName: 'MAY',
                    MonthValueInNo: 4,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;

            case 5:
                this.setState({

                    MonthName: 'JUNE',
                    MonthValueInNo: 5,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;
            case 6:
                this.setState({

                    MonthName: 'JULY',
                    MonthValueInNo: 6,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 7:
                this.setState({
                    MonthName: 'AUG',
                    MonthValueInNo: 7,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 8:
                this.setState({
                    MonthName: 'SEP',
                    MonthValueInNo: 8,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 9:
                this.setState({

                    MonthName: 'OCT',
                    MonthValueInNo: 9,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            case 10:
                this.setState({
                    MonthName: 'NOV',
                    MonthValueInNo: 10,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 11:
                this.setState({
                    MonthName: 'DEC',
                    MonthValueInNo: 11,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            default:
                break;
        }



        DownloadAPI.get(`${this.props.params.date}`).then((success: any) => {
            debug.log(success,'asdasd')
            this.setState({ dowData: success.data })
        }).catch((err: any) => { debug.log('err') })
    }
    public changeRouteToUpdateEm = (value: any) => {
        const AttendanceRouteObject = this.state.LoginUserDetails
        const Date = this.props.params.date
        AttendanceRouteObject.CurrentMonthFormat = Date;

        


        browserHistory.push({
            pathname: `/employees/${value._id}/dashboard`,

            state: AttendanceRouteObject



        })

    }

    public changeRouteToAttendanceMI = (value: any) => {
        const AttendanceRouteObject = this.state.LoginUserDetails
        AttendanceRouteObject.CurrentMonthFormat = this.props.routeParams.date
        browserHistory.push({
            pathname: `/employees/${value._id}/attendance/${this.props.routeParams.date}`,
            state: AttendanceRouteObject
        })
        debug.log(AttendanceRouteObject)
    }

    public downloadSalDet = () => {

        const blob = new Blob([this.state.dowData], { type: "csv/plain;charset=utf-8" });
        saveAs(blob, "mydata.csv");
    }
    public render() {
        return (
            <SettingsComponent RoleProp={this.state.LoginUserDetails} YearProp={this.state.year} MonthProp={this.state.MonthName} NextDisable={this.state.NextDisableState}
                PreviousDisable={this.state.PreviousDisableState} DecrementMonthPropFunc={this.DecrementMonthContainerFunc}
                IncrementMonthPropFunc={this.IncrementMonthContainerFunc} SettingsArrayProp={this.state.settingsArray}
                SettingLoaderProps={this.state.SettingLoaderState} SettingEmptyTableProp={this.state.SettingEmptyTable}
                changeRouteToUpdateEm={this.changeRouteToUpdateEm}
                settingsArrayTotalProp={this.state.settingsArrayTotal}
                changeRouteToAttendanceMI={this.changeRouteToAttendanceMI}
                downloadSalDet={this.downloadSalDet}
            />
        );
    }
}

export default SettingsContainer;
