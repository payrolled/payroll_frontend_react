
import axios from 'axios'
import * as React from 'react';

import { browserHistory } from 'react-router'
import SetPassword from '../../components/setPassword/setPassword'
import RouteConfig from '../../routeconfig'


const SetPasswordAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const TokenVerification: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });



interface ISTATE {
    email: any,
    errordisplayState: any,
    errorFlagState: any,
    erorrTextState: any,
    errorVisibilityState: any,
    password: any,
    registerDisableState: boolean,
    AgainPassword: any,
    CardMainDivOpacityState: any,
    LoaderDisplayState: any
}
type Props = Partial<{
    router: any
}>

class SetPasswordContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            AgainPassword: undefined,
            CardMainDivOpacityState: '1',
            LoaderDisplayState: 'hidden',
            email: undefined,
            erorrTextState: undefined,
            errorFlagState: undefined,
            errorVisibilityState: 'hidden',
            errordisplayState: 'default',
            password: undefined,
            registerDisableState: true,
        }
    }
    public componentDidMount() {
        const token: any = this.props.router.location.query.token
        const routename = this.props.router.location.pathname;
        if (routename === '/setpassword') {
            if (token) {
                TokenVerification.get(`verifyToken${routename}/${token}`).then((success: any) => {
                    this.setState({
                        errorFlagState: false
                    })
                }).catch((error: any) => {
                    this.setState({
                        erorrTextState: error.response.data.message,
                        errorFlagState: true,
                    })
                })

            }





        }
        else if (routename === '/forgetpassword') {
            if (token) {
                TokenVerification.get(`verifyToken${routename}/${token}`).then((success: any) => {
                    this.setState({
                        errorFlagState: false
                    })
                }).catch((error: any) => {
                    this.setState({
                        erorrTextState: error.response.data.message,
                        errorFlagState: true,
                    })
                })

            }
        }
    }

    public PasswordtextFieldFunc = (value1: any) => {
        if (this.state.AgainPassword && value1.target.value) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }
        this.setState({
            password: value1.target.value
        })
    }


    public AgainPasswordtextFieldFunc = (value1: any) => {
        if (value1.target.value && this.state.password) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }
        this.setState({
            AgainPassword: value1.target.value
        })
    }
    public SetPassword = (event:any) => {
        event.preventDefault()
        const routename = this.props.router.location.pathname;

        if (routename === '/setpassword') {


            

            const password: any = this.state.password

            if (this.state.password && this.state.AgainPassword && (this.state.password === this.state.AgainPassword)) {
                if (this.state.password.length >= 6) {

                    this.setState({
                        CardMainDivOpacityState: '0.5',
                        LoaderDisplayState: 'unset',
                        registerDisableState: true,

                    })
                    const token: any = this.props.router.location.query.token
                    SetPasswordAPI.put(`password${routename}/${token}`, { password }).then((success: any) => {
                        // browserHistory.push('/Login')
                    })
                        .then((success2: any) => {
                            setTimeout(() => {
                                this.setState({
                                    CardMainDivOpacityState: '1',
                                    LoaderDisplayState: 'hidden',
                                    registerDisableState: false,

                                })
                                browserHistory.replace({
                                    pathname: `/success`,
                                    state: token
                                }
                                )
                            }, 3000)
                        })
                        .catch((error: any) => {
                           
                            if (error.response) {
                                setTimeout(() => {
                                    this.setState({
                                        CardMainDivOpacityState: '1',
                                        LoaderDisplayState: 'hidden',
                                        errorVisibilityState: 'unset',
                                        registerDisableState: false,

                                    })
                                }, 3000)

                             


                            }
                            else {
                                if (!error.status) {
                                    setTimeout(() => {
                                        this.setState({
                                            CardMainDivOpacityState: '1',
                                            LoaderDisplayState: 'hidden',
                                            errorVisibilityState: 'unset',
                                            errordisplayState: '* Network Error',
                                            registerDisableState: false,

                                        })
                                    }, 3000)
                                }
                            }
                        })
                }
                else {
                    this.setState({
                        errorVisibilityState: 'unset',
                        errordisplayState: '* Your password must be atleast 6 characters',
                    })
                }

            }
            else {
                this.setState({
                    errorVisibilityState: 'unset',
                    errordisplayState: '* Your password does not match',
                })
            }
        }
        else if (routename === '/forgetpassword') {
            const password: any = this.state.password

            if (this.state.password && this.state.AgainPassword && (this.state.password === this.state.AgainPassword)) {
                if (this.state.password.length >= 6) {

                    this.setState({
                        CardMainDivOpacityState: '0.5',
                        LoaderDisplayState: 'unset',
                        registerDisableState: true,

                    })
                    const token: any = this.props.router.location.query.token
                    SetPasswordAPI.put(`password${routename}/${token}`, { password }).then((success: any) => {
                        // browserHistory.push('/Login')
                    })
                        .then((success2: any) => {
                            localStorage.setItem('user', token)
                            setTimeout(() => {
                                this.setState({
                                    CardMainDivOpacityState: '1',
                                    LoaderDisplayState: 'hidden',
                                    registerDisableState: false,

                                })
                                browserHistory.replace({
                                    pathname: `/success`,
                                    state: token
                                }
                                )
                            }, 3000)
                        })
                        .catch((error: any) => {
                           
                            if (error.response) {
                                setTimeout(() => {
                                    this.setState({
                                        CardMainDivOpacityState: '1',
                                        LoaderDisplayState: 'hidden',
                                        errorVisibilityState: 'unset',
                                        registerDisableState: false,

                                    })
                                }, 3000)

                            


                            }
                            else {
                                if (!error.status) {
                                    setTimeout(() => {
                                        this.setState({
                                            CardMainDivOpacityState: '1',
                                            LoaderDisplayState: 'hidden',
                                            errorVisibilityState: 'unset',
                                            errordisplayState: '* Network Error',
                                            registerDisableState: false,

                                        })
                                    }, 3000)
                                }
                            }
                        })
                }
                else {
                    this.setState({
                        errorVisibilityState: 'unset',
                        errordisplayState: '* Your password must be atleast 6 characters',
                    })
                }

            }
            else {
                this.setState({
                    errorVisibilityState: 'unset',
                    errordisplayState: '* Your password does not match',
                })
            }
        }


    }
    public ChangeRouteToLogin = () => {
        browserHistory.push('/')
    }




    public render() {
        return (
            <SetPassword PasswordValue={this.PasswordtextFieldFunc} AgainPasswordValue={this.AgainPasswordtextFieldFunc} RegisterFuncProps={this.SetPassword} changeRouteToLogin={this.ChangeRouteToLogin} RegisterDisable={this.state.registerDisableState} LoaderDisplayProp={this.state.LoaderDisplayState} CarMainDivOpacityProp={this.state.CardMainDivOpacityState} errordisplayProp={this.state.errordisplayState} errorVisibilityProp={this.state.errorVisibilityState} errorFlag={this.state.errorFlagState} errorText={this.state.erorrTextState} />
        );
    }
}

export default SetPasswordContainer;
