
import axios from 'axios'
import * as React from 'react';

import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import RegisterForm from '../../components/register/registerForm'
import RouteConfig from '../../routeconfig'

const RegisterAPI: any = axios.create({ baseURL: `${RouteConfig}/api/` });

axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

interface ISTATE {
    email: any,
    errordisplayState: any,
    errorVisibilityState: any,
    password: any,
    registerDisableState: boolean,
    AgainPassword: any,
    CardMainDivOpacityState: any,
    LoaderDisplayState: any
}
type Props = Partial<{
}>
const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

class Register extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            AgainPassword: undefined,
            CardMainDivOpacityState: '1',
            LoaderDisplayState: 'hidden',
            email: undefined,
            errorVisibilityState: 'hidden',
            errordisplayState: 'default',
            password: undefined,
            registerDisableState: true,
        }
    }



    public EmailtextFieldFunc = (value1: any) => {
        if (this.state.AgainPassword && this.state.password && value1.target.value) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }
        this.setState({
            email: value1.target.value
        })
    }

    public PasswordtextFieldFunc = (value1: any) => {
        if (this.state.AgainPassword && this.state.email && value1.target.value) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }
        this.setState({
            password: value1.target.value
        })
    }


    public AgainPasswordtextFieldFunc = (value1: any) => {
        if (value1.target.value && this.state.email && this.state.password) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }
        this.setState({
            AgainPassword: value1.target.value
        })
    }
    public RegisterFunc = (event: any) => {
        event.preventDefault()
        
        const mailformat: any = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        const credentials: any = {
            email: this.state.email,
            password: this.state.password
        }
        if (this.state.email && this.state.password && this.state.AgainPassword && (this.state.password === this.state.AgainPassword)) {
            if (this.state.email.match(mailformat)) {
                if (this.state.password.length >= 6) {

                    this.setState({
                        CardMainDivOpacityState: '0.5',
                        LoaderDisplayState: 'unset',
                        registerDisableState: true,

                    })
                    RegisterAPI.post('users/', credentials).then((success: any) => {
                        localStorage.setItem('user', success.data.token)
                    })
                        .then((success2: any) => {
                            setTimeout(() => {
                                this.setState({
                                    CardMainDivOpacityState: '1',
                                    LoaderDisplayState: 'hidden',
                                    registerDisableState: false,

                                })
                                browserHistory.push('/service')
                            }, 3000)
                        })
                        .catch((error: any) => {

                            if (error.response) {
                                debug.log(error.response, 'retest')
                                if (error.response.data.message === 'Email already registered') {
                                    setTimeout(() => {
                                        this.setState({
                                            CardMainDivOpacityState: '1',
                                            LoaderDisplayState: 'hidden',
                                            errorVisibilityState: 'unset',
                                            errordisplayState: `* Email already registered`,
                                            registerDisableState: false,
                                        })
                                    }, 3000)
                                }
                                else {

                                    setTimeout(() => {
                                        this.setState({
                                            CardMainDivOpacityState: '1',
                                            LoaderDisplayState: 'hidden',
                                            errorVisibilityState: 'unset',
                                            errordisplayState: `* ${error.response.data.errors.email.message}`,
                                            registerDisableState: false,
                                        })
                                    }, 3000)
                                }

                            }
                            else {
                                if (!error.status) {
                                    setTimeout(() => {
                                        this.setState({
                                            CardMainDivOpacityState: '1',
                                            LoaderDisplayState: 'hidden',
                                            errorVisibilityState: 'unset',
                                            errordisplayState: '* Network Error',
                                            registerDisableState: false,

                                        })
                                    }, 3000)
                                }
                            }
                        })
                }
                else {
                    this.setState({
                        errorVisibilityState: 'unset',
                        errordisplayState: '* Your password must be atleast 6 characters',
                    })
                }
            }
            else {
                this.setState({
                    errorVisibilityState: 'unset',
                    errordisplayState: '* Invalid email format',
                })
            }
        }
        else {
            this.setState({
                errorVisibilityState: 'unset',
                errordisplayState: '* Your password does not match',
            })
        }
    }
    public ChangeRouteToLogin = () => {
        browserHistory.push('/')
    }


    public componentDidMount() {
        if (localStorage.getItem('user')) {
            browserHistory.push('/employees')
        }
    }

    public render() {
        return (
            <RegisterForm EmailValue={this.EmailtextFieldFunc} PasswordValue={this.PasswordtextFieldFunc} AgainPasswordValue={this.AgainPasswordtextFieldFunc} RegisterFuncProps={this.RegisterFunc} changeRouteToLogin={this.ChangeRouteToLogin} RegisterDisable={this.state.registerDisableState} LoaderDisplayProp={this.state.LoaderDisplayState} CarMainDivOpacityProp={this.state.CardMainDivOpacityState} errordisplayProp={this.state.errordisplayState} errorVisibilityProp={this.state.errorVisibilityState} />
        );
    }
}

export default Register;
