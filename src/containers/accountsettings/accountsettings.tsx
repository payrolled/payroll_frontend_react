import axios from 'axios';
import * as React from 'react';
import { browserHistory } from 'react-router';
import AccountSetting from '../../components/accountsettings/accountsettings';
import RouteConfig from '../../routeconfig'

interface ISTATE {
    AccSettEmail: any,
    CurrPassCh: any,
    CnfPassCh: any,
    NewPassCh: any,
    LoginUserDetails: any,
    SaveDisState: any,
    updateSuccesstext: any,
    updateSuccess: any
}

type Props = Partial<{
    location: any,
    params: any
}>

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

const ChangePass: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });


ChangePass.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class AccountSettingCon extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            AccSettEmail: '',
            CnfPassCh: undefined,
            CurrPassCh: undefined,
            LoginUserDetails: undefined,
            NewPassCh: undefined,
            SaveDisState: true,
            updateSuccess: false,
            updateSuccesstext: undefined,
        }
    }



    public componentDidMount() {
        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state,
            })

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    this.setState({
                        AccSettEmail: success.data.email,
                    })
                }
                else if (success.data.role === 'employee') {

                    this.setState({
                        AccSettEmail: success.data.user.email,
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        AccSettEmail: success.data.email,
                        LoginUserDetails: user,
                    })
                }
                else if (success.data.role === 'employee') {
                    const user = {
                        imageUrl: success.data.user.imageUrl,
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        AccSettEmail: success.data.user.email,
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
    }


    public CurrPassCh = (value1: any) => {
        if (this.state.CnfPassCh && this.state.NewPassCh && value1.target.value) {
            this.setState({
                SaveDisState: false,
            })
        }
        else {
            this.setState({
                SaveDisState: true,
            })
        }
        this.setState({
            CurrPassCh: value1.target.value,
            updateSuccess: false,
        })
    }
    public CnfPassCh = (value1: any) => {
        if (this.state.CurrPassCh && this.state.NewPassCh && value1.target.value) {
            this.setState({
                SaveDisState: false,
            })
        }
        else {
            this.setState({
                SaveDisState: true,
            })
        }
        this.setState({
            CnfPassCh: value1.target.value,
            updateSuccess: false,
        })
    }
    public NewPassCh = (value1: any) => {
        if (this.state.CnfPassCh && this.state.CurrPassCh && value1.target.value) {
            this.setState({
                SaveDisState: false,
            })
        }
        else {
            this.setState({
                SaveDisState: true,
            })
        }
        this.setState({
            NewPassCh: value1.target.value,
            updateSuccess: false,
        })
    }

    public SaveButton = () => {
        this.setState({
            SaveDisState: true,
            updateSuccess: false,
            updateSuccesstext: undefined,
        })
        if (this.state.NewPassCh === this.state.CnfPassCh) {
            const obj = {
                currentPassword: this.state.CurrPassCh,
                newPassword: this.state.NewPassCh
            }

            ChangePass.put('password', obj).then((success: any) => {
                this.setState({
                    SaveDisState: false,
                    updateSuccess: true,
                    updateSuccesstext: "Your password has successfully been updated",
                })
            }).catch((err: any) => {

                if (err.response) {
                    if (err.response.status === 403) {
                        if (err.response.data.message === "Wrong Current Password") {
                            this.setState({
                                SaveDisState: false,
                                updateSuccess: true,
                                updateSuccesstext: "Wrong Current Password",
                            })
                        }
                    }
                    else if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
                else {
                    this.setState({
                        SaveDisState: false,
                        updateSuccess: true,
                        updateSuccesstext: "Network Error",
                    })
                }
            })
        }
        else {
            this.setState({
                SaveDisState: false,
                updateSuccess: true,
                updateSuccesstext: "* Your password does not match",
            })
        }
    }
    public render() {
        return (
            <AccountSetting RoleProp={this.state.LoginUserDetails}
                AccSettEmail={this.state.AccSettEmail}
                CurrPassCh={this.CurrPassCh}
                CnfPassCh={this.CnfPassCh}
                NewPassCh={this.NewPassCh}
                SaveDisState={this.state.SaveDisState}
                SaveButton={this.SaveButton}
                updateSuccesstext={this.state.updateSuccesstext}
                updateSuccess={this.state.updateSuccess}


            />
        );
    }
}

export default AccountSettingCon;
