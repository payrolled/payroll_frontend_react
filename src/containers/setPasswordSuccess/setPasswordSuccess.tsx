
import * as React from 'react';
import { browserHistory } from 'react-router'

import SetPasswordSuccess from '../../components/setpasswordSuccess/setpasswordSuccess'

type Props = Partial<{
    location: any
}>
class SetPasswordSuccessContainer extends React.Component<Props> {
    public componentDidMount() {
        const paraToken: any = this.props.location.state;
        if (!paraToken) {


            browserHistory.replace('/')
        }

    }
    public render() {
        return (
            <SetPasswordSuccess />
        );
    }
}

export default SetPasswordSuccessContainer;
