
import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'

import TimerTool from '../../components/timerTool/timerTool'
import RouteConfig from '../../routeconfig'


const ProviderAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users`});
ProviderAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})



type Props = Partial<{}>
interface ISTATE {
    CardMainDivOpacityState: any,
    LoaderDisplayState: any

}

class TimerToolContainer extends React.Component<Props, ISTATE>{
    constructor(props: any) {
        super(props)
        this.state = {
            CardMainDivOpacityState: '1',
            LoaderDisplayState: 'hidden',
        }
    }


    public hubstaffDivFuncContainer = () => {
        const service: any = 'hubstaff'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {

                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')
                }, 3000)

            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })

    }

    public accountsightDivFuncContainer = () => {
        const service: any = 'accountsight'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {
                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')

                }, 3000)
            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })

    }


    public todoDivFuncContainer = () => {
        const service: any = 'todo'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {
                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')

                }, 3000)
            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })
    }


    public timecampDivFuncContainer = () => {
        const service: any = 'timecamp'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {
                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')
                }, 3000)

            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })
    }


    public desktimeDivFuncContainer = () => {
        const service: any = 'desktime'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {
                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')
                }, 3000)

            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })
    }

    public togglDivFuncContainer = () => {
        const service: any = 'toggl'
        this.setState({
            CardMainDivOpacityState: '0.5',
            LoaderDisplayState: 'unset',
        })
        ProviderAPI.put('/service', { service })
            .then((success2: any) => {
                setTimeout(() => {
                    this.setState({
                        CardMainDivOpacityState: '1',
                        LoaderDisplayState: 'hidden',
                    })
                    browserHistory.push('/service/api_key')
                }, 3000)

            })
            .catch((err: any) => {
                browserHistory.replace('/register')
            })
    }
    public render() {
        return (
            <TimerTool hubstaffDivFunc={this.hubstaffDivFuncContainer} accountsightDivFunc={this.accountsightDivFuncContainer} todoDivFunc={this.todoDivFuncContainer} timecampDivFunc={this.timecampDivFuncContainer} togglDivFunc={this.togglDivFuncContainer} DesktimeDivFunc={this.desktimeDivFuncContainer} LoaderDisplayProp={this.state.LoaderDisplayState} CarMainDivOpacityProp={this.state.CardMainDivOpacityState} />
        );
    }
}

export default TimerToolContainer;
