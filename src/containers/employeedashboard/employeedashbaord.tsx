import axios from 'axios';

import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router';

import { Debugger } from 'ts-debug';

import EmployeeDashboard from '../../components/employeedashboard/employeedashboard'
import RouteConfig from '../../routeconfig'


const Config = { isProd: false }; // example config in your application/

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

interface ISTATE {
    AnnualObject: any,
    CurrMonthObject: any,
    DashboardSalary: any,
    EmpAttStatus: any,
    EmployeeObject: any,
    LoginUserDetails: any,
    SalariesData: any,
    MonthName: any,
    PayslipLoaderState: any,
    employeeData: any,
    initialvalue: any,
    statusobj: any,
    display2: any,
    display3: any,
    display: any,
    year: any,
    SalariesGraphComparisonYearCon: any,
    SalariesComInitialvalue: any,
    SalariesComMonth: any,
    SalariesComYear: any,
    UpdateAttendanceBtnDisplay: any,
    ShowModal: any,
    visibility: any,
    basicSal: any,
    bonus: any,


}

type Props = Partial<{
    location: any,
    params: any
}>

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const SingleEmployeeStatus: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/status` });

const ShowSingleEmpSalToCom: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/dashboard` });
const ShowSingleEmpGraphToCom: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/employee` });
const UpdateEmployeeAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/updateEmployee` });

UpdateEmployeeAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

ShowSingleEmpSalToCom.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

ShowSingleEmpGraphToCom.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

SingleEmployeeStatus.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

SingleEmployeeStatus.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})


EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class EmployeeDashboardCon extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            AnnualObject: undefined,
            CurrMonthObject: undefined,
            DashboardSalary: undefined,
            EmpAttStatus: undefined,
            EmployeeObject: undefined,
            LoginUserDetails: undefined,
            MonthName: undefined,
            PayslipLoaderState: true,
            SalariesComInitialvalue: undefined,
            SalariesComMonth: undefined,
            SalariesComYear: undefined,
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: undefined,
            ShowModal: false,
            UpdateAttendanceBtnDisplay: false,
            basicSal: undefined,
            bonus: undefined,
            display: 'unset',
            display2: 'unset',
            display3: 'unset',
            employeeData: undefined,
            initialvalue: undefined,
            statusobj: undefined,
            visibility: 'hidden',
            year: undefined,
        }
    }

    public ChangeRouteToAttendance = (value: any) => {
        const CurrentMonthFormat = moment(this.state.EmpAttStatus).startOf('month').format('YYYY-MM-DD')
        debug.log(CurrentMonthFormat)
        debug.log(this.state)
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
        }
        browserHistory.push({
            pathname: `/employees/${userid}/attendance/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })

    }

    public componentDidMount() {
        const MomentCurrentMonthFormat: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")
        const date: any = moment(MomentCurrentMonthFormat).get('month');
        const year: any = moment(MomentCurrentMonthFormat).get('year');
        const month = date + 1

        this.setState({
            EmpAttStatus: MomentCurrentMonthFormat,
            SalariesGraphComparisonYearCon: year,
        })
        switch (date) {
            case 0:
                this.setState({
                    MonthName: 'JAN',
                    SalariesComInitialvalue: 0,
                    SalariesComMonth: 'JAN',
                    SalariesComYear: year,
                    initialvalue: 0,
                    year,
                })
                break;
            case 1:
                this.setState({

                    MonthName: 'FEB',
                    SalariesComInitialvalue: 1,
                    SalariesComMonth: 'FEB',
                    SalariesComYear: year,
                    initialvalue: 1,

                    year
                })
                break;
            case 2:
                this.setState({

                    MonthName: 'MAR',
                    SalariesComInitialvalue: 2,

                    SalariesComMonth: 'MAR',
                    SalariesComYear: year,

                    initialvalue: 2,

                    year
                })
                break;

            case 3:

                this.setState({

                    MonthName: 'APR',
                    SalariesComInitialvalue: 3,

                    SalariesComMonth: 'APR',
                    SalariesComYear: year,
                    initialvalue: 3,

                    year
                })
                break;

            case 4:
                this.setState({

                    MonthName: 'MAY',
                    SalariesComInitialvalue: 4,

                    SalariesComMonth: 'MAY',
                    SalariesComYear: year,
                    initialvalue: 4,

                    year
                })
                break;

            case 5:
                this.setState({

                    MonthName: 'JUNE',
                    SalariesComInitialvalue: 5,

                    SalariesComMonth: 'JUNE',
                    SalariesComYear: year,
                    initialvalue: 5,

                    year
                })
                break;
            case 6:
                this.setState({

                    MonthName: 'JULY',
                    SalariesComInitialvalue: 6,

                    SalariesComMonth: 'JULY',
                    SalariesComYear: year,
                    initialvalue: 6,
                    year
                })
                break;

            case 7:
                this.setState({

                    MonthName: 'AUG',
                    SalariesComInitialvalue: 7,

                    SalariesComMonth: 'AUG',
                    SalariesComYear: year,
                    initialvalue: 7,

                    year
                })
                break;

            case 8:
                this.setState({

                    MonthName: 'SEP',
                    SalariesComInitialvalue: 8,

                    SalariesComMonth: 'SEP',
                    SalariesComYear: year,
                    initialvalue: 8,

                    year
                })
                break;

            case 9:
                this.setState({

                    MonthName: 'OCT',
                    SalariesComInitialvalue: 9,

                    SalariesComMonth: 'OCT',
                    SalariesComYear: year,
                    initialvalue: 9,

                    year
                })
                break;
            case 10:
                this.setState({

                    MonthName: 'NOV',
                    SalariesComInitialvalue: 10,

                    SalariesComMonth: 'NOV',
                    SalariesComYear: year,
                    initialvalue: 10,

                    year
                })
                break;

            case 11:
                this.setState({
                    MonthName: 'DEC',
                    SalariesComInitialvalue: 11,

                    SalariesComMonth: 'DEC',
                    SalariesComYear: year,
                    initialvalue: 11,

                    year
                })
                break;
            default:
                break;
        }
        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'company') {

                    const user = {
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
        const employeeID: any = this.props.params.employeeId

        EmployeeDetailAPI.get(employeeID).then((success: any) => {
            this.setState({
                employeeData: success.data
            })
        })
            .catch((err: any) => {

                // log(err.response, 'att error')
            })

        SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${MomentCurrentMonthFormat}`).then((success2: any) => {
            const statusobj = success2.data;
            this.setState({
                statusobj
            })

        }).catch((err2: any) => {
            if (err2.response) {
                if (err2.response.data === 'No Record Found') {
                    const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }
                    this.setState({
                        statusobj
                    })
                }


            }
        })

        ShowSingleEmpSalToCom.get(`/${this.props.params.employeeId}/${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
            debug.log(success.data, 'asdasd')
            this.setState({
                DashboardSalary: success.data,
                display: 'hidden',
                display2: 'hidden',
                display3: 'hidden',
                visibility: 'unset'
            })
        }).catch((err: any) => {
            debug.log(err.response, 'err te')
            if (err.response) {
                if (err.response.data.message === 'This month attendance has no record') {
                    this.setState({
                        DashboardSalary: undefined,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }
            }
        })

        ShowSingleEmpGraphToCom.get(`/${this.props.params.employeeId}/chart/${year.toString()}`).then((success: any) => {
            const DateForGraph: any = [];

            success.data.map((value: any, index: any) => {
                const MonthObject = {
                    PKR: value,
                    name: moment().month(index).format('MMMM').slice(0, 3)
                }
                DateForGraph.push(MonthObject)
            })
            debug.log(DateForGraph, 'ok ok')

            this.setState({
                SalariesData: DateForGraph
            })
        }).catch((err: any) => {
            // debug.log(err.response,'xzczx')

        })

    }



    public DecDateFuncContainer = () => {
        let updatedDate: any = this.state.initialvalue - 1;
        let year = this.state.year
        const month = updatedDate + 1
        const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(-1, 'months').format('YYYY-MM-DD')
        this.setState({
            EmpAttStatus: momentIncDate,
            basicSal: undefined,
            bonus: undefined,
            display: 'unset',
        })
        debug.log(momentIncDate)

        if (updatedDate >= 0) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            updatedDate = 11
            year = year - 1
            this.setState({
                MonthName: 'DEC',
                initialvalue: 11,
                year: this.state.year - 1,
            })
        }
        SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${momentIncDate}`).then((success2: any) => {
            const statusobj = success2.data;
            this.setState({
                statusobj
            })

        }).catch((err2: any) => {
            debug.log(err2.response, 'muhazzib')
            if (err2.response) {
                if (err2.response.data.message === 'No record found') {
                    const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }
                    this.setState({
                        statusobj
                    })
                }


            }
        })
        ShowSingleEmpSalToCom.get(`/${this.props.params.employeeId}/${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
            debug.log(success.data, 'asdasd')
            this.setState({
                DashboardSalary: success.data,
                display: 'hidden',
                display2: 'hidden',
                display3: 'hidden',
                visibility: 'unset'
            })
        }).catch((err: any) => {
            debug.log(err.response, 'err te')
            if (err.response) {

                if (err.response.data.message === 'This month attendance has no record') {
                    this.setState({
                        DashboardSalary: undefined,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }
            }
        })
    }

    public IncDateFuncContainer = () => {


        const updatedDate: any = this.state.initialvalue + 1;
        const month = updatedDate + 1
        const year = this.state.year
        const momentIncDate: any = moment(this.state.EmpAttStatus).startOf('month').add(+1, 'months').format('YYYY-MM-DD')
        this.setState({
            EmpAttStatus: momentIncDate,
            basicSal: undefined,
            bonus: undefined,
            display: 'unset',
        })
        if (updatedDate <= 11) {

            switch (updatedDate) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        initialvalue: updatedDate
                    })
                    break;
                case 1:
                    this.setState({
                        MonthName: 'FEB',
                        initialvalue: updatedDate

                    })
                    break;
                case 2:
                    this.setState({
                        MonthName: 'MAR',
                        initialvalue: updatedDate

                    })
                    break;

                case 3:
                    this.setState({

                        MonthName: 'APR',
                        initialvalue: updatedDate

                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        initialvalue: updatedDate

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        initialvalue: updatedDate

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        initialvalue: updatedDate

                    })
                    break;

                case 7:
                    this.setState({

                        MonthName: 'AUG',
                        initialvalue: updatedDate

                    })
                    break;

                case 8:
                    this.setState({

                        MonthName: 'SEP',
                        initialvalue: updatedDate

                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        initialvalue: updatedDate

                    })
                    break;
                case 10:
                    this.setState({

                        MonthName: 'NOV',
                        initialvalue: updatedDate

                    })
                    break;

                case 11:
                    this.setState({

                        MonthName: 'DEC',
                        initialvalue: updatedDate

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({
                MonthName: 'JAN',
                initialvalue: 0,
                year: this.state.year + 1
            })
        }


        SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${momentIncDate}`).then((success2: any) => {
            const statusobj = success2.data;
            this.setState({
                statusobj
            })

        }).catch((err2: any) => {
            if (err2.response) {
                if (err2.response.data.message === 'No record found') {
                    const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }
                    this.setState({
                        statusobj
                    })
                }


            }
        })
        ShowSingleEmpSalToCom.get(`/${this.props.params.employeeId}/${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
            debug.log(success.data, 'asdasd')
            this.setState({
                DashboardSalary: success.data,
                display: 'hidden',
                display2: 'hidden',
                display3: 'hidden',
                visibility: 'unset'
            })
        }).catch((err: any) => {
            debug.log(err.response, 'err te')
            if (err.response) {

                if (err.response.data.message === 'This month attendance has no record') {
                    this.setState({
                        DashboardSalary: undefined,
                        display: 'hidden',
                        display2: 'hidden',
                        display3: 'hidden',
                        visibility: 'unset'
                    })
                }
            }
        })
    }

    public ChangeRouteToPayslip = (value: any) => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = this.state.EmpAttStatus
        }
        browserHistory.push({
            pathname: `/employees/${userid}/payslip`,
            state: AttendanceRouteObject
        })

    }


    public changeRouteToEditP = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        AttendanceRouteObject.employeeName = this.state.employeeData[0]

        browserHistory.push({
            pathname: `/employees/${userid}/editprofile`,
            state: AttendanceRouteObject
        })
    }
    public ChRouteToEmpD = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (AttendanceRouteObject) {
            AttendanceRouteObject.CurrentMonthFormat = this.state.EmpAttStatus
        }
        browserHistory.push({
            pathname: `/employees/${userid}`,
            state: AttendanceRouteObject
        })
    }


    public SalariesGraphDecDateFuncCon = () => {
        this.setState({
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: this.state.SalariesGraphComparisonYearCon - 1,
        })
        ShowSingleEmpGraphToCom.get(`/${this.props.params.employeeId}/chart/${this.state.SalariesGraphComparisonYearCon - 1}`).then((success: any) => {
            const DateForGraph: any = [];

            success.data.map((value: any, index: any) => {
                const MonthObject = {
                    PKR: value,
                    name: moment().month(index).format('MMMM').slice(0, 3)
                }
                DateForGraph.push(MonthObject)
            })
            debug.log(DateForGraph, 'ok ok')

            this.setState({
                SalariesData: DateForGraph
            })
        }).catch((err: any) => {
            // debug.log(err.response,'xzczx')

        })
    }

    public SalariesGraphIncDateFuncCon = () => {
        this.setState({
            SalariesData: undefined,
            SalariesGraphComparisonYearCon: this.state.SalariesGraphComparisonYearCon + 1,
        })
        ShowSingleEmpGraphToCom.get(`/${this.props.params.employeeId}/chart/${this.state.SalariesGraphComparisonYearCon + 1}`).then((success: any) => {
            const DateForGraph: any = [];

            success.data.map((value: any, index: any) => {
                const MonthObject = {
                    PKR: value,
                    name: moment().month(index).format('MMMM').slice(0, 3)
                }
                DateForGraph.push(MonthObject)
            })
            debug.log(DateForGraph, 'ok ok')

            this.setState({
                SalariesData: DateForGraph
            })
        }).catch((err: any) => {
            // debug.log(err.response,'xzczx')

        })
    }

    public ModalCloseFunc = () => {
        this.setState({
            ShowModal: false,
        })
    }
    public updateSalFunc = () => {

        this.setState({
            ShowModal: true
        })
    }
    public bsal = (event: any) => {
        this.setState({
            basicSal: event.target.value
        })
    }
    public bonusprop = (event: any) => {
        this.setState({
            bonus: event.target.value
        })
    }
    public FinalUpdateFunc = () => {
        this.setState({
            UpdateAttendanceBtnDisplay: true
        })
        const DateForUpEmp = this.state.EmpAttStatus ? this.state.EmpAttStatus : undefined
        const finalObjUEmp = {
            bonus: this.state.bonus ? this.state.bonus : this.state.DashboardSalary ? this.state.DashboardSalary.totalBonus : 0,
            salary: this.state.basicSal ? this.state.basicSal : this.state.DashboardSalary ? this.state.DashboardSalary.currentSalary : 0,
        }
        debug.log(finalObjUEmp, 'obj sd')
        UpdateEmployeeAPI.put(`/${this.props.params.employeeId}/${DateForUpEmp}`, finalObjUEmp).then((success2: any) => {
            debug.log('success')
            this.setState({
                ShowModal: false,
                UpdateAttendanceBtnDisplay: false,
                display: 'unset',
                display2: 'unset',
                display3: 'unset',
                visibility: 'hidden',
            })
            const updatedDate: any = this.state.initialvalue + 1;
            const month = updatedDate;
            const year = this.state.year;
            ShowSingleEmpSalToCom.get(`/${this.props.params.employeeId}/${year}-${month < 10 ? '0' + month : month}-01`).then((success: any) => {
                debug.log(success.data, 'asdasd')
                this.setState({
                    DashboardSalary: success.data,
                    display: 'hidden',
                    display2: 'hidden',
                    display3: 'hidden',
                    visibility: 'unset'
                })
            }).catch((err: any) => {
                debug.log(err.response, 'err te')
                if (err.response) {

                    if (err.response.data.message === 'This month attendance has no record') {
                        this.setState({
                            DashboardSalary: undefined,
                            display: 'hidden',
                            display2: 'hidden',
                            display3: 'hidden',
                            visibility: 'unset'
                        })
                    }
                }
            })

            ShowSingleEmpGraphToCom.get(`/${this.props.params.employeeId}/chart/${this.state.SalariesGraphComparisonYearCon}`).then((success: any) => {
                const DateForGraph: any = [];

                success.data.map((value: any, index: any) => {
                    const MonthObject = {
                        PKR: value,
                        name: moment().month(index).format('MMMM').slice(0, 3)
                    }
                    DateForGraph.push(MonthObject)
                })
                debug.log(DateForGraph, 'ok ok')

                this.setState({
                    SalariesData: DateForGraph
                })
            }).catch((err: any) => {
                // debug.log(err.response,'xzczx')

            })

        })
            .catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
                else if (!err.response) {
                    // this.setState({
                    //     SaveDisState: false
                    // })
                }
            })

    }
    public render() {
        return (
            <EmployeeDashboard RoleProp={this.state.LoginUserDetails} ChangeRouteToAttendanceComponentFunc={this.ChangeRouteToAttendance}
                AnnualObjectProp={this.state.AnnualObject}
                CurrMonthObjectProp={this.state.CurrMonthObject}
                EmployeeObjectProp={this.state.EmployeeObject}
                PayslipLoaderProp={this.state.PayslipLoaderState}
                employeeDataProps={this.state.employeeData}
                changeRouteToEditP={this.changeRouteToEditP}
                ChRouteToEmpD={this.ChRouteToEmpD}
                Year={this.state.year}
                Month={this.state.MonthName}
                DecDateFunc={this.DecDateFuncContainer}
                IncDateFunc={this.IncDateFuncContainer}
                ChangeRouteToPayslipComponentFunc={this.ChangeRouteToPayslip}
                statusobjprop={this.state.statusobj}
                MonthDetailObject={this.state.DashboardSalary} Loader={this.state.display}
                SalariesGraphComparisonYear={this.state.SalariesGraphComparisonYearCon}
                Salariesdata={this.state.SalariesData}
                paramDate={this.state.EmpAttStatus}
                SalariesGraphDecDateFunc={this.SalariesGraphDecDateFuncCon}
                SalariesGraphIncDateFunc={this.SalariesGraphIncDateFuncCon}
                ModalCloseFunc={this.ModalCloseFunc}
                ShowModal={this.state.ShowModal}
                updateSalFunc={this.updateSalFunc}
                bonusprop={this.bonusprop}
                bsalprop={this.bsal}
                FinalUpdateFunc={this.FinalUpdateFunc}
                UpdateAttendanceBtnDisplay={this.state.UpdateAttendanceBtnDisplay}
            />
        );
    }
}

export default EmployeeDashboardCon;
