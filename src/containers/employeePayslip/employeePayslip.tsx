import axios from 'axios';

import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router';
import { Debugger } from 'ts-debug';

import EmployeePayslip from '../../components/employeePayslip/employeePayslip'
import RouteConfig from '../../routeconfig'


const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

interface ISTATE {
    AnnualObject: any,
    CurrMonthObject: any,
    EmailBtn:any,
    EmployeeObject: any,
    EmailSuccess:any,
    EmailSuccessText:any,
    LoginUserDetails: any,
    PayslipLoaderState: any,
    employeeData: any,
    paySlipCurrDate: any,
    paySlipCurrMonth: any
}

type Props = Partial<{
    location: any,
    params: any
}>

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const PAYSLIPAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/payslip` });
const EmailPayslip:any= axios.create({ baseURL: `${RouteConfig}/api/attendances/payslip` });

EmailPayslip.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

PAYSLIPAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class EmployeePayslipContainer extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            AnnualObject: undefined,
            CurrMonthObject: undefined,
            EmailBtn:false,
            EmailSuccess:false,
            EmailSuccessText:undefined,
            EmployeeObject: undefined,
            LoginUserDetails: undefined,
            PayslipLoaderState: true,
            employeeData: undefined,
            paySlipCurrDate: undefined,
            paySlipCurrMonth: undefined
        }
    }
    public TabChDash = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        debug.log(userid)
        if (userid) {
            browserHistory.push({
                pathname: `/employees/${userid}/dashboard`,
                state: AttendanceRouteObject
            })
        }
        else {
            browserHistory.push({
                pathname: `/home`,
                state: AttendanceRouteObject
            })
        }
    }

    public ChangeRouteToAttendance = (value: any) => {
        const CurrentMonthFormat = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.state.LoginUserDetails

        const userid = this.props.params.employeeId;
        if (userid) {

            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/employees/${userid}/attendance/${CurrentMonthFormat}`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat;
            }
            browserHistory.push({
                pathname: `/attendance/${CurrentMonthFormat}`,
                state: AttendanceRouteObject
            })
        }

    }

    public componentDidMount() {

        if (this.props.location.state) {
            if (this.props.location.state.role === 'company') {
                const employeeID: any = this.props.params.employeeId

                EmployeeDetailAPI.get(employeeID).then((success: any) => {
                    this.setState({
                        employeeData: success.data
                    })
                })
                    .catch((err: any) => {

                        // log(err.response, 'att error')
                    })


                const employeeIDForPayS: any = this.props.params.employeeId;
                const momentCurrentDate: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");
                const momentCurrentDateMonth: any = new Date(momentCurrentDate).getMonth()
                debug.log(momentCurrentDateMonth, 'asd')
                this.setState({
                    paySlipCurrDate: momentCurrentDate,
                    paySlipCurrMonth: momentCurrentDateMonth
                })



                PAYSLIPAPI.get(`/${employeeIDForPayS}/${momentCurrentDate}`).then((success: any) => {
                    debug.log(success.data[0], 'saasad')

                    const AnnualObject: any = {
                        noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                        paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                        totalAbsentsInYear: success.data[0].totalAbsents,
                        totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,

                    }

                    debug.log(AnnualObject, 'kalsdja')
                    const EmployeeObject: any = success.data[0].employee
                    const CurrMonthObject: any = success.data[0].currentMonth

                    this.setState({
                        AnnualObject,
                        CurrMonthObject,
                        EmployeeObject,
                        PayslipLoaderState: false,
                    })
                }).catch((error: any) => {
                    if (error.response) {
                        if (error.response.data.message === 'No record found') {
                            this.setState({
                                AnnualObject: undefined,
                                CurrMonthObject: undefined,
                                EmployeeObject: undefined,
                                PayslipLoaderState: false,
                            })
                        }
                    }
                })
            }

            else {
                MyProfileAPI.get('me').then((success: any) => {
                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user

                    this.setState({
                        employeeData: EmpDetailArray
                    })

                })
                    .catch((err: any) => {

                        // log(err.response, 'att error')
                    })

                const momentCurrentDate: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");
                const momentCurrentDateMonth: any = new Date(momentCurrentDate).getMonth()
                debug.log(momentCurrentDateMonth, 'asd')
                this.setState({
                    paySlipCurrDate: momentCurrentDate,
                    paySlipCurrMonth: momentCurrentDateMonth
                })



                PAYSLIPAPI.get(`/${momentCurrentDate}`).then((success: any) => {

                    const AnnualObject: any = {
                        noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                        paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                        totalAbsentsInYear: success.data[0].totalAbsents,
                        totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,

                    }
                    debug.log(AnnualObject, 'kalsdja')


                    const EmployeeObject: any = success.data[0].employee
                    const CurrMonthObject: any = success.data[0].currentMonth

                    this.setState({
                        AnnualObject,
                        CurrMonthObject,
                        EmployeeObject,
                        PayslipLoaderState: false,
                    })
                }).catch((error: any) => {
                    if (error.response) {
                        if (error.response.data.message === 'No record found') {
                            this.setState({
                                AnnualObject: undefined,
                                CurrMonthObject: undefined,
                                EmployeeObject: undefined,
                                PayslipLoaderState: false,
                            })
                        }
                    }
                })
            }


            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'employee') {
                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user
                    const user = {
                        imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                        name: success.data.user.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user,
                        employeeData: EmpDetailArray
                    })
                }
            }).then((success2: any) => {
                if (this.state.LoginUserDetails) {
                    if (this.state.LoginUserDetails.role === 'company') {
                        const employeeID: any = this.props.params.employeeId

                        EmployeeDetailAPI.get(employeeID).then((success: any) => {
                            this.setState({
                                employeeData: success.data
                            })
                        })
                            .catch((err: any) => {

                                // log(err.response, 'att error')
                            })


                        const employeeIDForPayS: any = this.props.params.employeeId;
                        const momentCurrentDate: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");
                        const momentCurrentDateMonth: any = new Date(momentCurrentDate).getMonth()
                        debug.log(momentCurrentDateMonth, 'asd')
                        this.setState({
                            paySlipCurrDate: momentCurrentDate,
                            paySlipCurrMonth: momentCurrentDateMonth
                        })



                        PAYSLIPAPI.get(`/${employeeIDForPayS}/${momentCurrentDate}`).then((success: any) => {

                            const AnnualObject: any = {
                                noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                                paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                                totalAbsentsInYear: success.data[0].totalAbsents,
                                totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,

                            }

                            const EmployeeObject: any = success.data[0].employee
                            const CurrMonthObject: any = success.data[0].currentMonth

                            this.setState({
                                AnnualObject,
                                CurrMonthObject,
                                EmployeeObject,
                                PayslipLoaderState: false,
                            })
                        }).catch((error: any) => {
                            if (error.response) {
                                if (error.response.data.message === 'No record found') {
                                    this.setState({
                                        AnnualObject: undefined,
                                        CurrMonthObject: undefined,
                                        EmployeeObject: undefined,
                                        PayslipLoaderState: false,
                                    })
                                }
                            }
                        })
                    }
                    else {

                        const momentCurrentDate: any = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");
                        const momentCurrentDateMonth: any = new Date(momentCurrentDate).getMonth()
                        debug.log(momentCurrentDateMonth, 'asd')
                        this.setState({
                            paySlipCurrDate: momentCurrentDate,
                            paySlipCurrMonth: momentCurrentDateMonth
                        })



                        PAYSLIPAPI.get(`/${momentCurrentDate}`).then((success: any) => {
                            const AnnualObject: any = {
                                noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                                paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                                totalAbsentsInYear: success.data[0].totalAbsents,
                                totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,
                            }
                            const EmployeeObject: any = success.data[0].employee
                            const CurrMonthObject: any = success.data[0].currentMonth

                            this.setState({
                                AnnualObject,
                                CurrMonthObject,
                                EmployeeObject,
                                PayslipLoaderState: false,
                            })
                        }).catch((error: any) => {
                            if (error.response) {
                                if (error.response.data.message === 'No record found') {
                                    this.setState({
                                        AnnualObject: undefined,
                                        CurrMonthObject: undefined,
                                        EmployeeObject: undefined,
                                        PayslipLoaderState: false,
                                    })
                                }
                            }
                        })

                    }
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }


    }
    public PayslipMonthChange = (event: any) => {
        const INCDate = this.state.paySlipCurrMonth - 1;
        this.setState({
            EmailSuccess:false,
            PayslipLoaderState: true,
            paySlipCurrMonth: INCDate,
        })

        const PayslipCurrentDate: any = this.state.paySlipCurrDate
        let UpdatedDate;
        switch (INCDate) {
            case 0:
                UpdatedDate = moment(PayslipCurrentDate).month(0).format("YYYY-MM-DD");
                break;

            case 1:
                UpdatedDate = moment(PayslipCurrentDate).month(1).format("YYYY-MM-DD");
                break;

            case 2:
                UpdatedDate = moment(PayslipCurrentDate).month(2).format("YYYY-MM-DD");
                break;

            case 3:
                UpdatedDate = moment(PayslipCurrentDate).month(3).format("YYYY-MM-DD");
                break;

            case 4:
                UpdatedDate = moment(PayslipCurrentDate).month(4).format("YYYY-MM-DD");
                break;

            case 5:
                UpdatedDate = moment(PayslipCurrentDate).month(5).format("YYYY-MM-DD");
                break;

            case 6:
                UpdatedDate = moment(PayslipCurrentDate).month(6).format("YYYY-MM-DD");
                break;

            case 7:
                UpdatedDate = moment(PayslipCurrentDate).month(7).format("YYYY-MM-DD");
                break;

            case 8:
                UpdatedDate = moment(PayslipCurrentDate).month(8).format("YYYY-MM-DD");
                break;

            case 9:
                UpdatedDate = moment(PayslipCurrentDate).month(9).format("YYYY-MM-DD");
                break;

            case 10:
                UpdatedDate = moment(PayslipCurrentDate).month(10).format("YYYY-MM-DD");
                break;

            case 11:
                UpdatedDate = moment(PayslipCurrentDate).month(11).format("YYYY-MM-DD");
                break;
        }



        const employeeIDForPayS: any = this.props.params.employeeId;


        PAYSLIPAPI.get(`/${employeeIDForPayS}/${UpdatedDate}`).then((success: any) => {

            const AnnualObject: any = {
                noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                totalAbsentsInYear: success.data[0].totalAbsents,
                totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,
            }

            const EmployeeObject: any = success.data[0].employee
            const CurrMonthObject: any = success.data[0].currentMonth

            this.setState({
                AnnualObject,
                CurrMonthObject,
                EmployeeObject,
                PayslipLoaderState: false,
            })
        }).catch((error: any) => {
            if (error.response) {
                if (error.response.data.message === 'No record found') {
                    this.setState({
                        AnnualObject: undefined,
                        CurrMonthObject: undefined,
                        EmployeeObject: undefined,
                        PayslipLoaderState: false,

                    })
                }
            }
        })

    }

    public PayslipMonthChangeInC = (event: any) => {
        const INCDate = this.state.paySlipCurrMonth + 1;
        this.setState({
            EmailSuccess:false,
            PayslipLoaderState: true,
            paySlipCurrMonth: INCDate,
        })

        const PayslipCurrentDate: any = this.state.paySlipCurrDate
        let UpdatedDate;
        switch (INCDate) {
            case 0:
                UpdatedDate = moment(PayslipCurrentDate).month(0).format("YYYY-MM-DD");
                break;

            case 1:
                UpdatedDate = moment(PayslipCurrentDate).month(1).format("YYYY-MM-DD");
                break;

            case 2:
                UpdatedDate = moment(PayslipCurrentDate).month(2).format("YYYY-MM-DD");
                break;

            case 3:
                UpdatedDate = moment(PayslipCurrentDate).month(3).format("YYYY-MM-DD");
                break;

            case 4:
                UpdatedDate = moment(PayslipCurrentDate).month(4).format("YYYY-MM-DD");
                break;

            case 5:
                UpdatedDate = moment(PayslipCurrentDate).month(5).format("YYYY-MM-DD");
                break;

            case 6:
                UpdatedDate = moment(PayslipCurrentDate).month(6).format("YYYY-MM-DD");
                break;

            case 7:
                UpdatedDate = moment(PayslipCurrentDate).month(7).format("YYYY-MM-DD");
                break;

            case 8:
                UpdatedDate = moment(PayslipCurrentDate).month(8).format("YYYY-MM-DD");
                break;

            case 9:
                UpdatedDate = moment(PayslipCurrentDate).month(9).format("YYYY-MM-DD");
                break;

            case 10:
                UpdatedDate = moment(PayslipCurrentDate).month(10).format("YYYY-MM-DD");
                break;

            case 11:
                UpdatedDate = moment(PayslipCurrentDate).month(11).format("YYYY-MM-DD");
                break;
        }



        const employeeIDForPayS: any = this.props.params.employeeId;


        PAYSLIPAPI.get(`/${employeeIDForPayS}/${UpdatedDate}`).then((success: any) => {
            const AnnualObject: any = {
                noOfPaidLeavesInYear: success.data[0].noOfPaidLeaves,
                paidLeavesAvailedInYear: success.data[0].paidLeavesAvailedInYear,
                totalAbsentsInYear: success.data[0].totalAbsents,
                totalUnpaidLeavesInYear: success.data[0].totalUnpaidLeavesInYear,
            }

            const EmployeeObject: any = success.data[0].employee
            const CurrMonthObject: any = success.data[0].currentMonth

            this.setState({
                AnnualObject,
                CurrMonthObject,
                EmployeeObject,
                PayslipLoaderState: false,
            })
        }).catch((error: any) => {
            if (error.response) {
                if (error.response.data.message === 'No record found') {
                    this.setState({
                        AnnualObject: undefined,
                        CurrMonthObject: undefined,
                        EmployeeObject: undefined,
                        PayslipLoaderState: false,
                    })
                }
            }
        })

    }

    public changeRouteToEditP = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        AttendanceRouteObject.employeeName = this.state.employeeData[0]

        browserHistory.push({
            pathname: `/employees/${userid}/editprofile`,
            state: AttendanceRouteObject
        })
    }

    public ChRouteToEmpD = () => {
        const CurrentMonthFormat = this.props.location.state !== undefined ? this.props.location.state.CurrentMonthFormat ? this.props.location.state.CurrentMonthFormat : moment().format("YYYY-MM-DD") : moment().format("YYYY-MM-DD")

        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        debug.log(AttendanceRouteObject, 'assdadasd')

        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
            }
            browserHistory.push({
                pathname: `/employees/${userid}`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
            }
            browserHistory.push({
                pathname: `/profile`,
                state: AttendanceRouteObject
            })
        }
    }
    public EmailFunc =()=>{
        const userId=this.props.params.employeeId
        const payslipDate=moment().month(this.state.paySlipCurrMonth).format("YYYY-MM-DD");
        debug.log(payslipDate,'payslipDate')
        this.setState({
            EmailBtn:true,
        })
        EmailPayslip.post(`/${userId}/${payslipDate}/send`).then((success:any)=>{
            this.setState({
                EmailBtn:false,
                EmailSuccess:true,
                EmailSuccessText:'Payslip has been sent successfully'
            })
        }).catch((err:any)=>{
            this.setState({
                EmailBtn:false,
                EmailSuccess:true,
                EmailSuccessText:'Some thing went wrong'
            })
        })
    }
    public render() {
        return (
            <EmployeePayslip RoleProp={this.state.LoginUserDetails} ChangeRouteToAttendanceComponentFunc={this.ChangeRouteToAttendance}
                currentMonProp={this.state.paySlipCurrMonth}
                AnnualObjectProp={this.state.AnnualObject}
                CurrMonthObjectProp={this.state.CurrMonthObject}
                EmployeeObjectProp={this.state.EmployeeObject}
                PayslipMonthChangeProp={this.PayslipMonthChange}
                PayslipLoaderProp={this.state.PayslipLoaderState}
                employeeDataProps={this.state.employeeData}
                changeRouteToEditP={this.changeRouteToEditP}
                ChRouteToEmpD={this.ChRouteToEmpD}
                TabChDash={this.TabChDash}
                PayslipMonthChangeInC={this.PayslipMonthChangeInC}
                EmailFuncProp={this.EmailFunc}
                EmailSuccess={this.state.EmailSuccess}
                EmailSuccessText={this.state.EmailSuccessText}
                EmailBtnProp={this.state.EmailBtn}

            />
        );
    }
}

export default EmployeePayslipContainer;
