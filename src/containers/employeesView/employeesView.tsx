import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import EmployeesViewComponent from '../../components/employeesView/employeesView'
import RouteConfig from '../../routeconfig'
const EmployeesRenderAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/employeesByCompany` });
const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const Config = { isProd: false }; // example config in your application
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');



MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeesRenderAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

type Props = Partial<{
    location: any
}>
interface ISTATE {
    EmployeeStateArray: any,
    LoginUserDetails: any
}
class EmployeesViewContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = {
            EmployeeStateArray: undefined,
            LoginUserDetails: undefined
        }
    }
    public componentDidMount() {
        if (this.props.location.state) {

            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {

                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'company') {

                    const user = {
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 403) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }


        EmployeesRenderAPI.get().then((success: any) => {
            const unsortArray = success.data.filter((value: any) => {
                return value.archived === false;
            });

            debug.log(unsortArray)
            unsortArray.sort((a: any, b: any) => {
                const nameA = a.name.toLowerCase()
                const nameB = b.name.toLowerCase()
                if (nameA < nameB) {

                    return -1
                }
                if (nameA > nameB) {
                    return 1
                }
                return 0
            })

            this.setState({
                EmployeeStateArray: unsortArray
            })
        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })

    }
    public changeRToArch = () => {
        browserHistory.push({
            pathname: '/archives',
            state: this.state.LoginUserDetails
        })
    }
    public render() {
        return (

            <EmployeesViewComponent EmployeeArray={this.state.EmployeeStateArray} RoleProp={this.state.LoginUserDetails}
                changeRToArch={this.changeRToArch}
            />
        );
    }
}

export default EmployeesViewContainer;
