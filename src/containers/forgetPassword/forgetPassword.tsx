import axios from 'axios'
import * as React from 'react';
import ForgetPassword from '../../components/forgetPassword/forgetPassword'
import RouteConfig from '../../routeconfig'



interface ISTATE {
    email: any,
    errorVisibilityState: any,
    errordisplayState: any,
    LoaderDisplayState: any,
    registerDisableState: any,
}
type Props = Partial<{}>

const ForgetPasswordAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users` });


class ForgetPasswordContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = {
            LoaderDisplayState: 'hidden',
            email: undefined,
            errorVisibilityState: 'hidden',
            errordisplayState: 'default',

            registerDisableState: true,
        }
    }
    public ForgetpassChan = (event: any) => {

        this.setState({
            email: event.target.value,
            registerDisableState: event.target.value ? false : true,
        })
    }

    public ForgetpassFunc = (event: any) => {
        event.preventDefault()
        const email = this.state.email
        if (email) {
            this.setState({
                LoaderDisplayState: 'unset',
                registerDisableState: true,

            })
            ForgetPasswordAPI.post('/forgetPassword', { email }).then((success: any) => {
                this.setState({
                    LoaderDisplayState: 'hidden',
                    errorVisibilityState: 'unset',
                    errordisplayState: "* Please check your email to reset your password",
                    registerDisableState: false,

                })
            }).catch((err: any) => {
                this.setState({
                    LoaderDisplayState: 'hidden',
                    registerDisableState: false
                })
                if (err.response) {
                    if (err.response.data.message === 'Invalid Email.') {
                        this.setState({
                            errorVisibilityState: 'unset',
                            errordisplayState: "* Please enter valid email address",
                        })
                    }
                    else if (err.response.data.message === "You don't have any password yet first set it.") {
                        this.setState({
                            errorVisibilityState: 'unset',
                            errordisplayState: "* Please set your password first",
                        })
                    }
                }

            })
        }
    }
    public render() {
        return (
            <ForgetPassword
                ForgetpassChan={this.ForgetpassChan}
                ForgetpassFunc={this.ForgetpassFunc}
                LoaderDisplayProp={this.state.LoaderDisplayState}
                RegisterDisable={this.state.registerDisableState}
                errordisplayProp={this.state.errordisplayState}
                errorVisibilityProp={this.state.errorVisibilityState}
            />
        );
    }
}

export default ForgetPasswordContainer;
