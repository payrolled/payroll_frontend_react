import axios from 'axios';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router';
import { Debugger } from 'ts-debug';
import EmployeeAttendance from '../../components/employeeAttendance/employeeAttendance'
import RouteConfig from '../../routeconfig'
const EmployeesAttendance: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/user/` });
const EmployeeDetailAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });
const UpdateAttendanceAPI: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/updateattendance` });
const SingleEmployeeAttendance: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/employee` });
const SingleEmployeeStatus: any = axios.create({ baseURL: `${RouteConfig}/api/attendances/status` });
const Config = { isProd: false };
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');
SingleEmployeeStatus.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

SingleEmployeeAttendance.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

UpdateAttendanceAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeesAttendance.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeeDetailAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

interface ISTATE {
    present: any,
    absent: any,
    arrayTobeEdited: any,
    desktimeuser: any,
    dateForNDU: any,
    paidleave: any,
    unpaidleave: any,
    halfday: any,
    vacation: any,
    attendance: any,
    differenceresultforUI: any,
    employeeData: any,
    fifthWeekArray: any,
    firstWeekArray: any,
    forthWeekArray: any,
    index: any,
    late: any,
    month: any,
    momentIncrementValue: any,
    momentSelectedDate: any,
    paramDate: any,
    secondWeekArray: any,
    statusobj: any,
    thirdWeekArray: any,
    sixthWeekArray: any,
    value: any,
    year: any,
    AddAttSuccessState: any,
    AddAttSuccessTextState: any,
    LoginUserDetails: any,
    MonthName: any,
    MonthValueInNo: any,
    NextDisableState: any,
    PreviousDisableState: any,
    ShowModal: any,
    UpdateAttenBtnDisp: any
}
type Props = Partial<{
    UpdateAttendanceCon: any,
    location: any,
    params: any,
}>



class EmployeeAttendanceContainer extends React.Component<Props, ISTATE> {

    constructor(props: any) {
        super(props)
        this.state = {
            AddAttSuccessState: false,
            AddAttSuccessTextState: undefined,
            LoginUserDetails: undefined,
            MonthName: '',
            MonthValueInNo: '',
            NextDisableState: true,
            PreviousDisableState: true,
            ShowModal: false,
            UpdateAttenBtnDisp: false,
            absent: false,
            arrayTobeEdited: undefined,
            attendance: 'abc',
            dateForNDU: undefined,
            desktimeuser: false,
            differenceresultforUI: undefined,
            employeeData: undefined,
            fifthWeekArray: [],
            firstWeekArray: [],
            forthWeekArray: [],
            halfday: false,
            index: false,
            late: false,
            momentIncrementValue: 0,
            momentSelectedDate: '',
            month: '',
            paidleave: false,
            paramDate: undefined,
            present: false,
            secondWeekArray: [],
            sixthWeekArray: [],
            statusobj: undefined,
            thirdWeekArray: [],
            unpaidleave: false,
            vacation: false,
            value: undefined,
            year: '',
        }

    }
    public changeRouteToEditP = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        delete AttendanceRouteObject.CurrentMonthFormat
        AttendanceRouteObject.employeeName = this.state.employeeData[0]

        browserHistory.push({
            pathname: `/employees/${userid}/editprofile`,
            state: AttendanceRouteObject
        })
    }

    public ChangeRouteToPayslip = (value: any) => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails


        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/employees/${userid}/payslip`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/payslip`,
                state: AttendanceRouteObject
            })
        }


    }


    public CalenderWithNoData = (momentIncDatePar: any, momentIncValuePara: any) => {
        const monthLastDay: any = moment(momentIncDatePar).endOf('month').toDate();
        const monthEndDate: any = moment(momentIncDatePar).daysInMonth();
        const monthStartDay: any = moment(momentIncDatePar).startOf('month').toDate();
        const monthStartDayName: any = moment(monthStartDay).format('dddd')
        const monthLastDayName: any = moment(monthLastDay).format('dddd')
        const APIMonthSortedArray: any = []
        const firstWeekArrayDup: any = [];
        let firstWeekArrayDup2: any = [];
        const secondWeekArrayDup2: any = [];
        const thirdWeekArrayDup2: any = [];
        const forthWeekArrayDup2: any = [];
        const fifthWeekArrayDup2: any = [];
        const sixthWeekArrayDup2: any = [];

        this.setState({
            momentIncrementValue: momentIncValuePara
        })

        if (momentIncValuePara === 0) {
            this.setState({
                NextDisableState: true,
                PreviousDisableState: false
            })
        }
        else {
            this.setState({
                NextDisableState: false,
                PreviousDisableState: false
            })
        }


        if (monthStartDayName === 'Saturday') {

            for (let i = 1; i <= 6; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }

            firstWeekArrayDup2 = firstWeekArrayDup;

        }

        else if (monthStartDayName === 'Friday') {


            for (let i = 1; i <= 5; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }

            firstWeekArrayDup2 = firstWeekArrayDup;

        }

        else if (monthStartDayName === 'Thursday') {
            for (let i = 1; i <= 4; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }


            firstWeekArrayDup2 = firstWeekArrayDup;

        }

        else if (monthStartDayName === 'Wednesday') {

            for (let i = 1; i <= 3; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }

            firstWeekArrayDup2 = firstWeekArrayDup;

        }

        else if (monthStartDayName === 'Tuesday') {

            for (let i = 1; i <= 2; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }


            firstWeekArrayDup2 = firstWeekArrayDup;


        }

        else if (monthStartDayName === 'Monday') {



            for (let i = 1; i <= 1; i++) {
                firstWeekArrayDup.push('');
                this.setState({
                    firstWeekArray: firstWeekArrayDup
                })
            }


            firstWeekArrayDup2 = firstWeekArrayDup;

        }

        for (let i = 1; i <= monthEndDate; i++) {
            if (firstWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {
                    firstWeekArrayDup2.push(x)
                }
                else {
                    firstWeekArrayDup2.push(i)
                }

                this.setState({
                    firstWeekArray: firstWeekArrayDup2,
                    sixthWeekArray: sixthWeekArrayDup2
                })
                // log(firstWeekArrayDup2, 'first week')
            }

            else if (firstWeekArrayDup2.length === 7 && secondWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {

                    secondWeekArrayDup2.push(x)
                }
                else {
                    secondWeekArrayDup2.push(i)

                }
                this.setState({
                    secondWeekArray: secondWeekArrayDup2
                })

            }

            else if (secondWeekArrayDup2.length === 7 && thirdWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {
                    thirdWeekArrayDup2.push(x)
                }
                else {
                    thirdWeekArrayDup2.push(i)

                }
                this.setState({
                    thirdWeekArray: thirdWeekArrayDup2
                })


            }

            else if (thirdWeekArrayDup2.length === 7 && forthWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {
                    forthWeekArrayDup2.push(x)
                }
                else {
                    forthWeekArrayDup2.push(i)

                }
                this.setState({
                    forthWeekArray: forthWeekArrayDup2
                })

            }
            else if (forthWeekArrayDup2.length === 7 && fifthWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {
                    fifthWeekArrayDup2.push(x)
                }
                else {
                    fifthWeekArrayDup2.push(i)

                }

                if (monthEndDate - i === 0) {
                    if (monthLastDayName === 'Monday') {
                        for (let i2 = 1; i2 <= 5; i2++) {
                            fifthWeekArrayDup2.push('');
                        }

                    }

                    else if (monthLastDayName === 'Tuesday') {
                        for (let i2 = 1; i2 <= 4; i2++) {
                            fifthWeekArrayDup2.push('');
                        }

                    }

                    else if (monthLastDayName === 'Wednesday') {
                        for (let i2 = 1; i2 <= 3; i2++) {
                            fifthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Thursday') {
                        for (let i2 = 1; i2 <= 2; i2++) {
                            fifthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Friday') {
                        for (let i2 = 1; i2 <= 1; i2++) {
                            fifthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Sunday') {
                        for (let i2 = 1; i2 <= 6; i2++) {
                            fifthWeekArrayDup2.push('');
                        }
                    }
                }
                this.setState({
                    fifthWeekArray: fifthWeekArrayDup2
                })

            }
            else if (fifthWeekArrayDup2.length === 7 && sixthWeekArrayDup2.length < 7) {
                const x: any = APIMonthSortedArray.find((obj: any) => {
                    return moment(obj.date).date() === i
                })

                if (x) {
                    sixthWeekArrayDup2.push(x)
                }
                else {
                    sixthWeekArrayDup2.push(i)

                }


                if (monthEndDate - i === 0) {
                    if (monthLastDayName === 'Monday') {
                        for (let i2 = 1; i2 <= 5; i2++) {
                            sixthWeekArrayDup2.push('');
                        }

                    }

                    else if (monthLastDayName === 'Tuesday') {
                        for (let i2 = 1; i2 <= 4; i2++) {
                            sixthWeekArrayDup2.push('');
                        }

                    }

                    else if (monthLastDayName === 'Wednesday') {
                        for (let i2 = 1; i2 <= 3; i2++) {
                            sixthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Thursday') {
                        for (let i2 = 1; i2 <= 2; i2++) {
                            sixthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Friday') {
                        for (let i2 = 1; i2 <= 1; i2++) {
                            sixthWeekArrayDup2.push('');
                        }
                    }

                    else if (monthLastDayName === 'Sunday') {
                        for (let i2 = 1; i2 <= 6; i2++) {
                            sixthWeekArrayDup2.push('');
                        }
                    }
                }
                this.setState({
                    sixthWeekArray: sixthWeekArrayDup2
                })
                // log(sixthWeekArrayDup2, 'fifth')
            }

        }
    }

    public IncrementMonthContainerFunc = () => {
        this.setState({
            NextDisableState: true,
            PreviousDisableState: true,
            fifthWeekArray: [],
            firstWeekArray: [],
            forthWeekArray: [],
            secondWeekArray: [],
            sixthWeekArray: [],
            thirdWeekArray: [],
        })
        const IncMonthValue: any = this.state.MonthValueInNo + 1;

        const momentCurrentDate: any = moment().format();
        const momentIncValue: any = this.state.momentIncrementValue + 1;
        const momentIncDate: any = moment(momentCurrentDate).add(momentIncValue, 'months').toDate()
        const MomentCurrentMonthFormat: any = moment(momentIncDate).startOf('month').format("YYYY-MM-DD")
        // log(MomentCurrentMonthFormat, 'muhazzib uddin ')


        const userid = this.props.params.employeeId;
        const momentIncDateTochangeRoute: any = moment(momentIncDate).format("YYYY-MM-DD");
        let firstWeekArrayDup2: any = [];
        const secondWeekArrayDup2: any = [];
        const thirdWeekArrayDup2: any = [];
        const forthWeekArrayDup2: any = [];
        const fifthWeekArrayDup2: any = [];
        const sixthWeekArrayDup2: any = [];
        const firstWeekArrayDup: any = [];


        // // log(value,"for date")
        // log(userid, 'adalkdjas')
        if (userid) {
            browserHistory.push({
                pathname: `/employees/${userid}/attendance/${momentIncDateTochangeRoute}`,
            })
        }
        else {
            browserHistory.push({
                pathname: `/attendance/${momentIncDateTochangeRoute}`,
            })
        }
        EmployeesAttendance.get(`${userid ? `/` + this.props.params.employeeId + `/` : `/`}${MomentCurrentMonthFormat}`).then((success: any) => {
            SingleEmployeeStatus.get(`${userid ? `/` + this.props.params.employeeId + `/` : `/`}${this.props.params.date}`).then((success2: any) => {
                const statusobj = success2.data;
                this.setState({
                    statusobj
                })
                debug.log(statusobj, 's test')

            }).catch((err: any) => {
                debug.log(err.response, 'err test asd')
                if (err.response) {
                    if (err.response.data === 'No record found') {
                        const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0 }
                        this.setState({
                            statusobj
                        })
                    }

                    // if (err.response.data.message === "This Month Attendance doesnot exist") {
                    //     this.CalenderWithNoData(MomentCurrentMonthFormatWithCurrentDate, DifferenceResult)
                    // }
                }
            })

            const monthLastDay: any = moment(momentIncDate).endOf('month').toDate();
            // // log(monthLastDay, 'pk test')

            const monthEndDate: any = moment(momentIncDate).daysInMonth();
            const monthStartDay: any = moment(momentIncDate).startOf('month').toDate();
            const monthStartDayName: any = moment(monthStartDay).format('dddd')
            const monthLastDayName: any = moment(monthLastDay).format('dddd')
            const APIMonth: any = success.data[0].month


            // const APIMonthDays: any = APIMonth.find((obj: any) => {
            //     return obj.month.slice(0, 10) === MomentCurrentMonthFormat
            // });

            const APIMonthSortedArray: any = APIMonth.days.sort((a: any, b: any) => {

                const c: any = new Date(a.date)
                const d: any = new Date(b.date)

                return c - d
            });

            // log(APIMonthSortedArray, 'kash')



            this.setState({
                momentIncrementValue: momentIncValue
            })

            if (momentIncValue === 0) {
                this.setState({
                    NextDisableState: true,
                    PreviousDisableState: false
                })
            }
            else {
                this.setState({
                    NextDisableState: false,
                    PreviousDisableState: false
                })
            }


            if (monthStartDayName === 'Saturday') {
                // firstWeekArrayDup.push(i);

                for (let i = 1; i <= 6; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }
                // firstWeekArrayDup.push(i);

                firstWeekArrayDup2 = firstWeekArrayDup;

                // log(firstWeekArrayDup, 'test')
            }

            else if (monthStartDayName === 'Friday') {
                // firstWeekArrayDup.push(i);


                for (let i = 1; i <= 5; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;
                // log(firstWeekArrayDup, 'test')
            }

            else if (monthStartDayName === 'Thursday') {
                // firstWeekArrayDup.push(i);

                for (let i = 1; i <= 4; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Wednesday') {
                // firstWeekArrayDup.push(i);

                for (let i = 1; i <= 3; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Tuesday') {
                // firstWeekArrayDup.push(i);

                for (let i = 1; i <= 2; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Monday') {
                // firstWeekArrayDup.push(i);



                for (let i = 1; i <= 1; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            for (let i = 1; i <= monthEndDate; i++) {




                if (firstWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        firstWeekArrayDup2.push(x)
                    }
                    else {
                        firstWeekArrayDup2.push(i)

                    }


                    this.setState({
                        firstWeekArray: firstWeekArrayDup2,
                        sixthWeekArray: sixthWeekArrayDup2
                    })
                }

                else if (firstWeekArrayDup2.length === 7 && secondWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {

                        secondWeekArrayDup2.push(x)
                    }
                    else {
                        secondWeekArrayDup2.push(i)

                    }
                    this.setState({
                        secondWeekArray: secondWeekArrayDup2
                    })

                }

                else if (secondWeekArrayDup2.length === 7 && thirdWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        thirdWeekArrayDup2.push(x)
                    }
                    else {
                        thirdWeekArrayDup2.push(i)

                    }
                    this.setState({
                        thirdWeekArray: thirdWeekArrayDup2
                    })
                }

                else if (thirdWeekArrayDup2.length === 7 && forthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        forthWeekArrayDup2.push(x)
                    }
                    else {
                        forthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        forthWeekArray: forthWeekArrayDup2
                    })
                }
                else if (forthWeekArrayDup2.length === 7 && fifthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        fifthWeekArrayDup2.push(x)
                    }
                    else {
                        fifthWeekArrayDup2.push(i)

                    }

                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }
                    }

                    this.setState({
                        fifthWeekArray: fifthWeekArrayDup2
                    })
                }
                else if (fifthWeekArrayDup2.length === 7 && sixthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        sixthWeekArrayDup2.push(x)
                    }
                    else {
                        sixthWeekArrayDup2.push(i)

                    }


                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }
                    }

                    this.setState({
                        sixthWeekArray: sixthWeekArrayDup2
                    })
                }

            }


        }).catch((err: any) => {
            if (err.response) {
                if (err.response.data.message === "This Month Attendance doesnot exist") {
                    this.CalenderWithNoData(momentIncDate, momentIncValue)
                    const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }

                    this.setState({
                        statusobj
                    })
                }
            }
        })

        if (IncMonthValue <= 11) {

            switch (IncMonthValue) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        MonthValueInNo: IncMonthValue,
                    })
                    break;
                case 1:
                    this.setState({

                        MonthName: 'FEB',
                        MonthValueInNo: IncMonthValue,


                    })
                    break;
                case 2:
                    this.setState({

                        MonthName: 'MAR',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 3:

                    this.setState({

                        MonthName: 'APR',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 7:
                    this.setState({
                        MonthName: 'AUG',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 8:
                    this.setState({
                        MonthName: 'SEP',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 10:
                    this.setState({
                        MonthName: 'NOV',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 11:
                    this.setState({
                        MonthName: 'DEC',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({

                MonthName: 'JAN',
                MonthValueInNo: 0,
                year: this.state.year + 1

            })
        }

    }

    public DecrementMonthContainerFunc = () => {
        this.setState({
            NextDisableState: true,
            PreviousDisableState: true,
            fifthWeekArray: [],
            firstWeekArray: [],
            forthWeekArray: [],
            secondWeekArray: [],
            sixthWeekArray: [],
            thirdWeekArray: [],
        })

        const IncMonthValue: any = this.state.MonthValueInNo - 1;
        const momentCurrentDate: any = moment().format();
        const momentIncValue: any = this.state.momentIncrementValue - 1;
        const momentIncDate: any = moment(momentCurrentDate).add(momentIncValue, 'months').toDate()
        const MomentCurrentMonthFormat: any = moment(momentIncDate).startOf('month').format("YYYY-MM-DD")
        const userid = this.props.params.employeeId;
        const momentIncDateTochangeRoute: any = moment(momentIncDate).format("YYYY-MM-DD")
        if (userid) {
            browserHistory.push({
                pathname: `/employees/${userid}/attendance/${momentIncDateTochangeRoute}`,
            })
        }
        else {
            browserHistory.push({
                pathname: `/attendance/${momentIncDateTochangeRoute}`,
            })
        }
        EmployeesAttendance.get(`${userid ? `/` + this.props.params.employeeId + `/` : `/`}${MomentCurrentMonthFormat}`).then((success: any) => {
            SingleEmployeeStatus.get(`${userid ? `/` + this.props.params.employeeId + `/` : `/`}${this.props.params.date}`).then((success2: any) => {
                const statusobj = success2.data;
                this.setState({
                    statusobj
                })
                debug.log(statusobj, 's test')

            }).catch((err: any) => {
                debug.log(err.response, 'err test asd')
                if (err.response) {
                    if (err.response.data === 'No record found') {
                        const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0 }
                        this.setState({
                            statusobj
                        })
                    }

                }
            })

            const monthLastDay: any = moment(momentIncDate).endOf('month').toDate();

            const monthEndDate: any = moment(momentIncDate).daysInMonth();
            const monthStartDay: any = moment(momentIncDate).startOf('month').toDate();
            const monthStartDayName: any = moment(monthStartDay).format('dddd')
            const monthLastDayName: any = moment(monthLastDay).format('dddd')
            const APIMonth: any = success.data[0].month;

            let firstWeekArrayDup2: any = [];
            const secondWeekArrayDup2: any = [];
            const thirdWeekArrayDup2: any = [];
            const forthWeekArrayDup2: any = [];
            const fifthWeekArrayDup2: any = [];
            const sixthWeekArrayDup2: any = [];
            const firstWeekArrayDup: any = [];

            const APIMonthSortedArray: any = APIMonth.days.sort((a: any, b: any) => {

                const c: any = new Date(a.date)
                const d: any = new Date(b.date)

                return c - d
            });


            this.setState({
                NextDisableState: false,
                momentIncrementValue: momentIncValue,
            })

            if (monthStartDayName === 'Saturday') {
                for (let i = 1; i <= 6; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup
            }

            else if (monthStartDayName === 'Friday') {

                for (let i = 1; i <= 5; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Thursday') {

                for (let i = 1; i <= 4; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;
            }

            else if (monthStartDayName === 'Wednesday') {

                for (let i = 1; i <= 3; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Tuesday') {

                for (let i = 1; i <= 2; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Monday') {



                for (let i = 1; i <= 1; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }
                firstWeekArrayDup2 = firstWeekArrayDup;
            }

            for (let i = 1; i <= monthEndDate; i++) {




                if (firstWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        firstWeekArrayDup2.push(x)
                    }
                    else {
                        firstWeekArrayDup2.push(i)

                    }





                    this.setState({
                        firstWeekArray: firstWeekArrayDup2,
                        sixthWeekArray: sixthWeekArrayDup2
                    })
                }

                else if (firstWeekArrayDup2.length === 7 && secondWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {

                        secondWeekArrayDup2.push(x)
                    }
                    else {
                        secondWeekArrayDup2.push(i)

                    }
                    this.setState({
                        secondWeekArray: secondWeekArrayDup2
                    })

                }

                else if (secondWeekArrayDup2.length === 7 && thirdWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        thirdWeekArrayDup2.push(x)
                    }
                    else {
                        thirdWeekArrayDup2.push(i)

                    }
                    this.setState({
                        thirdWeekArray: thirdWeekArrayDup2
                    })
                }

                else if (thirdWeekArrayDup2.length === 7 && forthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        forthWeekArrayDup2.push(x)
                    }
                    else {
                        forthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        forthWeekArray: forthWeekArrayDup2
                    })
                }
                else if (forthWeekArrayDup2.length === 7 && fifthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        fifthWeekArrayDup2.push(x)
                    }
                    else {
                        fifthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        fifthWeekArray: fifthWeekArrayDup2
                    })
                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }
                    }
                }
                else if (fifthWeekArrayDup2.length === 7 && sixthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        sixthWeekArrayDup2.push(x)
                    }
                    else {
                        sixthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        sixthWeekArray: sixthWeekArrayDup2
                    })

                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }
                    }
                }

            }

            this.setState({
                PreviousDisableState: false
            })


        }).catch((err: any) => {
            if (err.response) {
                if (err.response.data.message === "This Month Attendance doesnot exist") {
                    this.CalenderWithNoData(momentIncDate, momentIncValue)
                    const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }

                    this.setState({
                        statusobj
                    })
                }
            }
        })

        if (IncMonthValue >= 0) {

            switch (IncMonthValue) {
                case 0:
                    this.setState({
                        MonthName: 'JAN',
                        MonthValueInNo: IncMonthValue,
                    })
                    break;
                case 1:
                    this.setState({

                        MonthName: 'FEB',
                        MonthValueInNo: IncMonthValue,


                    })
                    break;
                case 2:
                    this.setState({

                        MonthName: 'MAR',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 3:

                    this.setState({

                        MonthName: 'APR',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 4:
                    this.setState({

                        MonthName: 'MAY',
                        MonthValueInNo: IncMonthValue

                    })
                    break;

                case 5:
                    this.setState({

                        MonthName: 'JUNE',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 6:
                    this.setState({

                        MonthName: 'JULY',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 7:
                    this.setState({
                        MonthName: 'AUG',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 8:
                    this.setState({
                        MonthName: 'SEP',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 9:
                    this.setState({

                        MonthName: 'OCT',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                case 10:
                    this.setState({
                        MonthName: 'NOV',
                        MonthValueInNo: IncMonthValue


                    })
                    break;

                case 11:
                    this.setState({
                        MonthName: 'DEC',
                        MonthValueInNo: IncMonthValue

                    })
                    break;
                default:
                    break;
            }
        }
        else {
            this.setState({

                MonthName: 'DEC',
                MonthValueInNo: 11,
                year: this.state.year - 1




            })
        }
    }


    public RenderCalender = (id: any) => {
        let MomentCurrentMonthFormatWithCurrentDate = this.props.params.date;
        MomentCurrentMonthFormatWithCurrentDate = moment(MomentCurrentMonthFormatWithCurrentDate).startOf('month').format("YYYY,MM,DD")
        const MomentCurrentMonthFormatWithCurrentDateFullYear = new Date(MomentCurrentMonthFormatWithCurrentDate).getFullYear()
        const MomentCurrentMonthFormatWithCurrentDateMonth = new Date(MomentCurrentMonthFormatWithCurrentDate).getMonth()
        const MomentCurrentMonthFormatWithCurrentDateDay = new Date(MomentCurrentMonthFormatWithCurrentDate).getDate()
        const MomentCurrentMonthFormatWithFirstDate = moment().startOf('month').format("YYYY,MM,DD")
        const MomentCurrentMonthFormatWithFirstDateFullYear = new Date(MomentCurrentMonthFormatWithFirstDate).getFullYear()
        const MomentCurrentMonthFormatWithFirstDateMonth = new Date(MomentCurrentMonthFormatWithFirstDate).getMonth()
        const MomentCurrentMonthFormatWithFirstDateDay = new Date(MomentCurrentMonthFormatWithFirstDate).getDate()
        const DateDifferenceElement1 = moment([MomentCurrentMonthFormatWithCurrentDateFullYear, MomentCurrentMonthFormatWithCurrentDateMonth, MomentCurrentMonthFormatWithCurrentDateDay]);
        const DateDifferenceElement2 = moment([MomentCurrentMonthFormatWithFirstDateFullYear, MomentCurrentMonthFormatWithFirstDateMonth, MomentCurrentMonthFormatWithFirstDateDay]);
        const DifferenceResult = DateDifferenceElement1.diff(DateDifferenceElement2, 'month')
        const MomentCurrentMonthFormatWithCurrentDateForAPI = moment(MomentCurrentMonthFormatWithCurrentDate).startOf('month').format("YYYY-MM-DD")
        this.setState({
            MonthValueInNo: DifferenceResult,
            differenceresultforUI: DifferenceResult,
            momentIncrementValue: DifferenceResult,
        })

        EmployeesAttendance.get(`${id ? `/` + this.props.params.employeeId + `/` : `/`}${MomentCurrentMonthFormatWithCurrentDateForAPI}`).then((success: any) => {
            const IncMonthValue: any = DifferenceResult;
            const momentCurrentDate: any = moment().format();

            const momentIncDate: any = moment(momentCurrentDate).add(IncMonthValue, 'months').toDate()

            const monthLastDay: any = moment(momentIncDate).endOf('month').toDate();
            const monthEndDate: any = moment(momentIncDate).daysInMonth();
            const monthStartDay: any = moment(momentIncDate).startOf('month').toDate();

            const monthStartDayName: any = moment(monthStartDay).format('dddd')
            const monthLastDayName: any = moment(monthLastDay).format('dddd')

            const APIMonth: any = success.data[0].month

            const APIMonthSortedArray: any = APIMonth.days.sort((a: any, b: any) => {

                const c: any = new Date(a.date)
                const d: any = new Date(b.date)

                return c - d
            });

            let firstWeekArrayDup2: any = [];
            const secondWeekArrayDup2: any = [];
            const thirdWeekArrayDup2: any = [];
            const forthWeekArrayDup2: any = [];
            const fifthWeekArrayDup2: any = [];
            const sixthWeekArrayDup2: any = [];
            const firstWeekArrayDup: any = [];




            if (monthStartDayName === 'Saturday') {

                for (let i = 1; i <= 6; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Friday') {
                for (let i = 1; i <= 5; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Thursday') {

                for (let i = 1; i <= 4; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Wednesday') {

                for (let i = 1; i <= 3; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }

                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Tuesday') {
                for (let i = 1; i <= 2; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            else if (monthStartDayName === 'Monday') {
                for (let i = 1; i <= 1; i++) {
                    firstWeekArrayDup.push('');
                    this.setState({
                        firstWeekArray: firstWeekArrayDup
                    })
                }


                firstWeekArrayDup2 = firstWeekArrayDup;

            }

            for (let i = 1; i <= monthEndDate; i++) {

                if (firstWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        firstWeekArrayDup2.push(x)
                    }
                    else {
                        firstWeekArrayDup2.push(i)

                    }

                    this.setState({
                        firstWeekArray: firstWeekArrayDup2,
                        sixthWeekArray: sixthWeekArrayDup2
                    })
                }
                else if (firstWeekArrayDup2.length === 7 && secondWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {

                        secondWeekArrayDup2.push(x)
                    }
                    else {
                        secondWeekArrayDup2.push(i)

                    }
                    this.setState({
                        secondWeekArray: secondWeekArrayDup2
                    })

                }

                else if (secondWeekArrayDup2.length === 7 && thirdWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        thirdWeekArrayDup2.push(x)
                    }
                    else {
                        thirdWeekArrayDup2.push(i)

                    }
                    this.setState({
                        thirdWeekArray: thirdWeekArrayDup2
                    })

                }

                else if (thirdWeekArrayDup2.length === 7 && forthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        forthWeekArrayDup2.push(x)
                    }
                    else {
                        forthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        forthWeekArray: forthWeekArrayDup2
                    })
                }
                else if (forthWeekArrayDup2.length === 7 && fifthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        fifthWeekArrayDup2.push(x)
                    }
                    else {
                        fifthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        fifthWeekArray: fifthWeekArrayDup2
                    })
                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                fifthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                fifthWeekArrayDup2.push('');
                            }
                        }
                    }
                }
                else if (fifthWeekArrayDup2.length === 7 && sixthWeekArrayDup2.length < 7) {
                    const x: any = APIMonthSortedArray.find((obj: any) => {
                        return moment(obj.date).date() === i
                    })

                    if (x) {
                        sixthWeekArrayDup2.push(x)
                    }
                    else {
                        sixthWeekArrayDup2.push(i)

                    }
                    this.setState({
                        sixthWeekArray: sixthWeekArrayDup2
                    })

                    if (monthEndDate - i === 0) {
                        if (monthLastDayName === 'Monday') {
                            for (let i2 = 1; i2 <= 5; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Tuesday') {
                            for (let i2 = 1; i2 <= 4; i2++) {
                                sixthWeekArrayDup2.push('');
                            }

                        }

                        else if (monthLastDayName === 'Wednesday') {
                            for (let i2 = 1; i2 <= 3; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Thursday') {
                            for (let i2 = 1; i2 <= 2; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Friday') {
                            for (let i2 = 1; i2 <= 1; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }

                        else if (monthLastDayName === 'Sunday') {
                            for (let i2 = 1; i2 <= 6; i2++) {
                                sixthWeekArrayDup2.push('');
                            }
                        }
                    }
                }

            }

            if (DifferenceResult < 0) {
                this.setState({
                    NextDisableState: false,
                    PreviousDisableState: false
                })
            }
            else {
                this.setState({
                    PreviousDisableState: false
                })
            }




            SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${MomentCurrentMonthFormatWithCurrentDateForAPI}`).then((success2: any) => {
                const statusobj = success2.data;
                this.setState({
                    statusobj
                })
                debug.log(statusobj, 's test')

            }).catch((err2: any) => {
                debug.log(err2.response, 'err test')
                if (err2.response) {
                    if (err2.response.data === 'No Record Found') {
                        const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0, totalPaidLeaves: 0 }
                        this.setState({
                            statusobj
                        })
                    }


                }
            })

        }).catch((err: any) => {
            if (err.response) {
                if (err.response.data.message === "This Month Attendance doesnot exist") {
                    this.CalenderWithNoData(MomentCurrentMonthFormatWithCurrentDate, DifferenceResult)


                    SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${MomentCurrentMonthFormatWithCurrentDateForAPI}`).then((success: any) => {
                        const statusobj = success.data;
                        this.setState({
                            statusobj
                        })
                        debug.log(statusobj, 's test')

                    }).catch((err2: any) => {
                        debug.log(err2.response, 'err test')
                        if (err2.response) {
                            if (err2.response.data === 'No Record Found') {
                                const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0 }
                                this.setState({
                                    statusobj
                                })
                            }


                        }
                    })

                }
            }
        })







        switch (MomentCurrentMonthFormatWithCurrentDateMonth) {
            case 0:
                this.setState({
                    MonthName: 'JAN',
                    MonthValueInNo: 0,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            case 1:
                this.setState({

                    MonthName: 'FEB',
                    MonthValueInNo: 1,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;
            case 2:
                this.setState({

                    MonthName: 'MAR',
                    MonthValueInNo: 2,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;

            case 3:

                this.setState({

                    MonthName: 'APR',
                    MonthValueInNo: 3,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;

            case 4:
                this.setState({

                    MonthName: 'MAY',
                    MonthValueInNo: 4,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;

            case 5:
                this.setState({

                    MonthName: 'JUNE',
                    MonthValueInNo: 5,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,


                })
                break;
            case 6:
                this.setState({

                    MonthName: 'JULY',
                    MonthValueInNo: 6,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 7:
                this.setState({
                    MonthName: 'AUG',
                    MonthValueInNo: 7,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 8:
                this.setState({
                    MonthName: 'SEP',
                    MonthValueInNo: 8,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 9:
                this.setState({

                    MonthName: 'OCT',
                    MonthValueInNo: 9,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            case 10:
                this.setState({
                    MonthName: 'NOV',
                    MonthValueInNo: 10,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,



                })
                break;

            case 11:
                this.setState({
                    MonthName: 'DEC',
                    MonthValueInNo: 11,
                    year: MomentCurrentMonthFormatWithCurrentDateFullYear,

                })
                break;
            default:
                break;
        }


    }

    public componentDidMount() {
        if (this.props.location.state) {

            if (this.props.location.state.role === 'company') {
                const employeeID: any = this.props.params.employeeId

                EmployeeDetailAPI.get(employeeID).then((success: any) => {
                    this.setState({
                        employeeData: success.data
                    })
                }).then((success6: any) => {
                    this.RenderCalender(true)
                })
                    .catch((err: any) => {

                        // log(err.response, 'att error')
                    })

            }
            else {
                MyProfileAPI.get('me').then((success: any) => {
                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user

                    this.setState({
                        employeeData: EmpDetailArray
                    })
                })
                    .catch((err: any) => {

                        // log(err.response, 'att error')
                    })
                this.RenderCalender(false)

            }
            this.setState({
                fifthWeekArray: [],
                firstWeekArray: [],
                forthWeekArray: [],
                paramDate: this.props.params.date,
                secondWeekArray: [],
                sixthWeekArray: [],
                thirdWeekArray: [],
            })
            this.setState({
                LoginUserDetails: this.props.location.state,
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'employee') {
                    const EmpDetailArray = [];
                    EmpDetailArray[0] = success.data.user
                    const user = {
                        imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                        name: success.data.user.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user,
                        employeeData: EmpDetailArray
                    })
                }
            }).then((success2: any) => {
                if (this.state.LoginUserDetails) {
                    if (this.state.LoginUserDetails.role === 'company') {
                        const employeeID: any = this.props.params.employeeId

                        EmployeeDetailAPI.get(employeeID).then((success: any) => {
                            this.setState({
                                employeeData: success.data
                            })
                        }).then((nameset: any) => {
                            this.RenderCalender(true)
                        })
                            .catch((err: any) => {

                                // log(err.response, 'att error')
                            })
                    }
                    else {
                        this.RenderCalender(false)
                    }
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
    }


    public update = (value: any, index: any, arrayNo: any) => {
        this.setState({
            AddAttSuccessState: false,
        })
        if (typeof (value) !== 'number') {
            if (arrayNo === 1) {
                this.setState({
                    ShowModal: true,
                    arrayTobeEdited: 1,
                    index,
                    value,
                })
            }
            else if (arrayNo === 2) {
                this.setState({
                    ShowModal: true,
                    arrayTobeEdited: 2,
                    index,
                    value


                })
            }
            else if (arrayNo === 3) {
                this.setState({
                    ShowModal: true,
                    arrayTobeEdited: 3,
                    index,
                    value


                })
            } else if (arrayNo === 4) {
                this.setState({
                    ShowModal: true,

                    arrayTobeEdited: 4,
                    index,
                    value


                })
            } else if (arrayNo === 5) {
                this.setState({
                    ShowModal: true,

                    arrayTobeEdited: 5,
                    index,
                    value


                })
            }
            else if (arrayNo === 6) {
                this.setState({
                    ShowModal: true,

                    arrayTobeEdited: 6,
                    index,
                    value


                })
            }
        }
        else {
            const desktimeuser = this.state.employeeData[0].status
            if (desktimeuser === 'nonDesktimeUser') {
                const paramDate = this.props.params.date;
                const value1 = {
                    date: paramDate + 'T00:00:00.000Z'
                }

                if (arrayNo === 1) {
                    this.setState({
                        ShowModal: true,
                        arrayTobeEdited: 1,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1,
                    })
                }
                else if (arrayNo === 2) {
                    this.setState({
                        ShowModal: true,
                        arrayTobeEdited: 2,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1
                    })
                }
                else if (arrayNo === 3) {
                    this.setState({
                        ShowModal: true,
                        arrayTobeEdited: 3,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1
                    })
                } else if (arrayNo === 4) {
                    this.setState({
                        ShowModal: true,

                        arrayTobeEdited: 4,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1
                    })
                } else if (arrayNo === 5) {
                    this.setState({
                        ShowModal: true,
                        arrayTobeEdited: 5,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1
                    })
                }
                else if (arrayNo === 6) {
                    this.setState({
                        ShowModal: true,
                        arrayTobeEdited: 6,
                        dateForNDU: value,
                        desktimeuser: true,
                        index,
                        value: value1
                    })
                }

            }

        }


    }
    public ModalCloseFunc = () => {
        this.setState({
            ShowModal: false,
            absent: false,
            arrayTobeEdited: undefined,
            halfday: false,
            index: undefined,
            late: false,
            paidleave: false,
            present: false,
            unpaidleave: false,
            vacation: false,
            value: undefined,
        })
    }

    public APIFunction = (date: any, id: any, status: any, arrayTobeEd: any, ArrayCopy: any, FirstWeekArrayIndex: any) => {

        this.setState({
            UpdateAttenBtnDisp: true
        })
        if (!this.state.desktimeuser) {
            UpdateAttendanceAPI.put(`/${id}/${date}`, { status }).then((success: any) => {
                SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${this.props.params.date}`).then((success2: any) => {
                    const statusobj = success2.data;
                    this.setState({
                        statusobj
                    })
                    debug.log(statusobj, 's test')

                }).catch((err: any) => {
                    debug.log(err.response, 'err test')
                    if (err.response) {
                        if (err.response.data === 'No Record Found') {
                            const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0 }
                            this.setState({
                                statusobj
                            })
                        }
                    }
                })
                if (arrayTobeEd === 1) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 2) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 3) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 4) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 5) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 6) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'vacation'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'absent'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'present'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'paidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'unpaidleave'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'halfday'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        const ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue.status = 'late'
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                }

                this.setState({
                    ShowModal: false,
                    UpdateAttenBtnDisp: false,
                    absent: false,
                    arrayTobeEdited: undefined,
                    halfday: false,
                    index: undefined,
                    late: false,
                    paidleave: false,
                    present: false,
                    unpaidleave: false,
                    vacation: false,
                    value: undefined,
                })
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })
        }
        else {
            const ArrayIndexNonD = FirstWeekArrayIndex;
            const ArrayValueNonD = ArrayCopy[ArrayIndexNonD]
            const YearND = moment(date, "YYYY-MM-DD").year()
            const MonthND = moment(date, "YYYY-MM-DD").month()
            const DateND = ArrayValueNonD

            const FinalDateforND = moment([YearND, MonthND, DateND]).format("YYYY-MM-DD")


            const StatusObject = {
                status
            }

            SingleEmployeeAttendance.post(`/${id}/${FinalDateforND}`, StatusObject).then((success: any) => {
                SingleEmployeeStatus.get(`/${this.props.params.employeeId}/${this.props.params.date}`).then((success2: any) => {
                    const statusobj = success2.data;
                    this.setState({
                        statusobj
                    })
                    debug.log(statusobj, 's test')

                }).catch((err: any) => {
                    debug.log(err.response, 'err test')
                    if (err.response) {
                        if (err.response.data === 'No Record Found') {
                            const statusobj = { totalAbsents: 0, totalHalfDays: 0, totalLates: 0, totalPresents: 0 }
                            this.setState({
                                statusobj
                            })
                        }

                    }
                })

                if (arrayTobeEd === 1) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]

                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }

                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            firstWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 2) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            secondWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 3) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue

                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            thirdWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 4) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            forthWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 5) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            fifthWeekArray: ArrayCopy
                        })
                    }
                }
                else if (arrayTobeEd === 6) {
                    if (status === 'vacation') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'vacation',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'absent') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'absent',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'present') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'present',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'paidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'paidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'unpaidleave') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'unpaidleave',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'halfday') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'halfday',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                    else if (status === 'late') {
                        const ArrayCopy2 = ArrayCopy;
                        const ArrayIndex = FirstWeekArrayIndex;
                        let ArrayValue = ArrayCopy[ArrayIndex]
                        ArrayValue = {
                            _id: id,
                            date: FinalDateforND + "T00:00:00.000Z",
                            status: 'late',
                        }
                        ArrayCopy2[ArrayIndex] = ArrayValue
                        this.setState({
                            sixthWeekArray: ArrayCopy
                        })
                    }
                }
                this.setState({
                    ShowModal: false,
                    UpdateAttenBtnDisp: false,
                    absent: false,
                    arrayTobeEdited: undefined,
                    halfday: false,
                    index: undefined,
                    late: false,
                    paidleave: false,
                    present: false,
                    unpaidleave: false,
                    vacation: false,
                    value: undefined,
                })
            }).catch((err: any) => {

                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                    else if (err.response.status === 400) {
                        if (err.response.data.error === 'Attendance cant be made on the provided date.') {
                            this.setState({
                                AddAttSuccessState: true,
                                AddAttSuccessTextState: "Attendance can't be made on the provided date",
                                ShowModal: false,
                                UpdateAttenBtnDisp: false,
                                desktimeuser: false,

                            })
                        }
                    }
                }
            })
        }
    }

    public FinalUpdateFunc = () => {

        const updateDate = this.state.value.date.slice(0, 10)
        const employeeID = this.props.params.employeeId

        if (this.state.arrayTobeEdited === 1) {
            const FirstWeekArrayCopy = this.state.firstWeekArray;
            const FirstWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 1, FirstWeekArrayCopy, FirstWeekArrayIndex)

            }
        }
        else if (this.state.arrayTobeEdited === 2) {
            const SecondtWeekArrayCopy = this.state.secondWeekArray;
            const SecondWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 2, SecondtWeekArrayCopy, SecondWeekArrayIndex)

            }
        }
        else if (this.state.arrayTobeEdited === 3) {
            const ThirdtWeekArrayCopy = this.state.thirdWeekArray;
            const ThirdWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 3, ThirdtWeekArrayCopy, ThirdWeekArrayIndex)

            }
        }
        else if (this.state.arrayTobeEdited === 4) {
            const ForthtWeekArrayCopy = this.state.forthWeekArray;
            const ForthWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 4, ForthtWeekArrayCopy, ForthWeekArrayIndex)

            }
        }
        else if (this.state.arrayTobeEdited === 5) {
            const FifthWeekArrayCopy = this.state.fifthWeekArray;
            const FifthWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 5, FifthWeekArrayCopy, FifthWeekArrayIndex)

            }
        }

        else if (this.state.arrayTobeEdited === 6) {
            const SixthWeekArrayCopy = this.state.sixthWeekArray;
            const SixthWeekArrayIndex = this.state.index;
            if (this.state.absent === true) {
                this.APIFunction(updateDate, employeeID, 'absent', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)
            }
            else if (this.state.present === true) {
                this.APIFunction(updateDate, employeeID, 'present', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)
            }
            else if (this.state.vacation === true) {
                this.APIFunction(updateDate, employeeID, 'vacation', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)
            }
            else if (this.state.unpaidleave === true) {
                this.APIFunction(updateDate, employeeID, 'unpaidleave', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)


            }
            else if (this.state.paidleave === true) {
                this.APIFunction(updateDate, employeeID, 'paidleave', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)

            }
            else if (this.state.late === true) {
                this.APIFunction(updateDate, employeeID, 'late', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)

            }
            else if (this.state.halfday === true) {
                this.APIFunction(updateDate, employeeID, 'halfday', 6, SixthWeekArrayCopy, SixthWeekArrayIndex)

            }
        }
    }

    public UpdateAttendanceCon = (event: React.ChangeEvent<any>, value: any) => {

        if (event.target.value === 'halfday') {
            this.setState({
                absent: false,
                halfday: true,
                late: false,
                paidleave: false,
                present: false,
                unpaidleave: false,
                vacation: false,
            })
        }
        else if (event.target.value === 'present') {
            this.setState({
                absent: false,
                halfday: false,
                late: false,
                paidleave: false,
                present: true,
                unpaidleave: false,
                vacation: false,
            })
        }

        else if (event.target.value === 'absent') {
            this.setState({
                absent: true,
                halfday: false,
                late: false,
                paidleave: false,
                present: false,
                unpaidleave: false,
                vacation: false,
            })

        }

        else if (event.target.value === 'paidleave') {
            this.setState({
                absent: false,
                halfday: false,
                late: false,
                paidleave: true,
                present: false,
                unpaidleave: false,
                vacation: false,
            })

        }
        else if (event.target.value === 'unpaidleave') {
            this.setState({
                absent: false,
                halfday: false,
                late: false,
                paidleave: false,
                present: false,
                unpaidleave: true,
                vacation: false,
            })
        }
        else if (event.target.value === 'vacation') {
            this.setState({
                absent: false,
                halfday: false,
                late: false,
                paidleave: false,
                present: false,
                unpaidleave: false,
                vacation: true,
            })
        }
        else if (event.target.value === 'late') {
            this.setState({
                absent: false,
                halfday: false,
                late: true,
                paidleave: false,
                present: false,
                unpaidleave: false,
                vacation: false,
            })
        }
    }
    public ChRouteToEmpD = () => {
        const userid = this.props.params.employeeId;
        const AttendanceRouteObject = this.state.LoginUserDetails
        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/employees/${userid}`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/profile`,
                state: AttendanceRouteObject
            })
        }
    }

    public TabChDash = () => {

        const AttendanceRouteObject = this.state.LoginUserDetails
        const userid = this.props.params.employeeId;
        if (userid) {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/employees/${userid}/dashboard`,
                state: AttendanceRouteObject
            })
        }
        else {
            if (AttendanceRouteObject) {
                AttendanceRouteObject.CurrentMonthFormat = this.props.params.date
            }
            browserHistory.push({
                pathname: `/home`,
                state: AttendanceRouteObject
            })
        }
    }
    public render() {
        return (
            <EmployeeAttendance
                YearProp={this.state.year}
                MonthProp={this.state.MonthName}
                IncrementMonthPropFunc={this.IncrementMonthContainerFunc}
                DecrementMonthPropFunc={this.DecrementMonthContainerFunc}
                firstWeekArrayProp={this.state.firstWeekArray}
                fifthWeekArrayProp={this.state.fifthWeekArray}
                secondWeekArrayProp={this.state.secondWeekArray}
                thirdWeekArrayProp={this.state.thirdWeekArray}
                forthWeekArrayProp={this.state.forthWeekArray}
                sixthWeekArrayProp={this.state.sixthWeekArray}
                NextDisable={this.state.NextDisableState}
                PreviousDisable={this.state.PreviousDisableState}
                employeeDataProps={this.state.employeeData}
                RoleProp={this.state.LoginUserDetails}
                ChangeRouteToPayslipComponentFunc={this.ChangeRouteToPayslip}
                ShowModal={this.state.ShowModal}
                abc={this.update}
                ModalCloseFunc={this.ModalCloseFunc}
                FinalUpdateFunc={this.FinalUpdateFunc}
                valueprop={this.state.value}
                paramDate={this.state.paramDate}
                UpdateAttendanceCon={this.UpdateAttendanceCon}
                UpdateAttendanceBtnDisplay={this.state.UpdateAttenBtnDisp}
                changeRouteToEditP={this.changeRouteToEditP}
                AddAttSuccess={this.state.AddAttSuccessState}
                AddAttSuccessText={this.state.AddAttSuccessTextState}
                ChRouteToEmpD={this.ChRouteToEmpD}
                dateforNDUProp={this.state.dateForNDU}
                statusobjprop={this.state.statusobj}
                TabChDash={this.TabChDash}

            />
        );
    }
}


export default EmployeeAttendanceContainer;
