import axios from 'axios';

import * as React from 'react';
import { browserHistory } from 'react-router';
import { Debugger } from 'ts-debug';
import Invite from '../../components/invite/invite'
import RouteConfig from '../../routeconfig'
const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

interface ISTATE {
    InviteBtnState: any,
    InviteEmpArray: any,
    InviteSnackBState: any,
    InviteSnackBarTxtSt: any,
    LoginUserDetails: any,
    SyncBtnDis: any
}

type Props = Partial<{
    location: any,
    params: any,

}>


const EmployeesRenderAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/employeesByCompany` });
const InviteEmpApi: any = axios.create({ baseURL: `${RouteConfig}/api/users/invite` });

const SyncEmp: any = axios.create({ baseURL: `${RouteConfig}/api/users/syncdesktime` });
const archiveNdEmp: any = axios.create({ baseURL: `${RouteConfig}/api/users/archive` });



const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

archiveNdEmp.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
SyncEmp.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

InviteEmpApi.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

EmployeesRenderAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
class InviteCon extends React.Component<Props, ISTATE>  {
    constructor(props: any) {
        super(props)
        this.state = {
            InviteBtnState: false,
            InviteEmpArray: undefined,
            InviteSnackBState: false,
            InviteSnackBarTxtSt: undefined,
            LoginUserDetails: undefined,
            SyncBtnDis: false,
        }
    }


    public componentDidMount() {

        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }


                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'employee') {
                    browserHistory.replace('/')
                    localStorage.clear()
                    sessionStorage.clear()
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        browserHistory.replace('/')
                    }
                }
            })
        }


        EmployeesRenderAPI.get().then((success: any) => {
            debug.log(success,'success-----')
            this.setState({
                InviteEmpArray: success
            })
        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })

    }

    public InviteEmpConFun = (id: any, InviteIndex: any, reinvite: any) => {
        const EmpId = id;
        const Index = InviteIndex;
        const StateArrayForLoading = this.state.InviteEmpArray
        StateArrayForLoading.data[Index].loading = true
        debug.log(StateArrayForLoading, 'loading')
        this.setState({
            InviteBtnState: true,
            InviteEmpArray: StateArrayForLoading,
            InviteSnackBState: false,
            InviteSnackBarTxtSt: undefined,
        })


        InviteEmpApi.put(`/${EmpId}`)
            .then((success: any) => {
                if (reinvite) {
                    StateArrayForLoading.data[Index].status = 'invited'
                    delete StateArrayForLoading.data[Index].loading
                    debug.log(StateArrayForLoading, 'finish')

                    this.setState({
                        InviteBtnState: false,
                        InviteEmpArray: StateArrayForLoading,
                        InviteSnackBState: true,

                        InviteSnackBarTxtSt: 'Employee has been Re-Invited',
                    })
                }
                else {
                    StateArrayForLoading.data[Index].status = 'invited'
                    delete StateArrayForLoading.data[Index].loading
                    debug.log(StateArrayForLoading, 'finish')

                    this.setState({
                        InviteBtnState: false,
                        InviteEmpArray: StateArrayForLoading,
                    })
                }
            })
            .catch((err: any) => {
                debug.log(err.response, 'asd')
                delete StateArrayForLoading.data[Index].loading
                if (err) {
                    if (err.response.data.message === 'Employee already invited') {
                        this.setState({
                            InviteBtnState: false,
                            InviteEmpArray: StateArrayForLoading,
                            InviteSnackBState: true,
                            InviteSnackBarTxtSt: 'Employee is already Re-Invited'
                        })
                    }

                }

            })

    }
    public SyncFunc = () => {
        this.setState({
            SyncBtnDis: !this.state.SyncBtnDis
        })
        SyncEmp.post().then((success: any) => {
            this.setState({
                InviteSnackBState: true,
                InviteSnackBarTxtSt: 'Employees have been synchronized successfully',
                SyncBtnDis: false,
            })
        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
   public  archiveNdUser=(id: any, index:any)=>{
        archiveNdEmp.post(`/${id}`).then((success: any) => {
            debug.log(success,'-sseeeeee')

            const StateArrayForLoading = this.state.InviteEmpArray
            StateArrayForLoading.data[index].archived = true
            this.setState({
                InviteEmpArray: StateArrayForLoading,
                InviteSnackBState: true,
                InviteSnackBarTxtSt: 'Employee has been archived successfully',
                SyncBtnDis: false,
            })
        }).catch((err: any) => {
            debug.log(err.response,'-sseeeeee')
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    public render() {
        return (
            <Invite RoleProp={this.state.LoginUserDetails}
                InviteEmpProp={this.state.InviteEmpArray}
                InviteEmpFun={this.InviteEmpConFun}
                InviteBtnDisProp={this.state.InviteBtnState}
                InviteSnackBar={this.state.InviteSnackBState}
                InviteSnackBarText={this.state.InviteSnackBarTxtSt}
                SyncFunc={this.SyncFunc}
                syncBtnDis={this.state.SyncBtnDis}
                archiveNdUser={this.archiveNdUser}
            />
        );
    }
}

export default InviteCon;
