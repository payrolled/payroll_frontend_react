
import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import Login from '../../components/Login/LoginForm'
import RouteConfig from '../../routeconfig'

const Config = { isProd: false }; // example config in your application

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

const LoginAPI: any = axios.create({ baseURL: `${RouteConfig}/api/auth/` });




interface ISTATE {
    LoaderDisplayState: any,
    email: any,
    errordisplayState: any,
    errorVisibilityState: any,
    password: any,
    registerDisableState: any
}
type Props = Partial<{
    params: any,
    router: any,

}>



class LoginContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            LoaderDisplayState: 'hidden',
            email: undefined,
            errorVisibilityState: 'hidden',
            errordisplayState: 'default',
            password: undefined,
            registerDisableState: true,
        }
    }

    public EmailtextFieldFunc = (value1: any) => {
        if (this.state.password && value1.target.value) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })
        }

        this.setState({
            email: value1.target.value
        })
    }

    public PasswordtextFieldFunc = (value1: any) => {
        if (this.state.email && value1.target.value) {
            this.setState({
                registerDisableState: false
            })
        }
        else {
            this.setState({
                registerDisableState: true
            })

        }
        this.setState({
            password: value1.target.value
        })
    }
    public ChangeRouteToRegister = () => {
        browserHistory.push('/register')
    }

    public LoginFunc = (event:any) => {
        event.preventDefault()
        if (this.state.email && this.state.password) {
            this.setState({
                LoaderDisplayState: 'unset',
                registerDisableState: true,
            })
            const credentials: any = {
                email: this.state.email,
                password: this.state.password,
            }
            LoginAPI.post('local/', credentials).then((success: any) => {

                localStorage.setItem('user', success.data.token)
                if (success.data.role === 'company') {
                    if(success.data.service){
                        if(success.data.apikey){
                            const user = {
                                name: success.data.body.name,
                                role: success.data.role,
                            }
                            browserHistory.push({
                                pathname: '/home',
                                state: user
                            })
                        }
                        else{
                            browserHistory.push({
                                pathname: '/service/api_key',
                            })
                        }
                    }
                    else{
                        browserHistory.push({
                            pathname: '/service',
                        })
                    }
                    debug.log(success.data,'test')
                }
                else if (success.data.role === 'employee') {
                    browserHistory.push({
                        pathname: '/home',
                        state: success.data
                    })
                }

            }).catch((error: any) => {
                this.setState({
                    LoaderDisplayState: 'hidden',
                    registerDisableState: false
                })
                if (error.response) {
                    if(error.response.status===403){
                        if(error.response.data.token){
                            if(error.response.data.service === true && error.response.data.apikey===false){
                                localStorage.setItem('user',error.response.data.token)
                                browserHistory.push('/service/api_key')
                            }
                            else{
                                localStorage.setItem('user',error.response.data.token)
                                browserHistory.push('/service')
                            }
                        }
                    }
                    else if(error.response.status===401){
                        this.setState({
                            errorVisibilityState: 'unset',
                            errordisplayState: `* ${error.response.data.message}`,
                        })
                    }
                    else if (error.response.data === 'Missing password or salt') {
                        this.setState({
                            errorVisibilityState: 'unset',
                            errordisplayState: "* Your password is not set",
                        })
                    }
                    else if(error.response.data.message==='This password is not correct.'){
                        this.setState({
                            errorVisibilityState: 'unset',
                            errordisplayState: "* Your password is incorrect",
                        })
                    }
                }
                else {
                    this.setState({
                        errorVisibilityState: 'unset',
                        errordisplayState: `* Network Error`,
                    })
                }
            })
        }
    }
    public render() {
        return (
            <Login changeRouteToRegister={this.ChangeRouteToRegister} EmailValue={this.EmailtextFieldFunc} 
            PasswordValue={this.PasswordtextFieldFunc} LoginFuncProps={this.LoginFunc} RegisterDisable={this.state.registerDisableState} 
            LoaderDisplayProp={this.state.LoaderDisplayState} errordisplayProp={this.state.errordisplayState} errorVisibilityProp={this.state.errorVisibilityState} />
        );
    }
}

export default LoginContainer;
