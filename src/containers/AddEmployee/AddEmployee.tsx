import axios from 'axios'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import AddEmployeeComponent from '../../components/AddEmployee/AddEmployee';
import RouteConfig from '../../routeconfig'


const Config = { isProd: false };

const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });


const AddEmployeeAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

AddEmployeeAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

type Props = Partial<{
    location: any
}>
interface ISTATE {
    AddEmpBtnDis: any,
    EmployeeStateArray: any,
    LoginUserDetails: any,
    image: any,
    accountno: any,
    address: any,
    banktit: any,
    cellno: any,
    currsalary: any,
    designation: any,
    desktimeID: any,
    emailadd: any,
    employeeID: any,
    female: any,
    fullname: any,
    male: any,
    updateError: any,
    updateErrorText: any,
    updateSuccess: any,
    updateSuccesstext: any,
    username: any,
}
class AddEmployeeContainer extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = {
            AddEmpBtnDis: false,
            EmployeeStateArray: undefined,
            LoginUserDetails: undefined,
            accountno: undefined,
            address: undefined,
            banktit: undefined,
            cellno: undefined,
            currsalary: undefined,
            designation: undefined,
            desktimeID: undefined,
            emailadd: undefined,
            employeeID: undefined,
            female: false,
            fullname: undefined,
            image: undefined,
            male: true,
            updateError: false,
            updateErrorText: undefined,
            updateSuccess: false,
            updateSuccesstext: undefined,
            username: undefined,
        }
    }
    public componentDidMount() {

        if (this.props.location.state) {
            this.setState({
                LoginUserDetails: this.props.location.state
            })
        }
        else {

            MyProfileAPI.get('me').then((success: any) => {
                if (success.data.role === 'company') {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    this.setState({
                        LoginUserDetails: user
                    })
                }
                else if (success.data.role === 'company') {

                    const user = {
                        name: success.data.user.name,
                        role: success.data.role,
                    }

                    this.setState({
                        LoginUserDetails: user
                    })
                }
            }).catch((err: any) => {
                if (err.response) {

                    if (err.response.status === 401) {
                        localStorage.removeItem('user')
                        sessionStorage.clear()
                        browserHistory.replace('/')
                    }
                }
            })

        }
    }

    public AddEmployeeFunc = () => {
        const finalObjUEmp = {
            accountNumber: this.state.accountno,
            address: this.state.address,
            bankTitle: this.state.banktit,
            cellNumbers: this.state.cellno,
            designation: this.state.designation,
            employeeId: this.state.employeeID,
            gender: this.state.female ? 'female' : 'male',
            imagePath: this.state.image,
            name: this.state.fullname,
            salary: Number(this.state.currsalary),
            userName: this.state.username,
        }
        if (finalObjUEmp.name && finalObjUEmp.userName) {
            this.setState({
                AddEmpBtnDis: true
            })
            AddEmployeeAPI.post('add', finalObjUEmp).then((success: any) => {
                debug.log(success.data._id, 'add success')
                this.setState({
                    AddEmpBtnDis: false,
                    updateSuccess: true,
                    updateSuccesstext: 'Employee has been added successfully',
                })
                browserHistory.push({
                    pathname: `/employees/${success.data._id}`,
                    state: this.state.LoginUserDetails,
                })
            }).catch((err: any) => {
                // debug.log(err.response, 'add emp err')
            })
        }
        else {
            this.setState({
                updateError: true,
                updateErrorText: '* Fullname & Username are must required.',
            })
            window.scrollTo(0, document.body.scrollHeight);
        }
    }
    public NameChangeFunc = (event: React.ChangeEvent<any>) => {
        this.setState({
            accountno: event.target.name === 'accNo' ? event.target.value : this.state.accountno,
            address: event.target.name === 'address' ? event.target.value : this.state.address,
            banktit: event.target.name === 'banktit' ? event.target.value : this.state.banktit,
            cellno: event.target.name === 'cellno' ? event.target.value : this.state.cellno,
            currsalary: event.target.name === 'salary' ? event.target.value : this.state.currsalary,
            designation: event.target.name === 'designation' ? event.target.value : this.state.designation,
            desktimeID: event.target.name === 'desktimeId' ? event.target.value : this.state.desktimeID,
            emailadd: event.target.name === 'email' ? event.target.value : this.state.emailadd,
            employeeID: event.target.name === 'employeeId' ? event.target.value : this.state.employeeID,
            female: event.target.name === 'female' ? true : false,
            fullname: event.target.name === 'name' ? event.target.value : this.state.fullname,
            image: event.target.name === 'image' ? event.target.value : this.state.image,
            male: event.target.name === 'male' ? true : false,
            username: event.target.name === 'username' ? event.target.value : this.state.username,
        })
    }
    public handleImageChange = (event: React.ChangeEvent<any>) => {
        const reader = new FileReader();

        reader.onload = (e2: any) => {
            this.setState({ image: e2.target.result });
        };
        if (event.target.files[0] === undefined) {
            this.setState({
                image: this.state.image
            })
        }
        else {

            reader.readAsDataURL(event.target.files[0]);
        }
    }
    public clearImageState = () => {
        this.setState({
            image: undefined
        })
    }
    public render() {
        return (
            <AddEmployeeComponent RoleProp={this.state.LoginUserDetails}
                FieldChange={this.NameChangeFunc}
                handleImageChange={this.handleImageChange}
                imageProp={this.state.image}
                clearImageState={this.clearImageState}
                AddEmployeFunProp={this.AddEmployeeFunc}
                maleCheck={this.state.male}
                femaleCheck={this.state.female}
                updateSuccess={this.state.updateSuccess}
                updateErrorText={this.state.updateErrorText}
                updateError={this.state.updateError}
                AddEmpBtnDis={this.state.AddEmpBtnDis}
                updateSuccesstext={this.state.updateSuccesstext}

            />
        );
    }
}

export default AddEmployeeContainer;
