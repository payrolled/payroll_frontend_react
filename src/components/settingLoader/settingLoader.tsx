import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import * as React from 'react';
import './settingLoader.css';
interface ISTATE {
    type: any
}
type Props = Partial<{
    SettingLoaderProp: any
}>

const aqw = "muhazzib"
class SettingLoader extends React.Component<Props, ISTATE>{
    constructor(props: any) {
        super(props);
    }
    public render() {
        return (
            this.props.SettingLoaderProp ? (
                <Table multiSelectable={false} selectable={false} className='NoRecordSetting'>
                    <TableBody displayRowCheckbox={false}>
                        

                        {
                            aqw.split("").map((value, index) => {
                                return (
                                    <TableRow className='SettingLoaderTable' key={index}>
                                        {
                                            aqw.split("").map((value2, index2) => {
                                                return (
                                                    <TableRowColumn key={value2}>

                                                        <div className='AttendanceLoader noMargin'>

                                                            <div className='AttendanceLoaderChild1'>
                                                                <div className="animated-background-ParaNextToImage Settingloader1">
                                                                    .
                                                            </div>
                                                            </div>

                                                        </div>
                                                    </TableRowColumn>

                                                )
                                            })
                                        }
                                    </TableRow>

                                )

                            })
                        }

                    </TableBody>
                </Table >

            ) : null
        )

    }
}

export default SettingLoader;

