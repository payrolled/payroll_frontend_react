import { Card } from 'material-ui/Card';
import DatePicker from 'material-ui/DatePicker';
import LinearProgress from 'material-ui/LinearProgress';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import Toggle from 'material-ui/Toggle';
import * as moment from 'moment'
import * as React from 'react';
import 'react-circular-progressbar/dist/styles.css';
import { browserHistory } from 'react-router'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import './attendance.css';

const styles = {
    block: {
        maxWidth: 250,
    },
    labelStyle: {
        color: 'black',
        fontFamily: 'NexaBold'
    },
    thumbOff: {
        backgroundColor: 'white',
    },
    thumbSwitched: {
        backgroundColor: 'white',
    },
    toggle: {
        marginBottom: 16,
    },
    trackOff: {
        backgroundColor: '#008141',
    },
    trackSwitched: {
        backgroundColor: '#008141',
    },
};

interface ISTATE {
    controlledDate: any
}


type Props = Partial<{
    AttBtnDisProp: any,
    AttErrText: any,
    AttErrTextFlag: any,
    DateChangeFunc: any,
    DateChangeValue: any,
    FinalAddAttFunc: any,
    RoleProp: any,
    DateChangeValue2: any,
    DateChangeFunc2: any,
    doubleAtt: any,
    ToggAttFunc: any,
    toggText: any,
    showPerLoa: any,
    percentage: any
}>
class Attendance extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
    }

    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'AdminDashboard') {
            browserHistory.push({
                pathname: '/home',
                state: this.props.RoleProp
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            browserHistory.push({
                pathname: '/employees',
                state: this.props.RoleProp
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }




    public SignOut = () => {
        localStorage.removeItem('data')
        localStorage.removeItem('user')
        browserHistory.replace('/')
    }
    public render() {
        return (
            <div>

                <AppBarComponent RoleProp={this.props.RoleProp} HideNavigationItemEmployees='toggler' />
                <div className='DashboardMainBody'>
                    <Sidebar RoleProp={this.props.RoleProp} active='attendance' />

                    <div className='sideMainBody attendanceSidebody'>
                        <h1 className='ViewEmployeesHeading AddEmployeeHeadingPostion addemployeeHeadingColor'>ADD ATTENDANCE</h1>
                        <div className='payslipchild2Parent AddEmployeeBody'>
                            <div className='addattonlychild'>


                                <Card className='AddAttCard'>
                                    {
                                        this.props.showPerLoa ? (
                                            <LinearProgress mode="determinate" value={this.props.percentage} color={'#008141'} />
                                        ) :
                                         null}
                                    <div className='AttCardMDiv'>
                                        <div className='attendBtnDiv'>
                                            {/* <span>Multiple</span> */}
                                            <Toggle
                                                label={`${this.props.toggText}`}
                                                thumbStyle={styles.thumbOff}
                                                trackStyle={styles.trackOff}
                                                thumbSwitchedStyle={styles.thumbSwitched}
                                                trackSwitchedStyle={styles.trackSwitched}
                                                labelStyle={styles.labelStyle}
                                                className='attTogg'
                                                onToggle={this.props.ToggAttFunc}
                                            />
                                        </div>
                                        {
                                            this.props.doubleAtt ? (
                                                <div className='AddAttChild1'><label>From:</label></div>
                                            ) : (
                                                    <div className='AddAttChild1'><label>Date:</label></div>

                                                )
                                        }


                                        {
                                            this.props.doubleAtt ? (
                                                <div className='AddAttChild2'>

                                                    <DatePicker
                                                        hintText="Select Date"
                                                        value={this.props.DateChangeValue}
                                                        onChange={this.props.DateChangeFunc}
                                                        className='AddAttendanceCal'
                                                        textFieldStyle={{ width: '100%', borderBottomColor: '#3f4d58' }}
                                                    />
                                                </div>) : (
                                                    <div className='AddAttChild2'>

                                                        <DatePicker
                                                            hintText="Select Date"
                                                            value={this.props.DateChangeValue}
                                                            onChange={this.props.DateChangeFunc}
                                                            className='AddAttendanceCal'
                                                            textFieldStyle={{ width: '100%', borderBottomColor: '#3f4d58' }}

                                                        />
                                                    </div>
                                                )
                                        }


                                        {
                                            this.props.doubleAtt ? (
                                                <div className='AddAttChild1To'><label>To:</label></div>
                                            ) : null
                                        }


                                        {
                                            this.props.doubleAtt ? (
                                                <div className='AddAttChild2To'>

                                                    <DatePicker
                                                        hintText="Select Date"
                                                        value={this.props.DateChangeValue2}
                                                        onChange={this.props.DateChangeFunc2}
                                                        className='AddAttendanceCal'
                                                        textFieldStyle={{ width: '100%', borderBottomColor: '#3f4d58' }}

                                                    />
                                                </div>) : null
                                        }


                                        < div className='attendBtnDiv'>
                                            <RaisedButton label="Submit" backgroundColor='#008141' className='btnFontColor registerbuttonstyle addAttSubmitBtn' style={{ color: 'white', width: '2px !important' }} onClick={this.props.FinalAddAttFunc} disabled={this.props.AttBtnDisProp} />
                                        </div>

                                    </div>


                                </Card>
                                <Snackbar
                                    open={this.props.AttErrTextFlag}
                                    message={this.props.AttErrText}
                                    contentStyle={{ textAlign: 'center' }}
                                // autoHideDuration={3000}
                                // onRequestClose={this.handleRequestClose}
                                />

                            </div>
                        </div>
                    </div>
                </div >
                <Footer FooterClass='ResgisterFooter' />
            </div >
        )
    }
}

export default Attendance;
