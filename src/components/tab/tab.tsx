import * as React from 'react';
import './tab.css'
interface ITabProps {
    activeProfile: boolean,
    activeDash: boolean,
    activePay: boolean,
    activeAtt: boolean,
    AttChPay:any,
    TabChDash:any,
    TabChPay: any,
    TabChProfile:any,
}
const Tab: React.SFC<ITabProps> = (props) => (
    <div className='tabParent'>
        <div className={`tabChild2 ${props.activeDash ? 'activeTab' : null}`} onClick={props.TabChDash?props.TabChDash:null}>DASHBOARD</div>
        <div className={`tabChild3 ${props.activePay ? 'activeTab' : null}`} onClick={props.TabChPay?props.TabChPay:null}>PAYSLIP</div>
        <div className={`tabChild4 ${props.activeAtt ? 'activeTab' : null}`} onClick={props.AttChPay?props.AttChPay:null}>ATTENDANCE</div>
        <div className={`tabChild1 ${props.activeProfile ? 'activeTab' : null}`} onClick={props.TabChProfile?props.TabChProfile:null}>PROFILE</div>

    </div>  
)


export default Tab