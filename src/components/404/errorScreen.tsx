import * as React from 'react';
import './errorScreen.css';
const ErrorScreen: React.SFC = (props) => (
    <div className='ErrMainDiv'>
        <h1>404<br />Page Not Found</h1>
        <p>Sorry, but the page you were trying to view does not exist.</p>
    </div>
)


export default ErrorScreen