import Drawer from 'material-ui/Drawer';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'// import './ApiKeyScreen.css';
import { Bar, BarChart, Cell, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import AppBarComponent from '../AppBar/AppBar';
// import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import Tab from '../tab/tab'
import './employeedashboard.css';


type Props = Partial<{
    AnnualObjectProp: any,
    CurrMonthObjectProp: any,
    ChRouteToEmpD: any,
    EmployeeObjectProp: any,
    PayslipLoaderProp: any,
    PayslipMonthChangeInC: any,
    PayslipMonthChangeProp: any,
    RoleProp: any,
    ChangeRouteToAttendanceComponentFunc: any,
    ChangeRouteToPayslipComponentFunc: any,
    SalariesGraphComparisonYear: any,
    SalariesGraphIncDateFunc: any,
    employeeDataProps: any
    currentMonProp: any,
    changeRouteToEditP: any,
    IncDateFunc: any,
    statusobjprop: any,
    DecDateFunc: any,
    Salariesdata: any
    Loader: any,
    Month: any,
    MonthDetailObject: any,
    SalariesGraphDecDateFunc: any,
    SalaryDivDisplay: any,
    Year: any,
    ShowModal: any,
    ModalCloseFunc: any,
    updateSalFunc: any,
    valueprop: any,
    dateforNDUProp: any,
    paramDate: any,
    UpdateAttendanceBtnDisplay: any,
    bonusprop: any,
    FinalUpdateFunc: any,
    bsalprop: any


}>

class EmployeeDashboard extends React.Component<Props> {
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            delete obj.CurrentMonthFormat


            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }


    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <div className={`sidebar empdashSBanim ${this.props.ShowModal ? 'modalopenEmpDSideBar' : null}`}>
                        {
                            this.props.RoleProp ?
                                this.props.RoleProp.role === 'company' ?
                                    (
                                        <ul className='sidebarList'>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToEmployees}><i className="material-icons EmployeeIcon">supervisor_account</i>Employees</li>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToAttendance}><i className="material-icons EmployeeIcon">schedule</i>Attendance</li>
                                            <li className='sidebarListLastLi' onClick={this.ChangeRouteToSettings}><i className="material-icons SettingsIcon">pie_chart</i>Salary Sheet</li>
                                        </ul>
                                    )
                                    :

                                    this.props.RoleProp.role === 'employee' ?
                                        (
                                            <ul className='sidebarList'>
                                                <li className='sidebarListFirstTwoLi NoBorderBottom' onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                                            </ul>

                                        ) : null : null

                        }
                    </div>
                    <div className={`sideMainBody payslipSideBody ${this.props.ShowModal ? 'modalopenUiEmpDMainBody' : null}`}>

                        {
                            this.props.employeeDataProps ? (
                                <div className='sideMainBodyChild1'>
                                    <h1 className='DashboardMainHeadND'>
                                        {this.props.employeeDataProps[0].name}
                                    </h1>
                                    <div className='DashDSwitcher'>
                                        <button onClick={this.props.DecDateFunc} className='NoPadddingND'><i className="material-icons">keyboard_arrow_left</i></button>
                                        <span className='salarygraphYear'>{`${this.props.Month} ${this.props.Year}`}</span>
                                        <button onClick={this.props.IncDateFunc} className='NoPadddingND'><i className="material-icons" >keyboard_arrow_right</i></button>
                                    </div>

                                </div>

                            ) : (
                                    <div className="animated-background-toggleLine EmpDetailLoader">
                                        .
                                        </div>
                                )
                        }

                        <Tab activeProfile={false} activeDash={true} activeAtt={false} activePay={false}
                            AttChPay={this.props.ChangeRouteToAttendanceComponentFunc}
                            TabChProfile={this.props.ChRouteToEmpD}
                            TabChDash={null}
                            TabChPay={this.props.ChangeRouteToPayslipComponentFunc}
                        />

                        <div className='NOfAPMainDiv'>
                            <div className='NOfAPMainDiv1'>
                                <span>PRESENTS</span>
                                <span>{
                                    this.props.statusobjprop ?
                                        this.props.statusobjprop.totalPresents : 0
                                }</span>
                            </div>

                            <div className='NOfAPMainDiv1'>
                                <span>PAID LEAVES</span>
                                <span>{
                                    this.props.statusobjprop ?
                                        this.props.statusobjprop.totalPaidLeaves : 0
                                }</span>
                            </div>

                            <div className='NOfAPMainDiv2'>
                                <span>LATES</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalLates : 0
                                    }
                                </span>
                            </div>
                            <div className='NOfAPMainDiv3'>
                                <span>ABSENTS</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalAbsents : 0
                                    }
                                </span>
                            </div>
                            <div className='NOfAPMainDiv4'>
                                <span>HALF DAYS</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalHalfDays : 0
                                    }
                                </span>
                            </div>
                        </div>


                        <div className='sideMainBodyChild2'>
                            <div className='sideMainBodyChild2OnlyChild'>

                                <div className='sideMainBodyChild2Child1'>
                                    <div className='sideMainBodyChild2Child1Parent'>


                                        <div className='sideMainBodyChild2Child1ParentChild1'>
                                            <p className='USalSpanPara'>SALARIES</p>
                                            <span className='USalSpan' onClick={this.props.updateSalFunc}>EDIT</span>
                                        </div>


                                        <div className='NewThemeParentChild3' >
                                            <div className='NewThemeParentChild3Child1'>

                                                <div className='NewThemeParentChild3Child111'>
                                                    <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                        {
                                                            this.props.Loader === 'hidden' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                    <p>Basic</p>
                                                                    <h1 style={{ visibility: this.props.SalaryDivDisplay }} className='BasicColor'>{this.props.MonthDetailObject !== undefined ? (`${this.props.MonthDetailObject.currentSalary ? `PKR ${this.props.MonthDetailObject.currentSalary.toLocaleString()}` : 'PKR 0.00'}`) : ('PKR 0.00')}</h1>
                                                                </div>
                                                            ) : null
                                                        }


                                                        {
                                                            this.props.Loader === 'unset' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild1   '>
                                                                    <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                    {/* <p>Total Salary</p>
                <h1 style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (this.props.MonthDetailObject.totalSalary) : ('PKR 0.00')}</h1> */}
                                                                </div>
                                                            ) : null
                                                        }

                                                        {
                                                            this.props.Loader === 'unset' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild2   '>
                                                                    <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>

                                                                </div>
                                                            ) : null
                                                        }


                                                        {
                                                            this.props.Loader === 'hidden' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                    <p>Bonus</p>
                                                                    <h1 className='Bonuscolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (`PKR ${this.props.MonthDetailObject.totalBonus.toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                </div>

                                                            ) : null
                                                        }
                                                    </div>

                                                    <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                        {
                                                            this.props.Loader === 'unset' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild1   '>
                                                                    <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>

                                                                </div>
                                                            ) : null
                                                        }

                                                        {
                                                            this.props.Loader === 'hidden' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                    <p>Deductions</p>
                                                                    <h1 className='Deductioncolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (`PKR ${this.props.MonthDetailObject.totalDeductions.toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                </div>
                                                            ) : null
                                                        }



                                                        {
                                                            this.props.Loader === 'unset' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild2   '>
                                                                    <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>

                                                                </div>
                                                            ) : null
                                                        }


                                                        {
                                                            this.props.Loader === 'hidden' ? (
                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                    <p>Tax</p>
                                                                    <h1 className='Taxcolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (`PKR ${this.props.MonthDetailObject.totalTax.toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                </div>
                                                            ) : null
                                                        }
                                                    </div>
                                                </div>

                                                <div className='NewThemeParentChild3Child112'>

                                                    <div className='sideMainBodyChild2Child1ParentChild3Parent'>
                                                        {
                                                            this.props.Loader === 'unset' ? (

                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth      totalPaidWidth'>
                                                                    <div className="animated-backgroundDashboardTotalPaid NDTPHeight">.</div>

                                                                </div>
                                                            ) : null
                                                        }


                                                        {
                                                            this.props.Loader === 'hidden' ? (


                                                                <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth'>
                                                                    <p>Total Payable</p>

                                                                    <h1 style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${((this.props.MonthDetailObject.totalBonus + this.props.MonthDetailObject.totalSalaries) - this.props.MonthDetailObject.totalTax).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                </div>
                                                            ) : null
                                                        }
                                                    </div>
                                                </div>

                                            </div>




                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div className='sideMainBodyChild3'>

                            <div className='sideMainBodyChild2Child1Parent'>

                                <div className='sideMainBodyChild2Child1ParentChild2'>
                                    <p className='salariesGraph'>SALARIES GRAPH</p>
                                    <div className='DateSwitcher'>
                                        <button onClick={this.props.SalariesGraphDecDateFunc} className='NoPaddding'><i className="material-icons">keyboard_arrow_left</i></button><span className='salarygraphYearND'>{`${this.props.SalariesGraphComparisonYear}`}</span><button onClick={this.props.SalariesGraphIncDateFunc} className='NoPaddding'><i className="material-icons" >keyboard_arrow_right</i></button>
                                    </div>
                                </div>


                                <div className='sideMainBodyChild2Child1ParentChild3 secondGraTTip' >

                                    {
                                        this.props.Salariesdata ? (
                                            <ResponsiveContainer width="98%" height={300}>

                                                <BarChart data={this.props.Salariesdata}
                                                >
                                                    <XAxis dataKey="name" />
                                                    <YAxis />

                                                    <Tooltip itemStyle={{ padding: 0, margin: 0, fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                        wrapperStyle={{ margin: 0, padding: 0, borderRadius: '6px' }}
                                                        separator={' '}
                                                        labelStyle={{ fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                        cursor={{ fill: '#ebebeb' }}


                                                    />
                                                    <Bar dataKey="PKR" fill="black" cursor={'pointer'} >
                                                        {
                                                            this.props.Salariesdata.map((value: any, index: any) => {
                                                                return (

                                                                    <Cell key={index} fill={index % 2 === 0 ? ("#e4b835") : ("#008141")} />
                                                                )
                                                            })
                                                        }
                                                    </Bar>

                                                </BarChart>

                                            </ResponsiveContainer>
                                        ) : (
                                                <div className='GraphLoaderMainDiv'>


                                                    <svg className="svg__loading" viewBox="0 0 311 120" version="1.1">
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <path d="M0,0 L0,119 L74.6161228,119 L74.6161228,0 L0,0 Z M78.7946257,0 L78.7946257,119 L153.410749,119 L153.410749,0 L78.7946257,0 Z M157.589251,0 L157.589251,119 L232.205374,119 L232.205374,0 L157.589251,0 Z M236.383877,0 L236.383877,119 L311,119 L311,0 L236.383877,0 Z" id="Rectangle-1-copy" fill="#FAFAFA" />
                                                            <rect className="svg__loading-new" x="0" y="74.618705" width="74.6161228" height="100%">
                                                                <animate id="animate__new-bar"
                                                                    attributeName="y"
                                                                    from="74.618705"
                                                                    to="74.618705"
                                                                    begin="0s"
                                                                    dur="1.5s"
                                                                    values="74.618705; 0; 74.618705; 74.618705; 74.618705"
                                                                    keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                    keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                    calcMode="spline"
                                                                    repeatCount="indefinite" />
                                                            </rect>


                                                            <rect className="svg__loading-open" x="78.7946257" y="37" width="74.6161228" height="100%">
                                                                <animate id="animate__open-bar"
                                                                    attributeName="y"
                                                                    from="37"
                                                                    to="37"
                                                                    begin="0.1s"
                                                                    dur="1.5s"
                                                                    values="37; 0; 37; 37; 37"
                                                                    keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                    keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                    calcMode="spline"
                                                                    repeatCount="indefinite" />
                                                            </rect>
                                                            <rect className="svg__loading-won" x="157.589251" y="57.3021583" width="74.6161228" height="100%">
                                                                <animate id="animate__won-bar"
                                                                    attributeName="y"
                                                                    from="57.3021583"
                                                                    to="57.3021583"
                                                                    begin="0.2s"
                                                                    dur="1.5s"
                                                                    values="57.3021583; 0; 57.3021583; 57.3021583; 57.3021583"
                                                                    keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                    keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                    calcMode="spline"
                                                                    repeatCount="indefinite" />
                                                            </rect>
                                                            <rect className="svg__loading-lost" x="236.383877" y="97.3093525" width="74.6161228" height="100%">
                                                                <animate id="animate__lost-bar"
                                                                    attributeName="y"
                                                                    from="97.3093525"
                                                                    to="97.3093525"
                                                                    begin="0.3s"
                                                                    dur="1.5s"
                                                                    values="97.3093525; 0; 97.3093525; 97.3093525; 97.3093525"
                                                                    keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                    keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                    calcMode="spline"
                                                                    repeatCount="indefinite" />
                                                            </rect>
                                                        </g>
                                                    </svg>
                                                </div>

                                            )
                                    }

                                </div>

                            </div>






                        </div>
                    </div>
                    <div className={`${this.props.ShowModal ? 'MatDrawer' : null}`}>

                        <Drawer
                            docked={false}

                            open={this.props.ShowModal}
                            onRequestChange={this.props.ModalCloseFunc}
                            openSecondary={true}
                        >

                            {
                                this.props.ShowModal ? (
                                    <div>
                                        <p className='modalHeading'>Update Salary</p>
                                        <span className='UAttDate'>
                                            {
                                                this.props.paramDate ?
                                                    (moment(this.props.paramDate).format("MMMM YYYY"))

                                                    : null
                                            }
                                        </span>

                                        <TextField
                                            hintText="Enter Basic Salary"
                                            floatingLabelText="Basic Salary:"
                                            floatingLabelStyle={{ color: 'black' }}
                                            floatingLabelFocusStyle={{ color: 'black', fontFamily: 'NexaLight' }}
                                            underlineStyle={{ color: '#008141' }}
                                            underlineFocusStyle={{ color: '#008141' }}
                                            underlineDisabledStyle={{ color: '#008141' }}
                                            underlineShow={true}
                                            className='updateSalTF1'
                                            onChange={this.props.bsalprop}
                                            type={'number'}

                                            defaultValue={this.props.MonthDetailObject ? this.props.MonthDetailObject.currentSalary ? this.props.MonthDetailObject.currentSalary : null : null}
                                        />

                                        <TextField
                                            hintText="Enter Bonus"
                                            floatingLabelText="Bonus:"
                                            floatingLabelStyle={{ color: 'black' }}
                                            floatingLabelFocusStyle={{ color: 'black', fontFamily: 'NexaLight' }}
                                            underlineStyle={{ color: '#008141' }}
                                            underlineFocusStyle={{ color: '#008141' }}
                                            underlineDisabledStyle={{ color: '#008141' }}
                                            underlineShow={true}
                                            className='updateSalTF1'
                                            onChange={this.props.bonusprop}
                                            type={'number'}
                                            defaultValue={this.props.MonthDetailObject ? this.props.MonthDetailObject.totalBonus ? this.props.MonthDetailObject.totalBonus : null : null}

                                        />

                                        <RaisedButton label="UPDATE" fullWidth={true} backgroundColor='#008141' className='UPAttBtnND'
                                            onClick={this.props.FinalUpdateFunc}
                                            disabled={this.props.UpdateAttendanceBtnDisplay}

                                            style={{ color: 'white' }} type="submit" />

                                    </div>
                                ) : null
                            }


                        </Drawer>
                    </div>


                </div>





                <Footer FooterClass='ResgisterFooter' />
            </div >
        );
    }
}

export default EmployeeDashboard;
