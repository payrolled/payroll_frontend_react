import { Card } from 'material-ui/Card';

import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router'


import * as React from 'react';
import Footer from '../footer/footer';
import './setpasswordSuccess.css';



class SetPasswordSuccess extends React.Component {
    public changeRouteToLogin = () => {
        browserHistory.replace('/')
    }
    public render() {
        return (
            <div className='RegisterMainDiv backdiv'>
                <div className='RegisterMainDivChild1'>
                    <Card className='CardMainDiv'>
                        <div>
                            <p className='centerSuccess'> Your password has successfully been updated</p>
                            <RaisedButton label='Click here to login' backgroundColor='#008141' onClick={this.changeRouteToLogin} className='centerSuccess2 registerbuttonstyle'/>
                        </div>

                    </Card>

                </div>

               <Footer FooterClass='LoginRegiterFooter'/>

            </div>
        );
    }
}

export default SetPasswordSuccess;
