
import { Card } from 'material-ui/Card';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import * as moment from 'moment'
import { browserHistory } from 'react-router'

import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import Tab from '../tab/tab'

import * as React from 'react';
import './emloyeeDetail.css';

type Props = Partial<{
    Month: any,
    RoleProp: any,
    Year: any,
    IncDateFunc: any,
    RemovePhoto: any,
    changeRouteToEditP: any,
    setImageProp: any,
    employeeDataProp: any,
    ChangeRouteToAttendanceComponentFunc: any,
    ChangeRouteToPayslipComponentFunc: any
    DecDateFunc: any,
    ImageVal: any,
    TabChDash: any
}>


const styles: any = {
    exampleImageInput: {
        borderRadius: '0px',
        height: '100%',
        left: '0',
        opacity: '0',
        position: 'relative',
        right: '0',
        top: '-20px',
        width: '100%',
    }
}
class EmployeeDetailComponent extends React.Component<Props>{
    constructor(props: any) {
        super(props)
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }



    public ChangeRouteToProfile = () => {
        // a
    }

    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                <Sidebar RoleProp = {this.props.RoleProp}/>

                    {

                        this.props.employeeDataProp === undefined ? (
                            <div className='sideMainBody singleEmployeeSideBodyHeight'>
                                <div className='sideMainBodyonlyChild'>
                                    <div className='singleEmployeeTogglerLine'>
                                        <div className="animated-background-toggleLine EmpDetailLoader">
                                            .
                                        </div>
                                    </div>
                                </div>



                                <Tab activeProfile={true} activeDash={false} activeAtt={false} activePay={false}
                                    TabChPay={this.props.ChangeRouteToPayslipComponentFunc} AttChPay={this.props.ChangeRouteToAttendanceComponentFunc}
                                    TabChProfile={null}
                                    TabChDash={this.props.TabChDash}

                                />



                                <div className='sideMainBodyEmployeeDetailSecondChild'>

                                    <div className='sideMainBodyEmployeeDetailSecondChildChild1'>
                                        <Card className='imageCard'>
                                            <div className="timeline-itemtable">
                                                <div className="animated-background-employeeDataDP">
                                                    <div>.</div>
                                                </div>
                                            </div>
                                        </Card>
                                        {
                                            this.props.RoleProp ?
                                                this.props.RoleProp.role === 'company' ? (
                                                    <div className='singleEmployeeTogglerLine'>
                                                        <div className="animated-background-ParaNextToImage">
                                                            .
                                                        </div>
                                                    </div>
                                                ) :
                                                    null : null
                                        }


                                        <div className='singleEmployeeTogglerLine'>
                                            <div className="animated-background-ParaNextToImage">
                                                .
                                                        </div>
                                        </div>




                                    </div>

                                    <div className='sideMainBodyEmployeeDetailSecondChildChild2'>
                                        <Card style={{ padding: '5px' }} className='tableCard'>
                                            <div className="timeline-itemtable">
                                                <div className="animated-backgroundtable facebooktable">
                                                    <div className="background-maskertable header-toptable">.</div>
                                                    <div className="background-maskertable header-lefttable">.</div>
                                                    <div className="background-maskertable header-righttable">.</div>
                                                    <div className="background-maskertable header-bottomtable">.</div>
                                                    <div className="background-maskertable subheader-lefttable">.</div>
                                                    <div className="background-maskertable subheader-righttable">.</div>
                                                    <div className="background-maskertable subheader-bottomtable">.</div>
                                                    <div className="background-maskertable content-toptable">.</div>
                                                    <div className="background-maskertable content-first-endtable">.</div>
                                                    <div className="background-maskertable content-second-linetable">.</div>
                                                    <div className="background-maskertable content-second-endtable">.</div>
                                                    <div className="background-maskertable content-third-linetable">.</div>
                                                    <div className="background-maskertable content-third-endtable">.</div>
                                                </div>
                                            </div>
                                        </Card>
                                    </div>
                                </div>
                            </div>
                        ) : null
                    }
                    {

                        this.props.employeeDataProp ? (
                            <div className='sideMainBody singleEmployeeSideBodyHeight'>
                                <div className='sideMainBodyonlyChild'>
                                    <div className='sideMainBodyChild1'>
                                       



                                        <div className='sideMainBodyonlyChild'>
                                            <div className='AttendancesideMainBodyChild1'>
                                                <h1 className='DashboardMainHeadND'>
                                                    {
                                                        this.props.employeeDataProp[0].name
                                                    }
                                                </h1>
                                            </div>


                                            <div className='attendanceHeadingDiv'>
                                                {
                                                    this.props.RoleProp ?
                                                        this.props.RoleProp.role === 'company' ? (
                                                            <IconMenu
                                                                iconButtonElement={
                                                                    <i className="material-icons attendanceOptionIcon">more_vert</i>}
                                                                targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                                                                anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                                                                style={{ boxShadow: '0px !important' }} className='EditProfileMenuIcon'

                                                            >


                                                                <MenuItem className='ArchivesTogItm' onClick={this.props.changeRouteToEditP}>Edit</MenuItem>

                                                            </IconMenu>
                                                        ) : null : null
                                                }
                                            </div>



                                        </div>





                                        
                                    </div>

                                </div>
                                <Tab activeProfile={true} activeDash={false} activeAtt={false} activePay={false}
                                    TabChPay={this.props.ChangeRouteToPayslipComponentFunc} AttChPay={this.props.ChangeRouteToAttendanceComponentFunc}
                                    TabChDash={this.props.TabChDash}
                                    TabChProfile={null}
                                />

                                <div className='sideMainBodyEmployeeDetailSecondChild'>

                                    <div className='sideMainBodyEmployeeDetailSecondChildChild1 changPhotoTC'>
                                        <Card className='imageCard empdetcard'>
                                            {
                                                this.props.RoleProp ?
                                                    this.props.RoleProp.role === 'employee' ?
                                                        this.props.employeeDataProp[0].imageUrl ?
                                                            (
                                                                <i className="material-icons" onClick={this.props.RemovePhoto}>
                                                                    remove_circle_outline
                                                                </i>
                                                            ) : null : null : null
                                            }

                                            < img src={this.props.employeeDataProp[0].imageUrl ? `${this.props.employeeDataProp[0].imageUrl}` : require('../../images/avatar.jpg')} className='employeeDetailPic' />
                                        </Card>



                                        {
                                            this.props.RoleProp ?
                                                this.props.RoleProp.role === 'company' ? (
                                                    <p className='firstpara'>{this.props.employeeDataProp[0].name}</p>
                                                ) : (
                                                        null
                                                    ) : null
                                        }
                                        {
                                            this.props.RoleProp ?
                                                this.props.RoleProp.role === 'company' ? (
                                                    <p className='secondpara'>{this.props.employeeDataProp[0].group}</p>

                                                ) : (
                                                        <div className='ChangePicMainDiv'>
                                                            <button className='addImBtn'>CHANGE PHOTO<input type="file" style={styles.exampleImageInput} onChange={this.props.setImageProp} /></button>
                                                        </div>
                                                        



                                                    ) : null
                                        }


                                    </div>

                                    <div className='sideMainBodyEmployeeDetailSecondChildChild2'>
                                        <Card style={{ padding: '5px' }} className='tableCard'>
                                            <table>
                                                <tr>
                                                    <td className='firsttd'>User Name</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].name}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Employee ID</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].employeeId ? this.props.employeeDataProp[0].employeeId : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Desktime ID</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].serviceId ? this.props.employeeDataProp[0].serviceId : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Email Address</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].email ? this.props.employeeDataProp[0].email : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Account Number</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].accountNumber ? this.props.employeeDataProp[0].accountNumber : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Bank Title</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].bankTitle ? this.props.employeeDataProp[0].bankTitle : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Address</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].address ? this.props.employeeDataProp[0].address : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Cell Number</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].cellNumber ? this.props.employeeDataProp[0].cellNumber : '-'}</td>
                                                </tr>
                                                <tr>
                                                    <td className='firsttd'>Gender</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].gender ? this.props.employeeDataProp[0].gender : '-'}</td>
                                                </tr>

                                                <tr>
                                                    <td className='firsttd'>Current Salary</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].salary ? this.props.employeeDataProp[0].salary : '-'}</td>
                                                </tr>


                                                <tr>
                                                    <td className='firsttd'>Working Hours</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].workingHours ? this.props.employeeDataProp[0].workingHours : '-'}</td>
                                                </tr>


                                                 <tr>
                                                    <td className='firsttd'>Half Day Hours</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].halfDayHours ?new Date((this.props.employeeDataProp[0].halfDayHours*3600) * 1000).toISOString().substr(11, 8) : '-'}</td>
                                                </tr>

                                                 <tr>
                                                    <td className='firsttd'>Productive Hours</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].productiveHours ? this.props.employeeDataProp[0].productiveHours : '-'}</td>
                                                </tr>

                                                  <tr>
                                                    <td className='firsttd'>Paid Leaves</td>
                                                    <td className='secondtd'>{this.props.employeeDataProp[0].noOfPaidLeaves ? this.props.employeeDataProp[0].noOfPaidLeaves : '-'}</td>
                                                </tr>
                                            </table>
                                        </Card>
                                    </div>
                                </div>
                            </div>
                        ) : null
                    }

                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>
        );
    }
}

export default EmployeeDetailComponent;
