import { Card } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import LinearProgress from 'material-ui/LinearProgress';

import * as React from 'react';
import Footer from '../footer/footer'
import './timerTool.css';



type Props = Partial<{
    nav1: any,
    accountsightDivFunc: any,
    hubstaffDivFunc: any,
    timecampDivFunc: any,
    todoDivFunc: any,
    togglDivFunc: any,
    DesktimeDivFunc: any,
    CarMainDivOpacityProp: any,
    LoaderDisplayProp: any
}>
class TimerTool extends React.Component<Props>{
    public constructor(props: any) {
        super(props);

    }



    public render() {
        return (
            <div className='RegisterMainDiv backdiv'>

                <div className='RegisterMainDivChild1'>
                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }
                    <Card className='CardMainDiv'>
                        <div className='PayRollLogoDiv'>
                            <img src={require('../../images/payrolllogoLogin.png')} />
                        </div>
                        <div className='GreetingDiv'>
                            <p>Welcome. <span>...!</span></p>
                        </div>
                        <div className='GreetingDiv'>
                            <p>Please select one of the following</p>
                        </div>
                        <div className='CheckBoxDiv' style={{ opacity: this.props.CarMainDivOpacityProp }}>
                            <div className='checkchild1' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.hubstaffDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon1ApiTimer">info_outline</i>
                                </IconButton>
                                <label>
                                    <img src={require('../../images/check1.png')} className='firstImage' />
                                </label>
                            </div>
                            <div className='checkchild2' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.DesktimeDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon2ApiTimer">info_outline</i>

                                </IconButton>
                                <label>
                                    <img src={require('../../images/check2.jpg')} className='desktimeIcon' />
                                </label>
                            </div >
                            <div className='checkchild3' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.togglDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon3ApiTimer">info_outline</i>

                                </IconButton>
                                <label>
                                    <img src={require('../../images/check3.jpg')} className='checkchild3img' />
                                </label>
                            </div>

                            <div className='checkchild4' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.accountsightDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon4ApiTimer">info_outline</i>

                                </IconButton>
                                <label>
                                    <img src={require('../../images/check4.jpg')} className='checkchild4img' />
                                </label>
                            </div>

                            <div className='checkchild5' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.timecampDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon5ApiTimer">info_outline</i>

                                </IconButton>
                                <label>
                                    <img src={require('../../images/check5.jpg')} className='checkchild4img' />
                                </label>
                            </div>

                            <div className='checkchild6' onClick={this.props.CarMainDivOpacityProp === '0.5' ? false : this.props.todoDivFunc}>
                                <IconButton className='infoIcon' tooltip="top-right" touch={true} tooltipPosition="top-left">
                                    <i className="material-icons timerMaterialIcon materialIcon6ApiTimer">info_outline</i>

                                </IconButton>
                                <label>
                                    <img src={require('../../images/check6.png')} className='checkchild5img' />
                                </label>
                            </div>



                        </div>
                    </Card>

                </div>

              <Footer FooterClass='LoginRegiterFooter'/>

            </div>
        );
    }
}

export default TimerTool;
