import axios from 'axios'
import { Card } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import TimePicker from 'material-ui/TimePicker';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
import RouteConfig from '../../routeconfig'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import './updateprofile.css';
const Config = { isProd: false }; // example config in your application
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

const styles: any = {
    bottom: '0',
    cursor: 'pointer',
    exampleImageInput: {
        borderRadius: '0px',
        left: '0',
        opacity: '0',
        position: 'absolute',
        right: '0',
        top: '0',
        width: '100%',
    }
}
interface ISTATE {
    SaveDisState: any,
    accountno: any,
    address: any,
    bonus: any,
    banktit: any,
    cellno: any,
    currsalary: any,
    employeeID: any,
    emailadd: any,
    designation: any,
    desktimeID: any,
    female: any,
    halfDayHours: any,
    image: any,
    name: any,
    male: any,
    paidleave: any,
    fullname: any,
    username: any,
    updateError: any,
    updateErrorText: any,
    updateSuccess: any,
    workinghours: any,
    productiveHours: any
}
type Props = Partial<{
    RoleProp: any,
    SaveButton: any,
    ChangeRouteToAttendanceComponentFunc: any,
    params: any,
    dateProp: any
}>



const UpdateEmployeeAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/updateEmployee` });
UpdateEmployeeAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

class UpdateProfile extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            SaveDisState: false,
            accountno: undefined,
            address: undefined,
            banktit: undefined,
            bonus: undefined,
            cellno: undefined,
            currsalary: undefined,
            designation: undefined,
            desktimeID: undefined,
            emailadd: undefined,
            employeeID: undefined,
            female: false,
            fullname: undefined,
            halfDayHours: undefined,
            image: null,
            male: true,
            name: undefined,
            paidleave: undefined,
            productiveHours: undefined,
            updateError: false,
            updateErrorText: undefined,
            updateSuccess: false,
            username: undefined,
            workinghours: undefined,
        }
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        delete AttendanceRouteObject.employeeName
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }


    public handleChange = (e: any) => {
        const reader = new FileReader();

        reader.onload = (e2: any) => {
            this.setState({ image: e2.target.result });
        };
        if (e.target.files[0] === undefined) {
            this.setState({
                image: this.state.image
            })
        }
        else {

            reader.readAsDataURL(e.target.files[0]);
        }


    }
    public clearImageState = () => {

        this.setState({ image: undefined });
    }



    public componentWillReceiveProps(nextProps: any) {

        if (nextProps.RoleProp) {
            this.setState({
                accountno: nextProps.RoleProp.employeeName.accountNumber,
                address: nextProps.RoleProp.employeeName.address,
                banktit: nextProps.RoleProp.employeeName.bankTitle,
                bonus: nextProps.RoleProp.employeeName.bonus,
                cellno: nextProps.RoleProp.employeeName.cellNumber,
                currsalary: nextProps.RoleProp.employeeName.salary,
                designation: nextProps.RoleProp.employeeName.designation,
                desktimeID: nextProps.RoleProp.employeeName.serviceId,
                emailadd: nextProps.RoleProp.employeeName.email,
                employeeID: nextProps.RoleProp.employeeName.employeeId,
                female: nextProps.RoleProp.employeeName.gender === "female" ? true : false,
                fullname: nextProps.RoleProp.employeeName.name,
                halfDayHours: nextProps.RoleProp.employeeName.halfDayHours,
                image: nextProps.RoleProp.employeeName.imageUrl ? nextProps.RoleProp.employeeName.imageUrl : null,
                male: nextProps.RoleProp.employeeName.gender === "male" ? true : false,
                paidleave: nextProps.RoleProp.employeeName.noOfPaidLeaves,
                productiveHours: nextProps.RoleProp.employeeName.productiveHours,
                username: nextProps.RoleProp.employeeName.userName,
                workinghours: nextProps.RoleProp.employeeName.workingHours,

            })
        }
    }

    public nameChange = (event: any, ) => {
        this.setState({
            fullname: event.target.value
        })
    }
    public UnameChange = (event: any, ) => {
        this.setState({
            username: event.target.value
        })
    }
    public EmpIdChange = (event: any, ) => {
        this.setState({
            employeeID: event.target.value
        })
    }
    public DeskIdChange = (event: any, ) => {
        this.setState({
            desktimeID: event.target.value
        })
    }
    public EmailAddChange = (event: any, ) => {
        this.setState({
            emailadd: event.target.value
        })
    }
    public AccNoChange = (event: any, ) => {
        this.setState({
            accountno: event.target.value
        })
    }
    public BtitChange = (event: any, ) => {
        this.setState({
            banktit: event.target.value
        })
    }
    public AddChange = (event: any, ) => {
        this.setState({
            address: event.target.value
        })
    }
    public CellNoChange = (event: any, ) => {
        this.setState({
            cellno: event.target.value
        })
    }
    public PaidLChange = (event: any, ) => {
        this.setState({
            paidleave: event.target.value
        })
    }
    public DesigChange = (event: any, ) => {
        this.setState({
            designation: event.target.value
        })
    }
    public CurrSChange = (event: any, ) => {
        this.setState({
            currsalary: event.target.value
        })
    }


    public BonusChange = (event: any, ) => {
        this.setState({
            bonus: event.target.value
        })
    }
    public MaleChange = (event: any, ) => {
        this.setState({
            female: false,
            male: true
        })
    }
    public FemaleChange = (event: any, ) => {
        this.setState({
            female: true,
            male: false,
        })
    }

    public WorkingHours = (event: any, ) => {
        this.setState({
            workinghours: event.target.value
        })
    }
    public halfDayHours = (event: any, time: any) => {
        debug.log(moment(time).format('HH:mm'),'event---event')
        const HalfDaytime = moment(time).format('HH:mm');
        debug.log(moment(HalfDaytime).seconds(),'msec')
        const a = HalfDaytime.split(':');

        const seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 ;
        debug.log(seconds/3600,'sec----');
        this.setState({
            halfDayHours: seconds/3600
        })
    }

    public SaveButton = () => {
        debug.log(this.state.halfDayHours,'----half')
        const DateForUpEmp = this.props.dateProp ? this.props.dateProp : undefined
        this.setState({
            SaveDisState: true
        })
        const employeeID = this.props.RoleProp.employeeName._id

        const finalObjUEmp = {
            accountNumber: this.state.accountno,
            address: this.state.address,
            bankTitle: this.state.banktit,
            bonus: this.state.bonus ? Number(this.state.bonus) : 0,
            cellNumber: this.state.cellno,
            designation: this.state.designation,
            desktimeId: this.state.desktimeID,
            email: this.state.emailadd,
            employeeId: this.state.employeeID,
            gender: this.state.female ? 'female' : 'male',
            halfDayHours: this.state.halfDayHours,
            imagePath: this.state.image,
            name: this.state.fullname,
            noOfPaidLeaves: this.state.paidleave,
            productiveHours: this.state.productiveHours,
            salary: Number(this.state.currsalary),
            userName: this.state.username,
            workingHours: this.state.workinghours,
        }

        if (DateForUpEmp) {

            UpdateEmployeeAPI.put(`/${employeeID}/${DateForUpEmp}`, finalObjUEmp).then((success2: any) => {
                const obj = {
                    name: this.props.RoleProp.name,
                    role: this.props.RoleProp.role,
                }
                browserHistory.replace({
                    pathname: `/salarysheet/${DateForUpEmp}`,
                    state: obj
                })
            })
                .catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }
                    else if (!err.response) {
                        this.setState({
                            SaveDisState: false
                        })
                    }
                })
        }
        else {
            UpdateEmployeeAPI.put(`/${employeeID}`, finalObjUEmp).then((success2: any) => {
                const obj = {
                    name: this.props.RoleProp.name,
                    role: this.props.RoleProp.role,
                }
                browserHistory.replace({
                    pathname: `/employees/${this.props.RoleProp.employeeName._id}`,
                    state: obj
                })
            })
                .catch((err: any) => {
                    if (err.response) {

                        if (err.response.status === 401) {
                            localStorage.removeItem('user')
                            sessionStorage.clear()
                            browserHistory.replace('/')
                        }
                    }
                    else if (!err.response) {
                        this.setState({
                            SaveDisState: false
                        })
                    }
                })
        }

    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public productiveHours = (event: any) => {
        this.setState({
            productiveHours: event.target.value
        })
    }

    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <Sidebar RoleProp={this.props.RoleProp} />
                    <div className='sideMainBody'>
                        <div className='sideMainBodyonlyChild'>

                            {
                                this.props.RoleProp ? (

                                    <div className='sideMainBodyChild1 employeesviewMobileView'>
                                        <h1 className='ViewEmployeesHeading ViewEmpHMob'>Edit Employee</h1>


                                        <RaisedButton label="SAVE" backgroundColor='#008141' className='btnFontColor registerbuttonstyle ViewSalaryButton' onClick={this.SaveButton} disabled={this.state.SaveDisState} />


                                    </div>

                                ) : (

                                        <div className="animated-background attendaceHeadingLoader">
                                            .
                                        </div>
                                    )
                            }
                        </div>




                        <div className='payslipchild2Parent'>
                            <div className='AddNewEmpoyeeChild1'>
                                <Card className='cardStyle'>
                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Full Name</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                hintText="Enter Full Name"
                                                hintStyle={{ color: '#747c83' }}
                                                className='textFieldWidth' underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                value={this.state.fullname}
                                                onChange={this.nameChange}

                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>User Name</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                hintText="Enter User Name"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                value={this.state.username}
                                                onChange={this.UnameChange}

                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Employee ID</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.employeeID}
                                                onChange={this.EmpIdChange}

                                                hintText="Enter Employee ID"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>




                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Desktime ID</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.desktimeID}
                                                onChange={this.DeskIdChange}
                                                disabled={true}
                                                hintText="Enter Desktime ID"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Email Address</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                value={this.state.emailadd}
                                                onChange={this.EmailAddChange}
                                                disabled={true}
                                                hintText="Enter Email Address"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Account Number</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.accountno}
                                                onChange={this.AccNoChange}

                                                hintText="Enter Account Number"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Bank Title</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                value={this.state.banktit}
                                                onChange={this.BtitChange}

                                                hintText="Enter Bank Title"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Address</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                value={this.state.address}
                                                onChange={this.AddChange}

                                                hintText="Enter Address"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Cell Number</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.cellno}
                                                onChange={this.CellNoChange}

                                                hintText="Enter Cell Number"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Paid Leaves</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.paidleave}
                                                onChange={this.PaidLChange}

                                                hintText="Paid Leaves Allowed"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Gender</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <label><input type='radio' checked={this.state.male} value='male'
                                                onChange={this.MaleChange.bind(this, 'male')}

                                            />Male</label>
                                            <label><input type='radio' checked={this.state.female} value='female'
                                                onChange={this.FemaleChange.bind(this, 'female')}

                                            />Female</label>

                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Designation</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                value={this.state.designation}
                                                hintText="Enter Designation"
                                                onChange={this.DesigChange}

                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Current Salary</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.currsalary}
                                                hintText="Enter Current Salary"
                                                onChange={this.CurrSChange}

                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>



                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Working Hours</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.workinghours}
                                                hintText="Enter Working Hours"
                                                onChange={this.WorkingHours}

                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Half Day Hours</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TimePicker
                                                format="24hr"
                                                hintText="24hr Format"
                                                onChange={this.halfDayHours}
                                            />
                                            {/* <TextField
                                                type='number'
                                                value={this.state.halfDayHours}
                                                hintText="Enter Half Day Hours"
                                                onChange={this.halfDayHours}

                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            /> */}
                                        </div>
                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Productive Hours</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                value={this.state.productiveHours}
                                                hintText="Enter Productive Hours"
                                                onChange={this.productiveHours}

                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    {
                                        this.props.dateProp ? (
                                            <div className='CardMainParent'>
                                                <div className='CardMainParentChild1'>
                                                    <p>Bonus</p>
                                                </div>
                                                <div className='CardMainParentChild2'>
                                                    <TextField
                                                        type='number'
                                                        value={this.state.bonus}
                                                        hintText="Enter Bonus"
                                                        onChange={this.BonusChange}

                                                        className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                    />
                                                </div>

                                            </div>
                                        ) : null
                                    }
                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1SmallError'>
                                            {
                                                this.state.updateSuccess ? (
                                                    <small>* The record has been updated</small>
                                                ) : this.state.updateError ? (
                                                    <small className='UpdateErrorColor'>{this.state.updateErrorText}</small>
                                                ) : null
                                            }
                                        </div>
                                    </div>

                                </Card>
                            </div>
                            {
                                !this.props.dateProp ? (
                                    <div className='AddNewEmpoyeeChild2'>
                                        <Card>
                                            <div className='ImageUploadMainDiv'>
                                                <div className='ImageUploadMainDivChild1'>
                                                    <div className='AfterUploadImageDiv' style={{ backgroundImage: this.state.image }}>
                                                        {
                                                            (this.state.image) ? (

                                                                <img src={this.state.image} className='uploadImageDiv' />
                                                            ) :
                                                                null
                                                        }
                                                    </div>
                                                </div>
                                                <div className='ImageUploadMainDivChild2'>


                                                    <RaisedButton label="BROWSE" backgroundColor='rgb(0, 129, 65)' className='btnFontColor registerbuttonstyle browsebtn' style={{ color: 'white', width: '2px !important', display: 'inline-block' }} containerElement='label'>
                                                        <input type="file" style={styles.exampleImageInput} onChange={this.handleChange} value={this.state.image === null ? '' : ''} />
                                                    </RaisedButton><span className='addnewspan'>To Add New</span>
                                                    <p>- OR -</p>
                                                    <RaisedButton label="REMOVE PHOTO" backgroundColor='rgb(0, 129, 65)' className='btnFontColor registerbuttonstyle removephotobtn' style={{ color: 'white', width: '2px !important', display: 'inline-block' }} onClick={this.clearImageState} />

                                                </div>


                                            </div>
                                        </Card>
                                    </div>
                                ) : null
                            }
                        </div>



                    </div>

                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div >
        );
    }
}

export default UpdateProfile;
