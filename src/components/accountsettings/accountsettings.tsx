import { Card } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import TextField from 'material-ui/TextField';
import * as moment from 'moment'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import './accountsettings.css';

import * as React from 'react';
import { browserHistory } from 'react-router'





type Props = Partial<{
    AccSettEmail: any,
    CurrPassCh: any,
    CnfPassCh: any,
    NewPassCh: any,
    RoleProp: any,
    SaveButton: any,
    SaveDisState: any,
    updateSuccess: any,
    updateSuccesstext: any
}>





class AccountSetting extends React.Component<Props> {
    constructor(props: any) {
        super(props)
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        delete AttendanceRouteObject.employeeName
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }




    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <Sidebar RoleProp = {this.props.RoleProp}/>
                    <div className='sideMainBody attendanceSidebody AccSettSideB'>



                        <div className='sideMainBodyonlyChild'>



                            <div className='AccSettMainBodyChild1'>
                                <h1 className='ViewEmployeesHeading AddEmployeeHeadingPostion addemployeeHeadingColor'>ACCOUNT SETTINGS</h1>
                                <RaisedButton label="UPDATE" backgroundColor='#008141' className='btnFontColor SaveBtn registerbuttonstyle' style={{ color: 'white', width: '2px !important', display: 'inline-block' }} onClick={this.props.SaveButton}
                                    disabled={this.props.SaveDisState}
                                />
                            </div>



                        </div>




                        <div className='payslipchild2Parent'>
                            <div className='AddNewEmpoyeeChild1 accSettCard'>
                                <Card className='cardStyleAS'>
                                    <div className='CardMainParentAccSet EmailParaMainDIv'>
                                        <div className='CardMainParentChild1AS'>
                                            <p>Email Address</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            {
                                                this.props.AccSettEmail ? (
                                                    <p className='AccSettEmailPara'>{this.props.AccSettEmail}</p>
                                                ) : (

                                                        <div className="facebookWave">
                                                            <div>.</div>
                                                            <div>.</div>
                                                            <div>.</div>
                                                        </div>
                                                    )
                                            }
                                        </div>

                                    </div>


                                    <div className='CardMainParentAccSet'>
                                        <div className='CardMainParentChild1AS'>
                                            <p>Current Password</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                hintText="Enter Current Password"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                type='password'
                                                onChange={this.props.CurrPassCh}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParentAccSet'>
                                        <div className='CardMainParentChild1AS'>
                                            <p>New Password</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='password'
                                                hintText="Enter New Password"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                onChange={this.props.NewPassCh}
                                            />
                                        </div>

                                    </div>




                                    <div className='CardMainParentAccSet'>
                                        <div className='CardMainParentChild1AS'>
                                            <p>Confirm New Password</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                hintText="Confirm New Password"
                                                type='password'
                                                onChange={this.props.CnfPassCh}
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>
                                   
                                </Card>
                            </div>

                        </div>


                    </div>

                    <Snackbar
                        open={this.props.updateSuccess}
                        message={this.props.updateSuccesstext}
                        contentStyle={{ textAlign: 'center' }}
                        autoHideDuration={4000}

                    />

                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div >
        );
    }
}

export default AccountSetting;
