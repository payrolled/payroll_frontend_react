import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Debugger } from 'ts-debug';
const Config = { isProd: false }; // example config in your application
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');

type Props = Partial<{
    RoleProp: any,
    active: string
}>

class Sidebar extends React.Component<Props> {
    constructor(props: any) {
        super(props)
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }

    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }

    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public render() {
        const { active } = this.props;
        debug.log(this.props,'props====<llsadldp')
        return (
            <div className='sidebar'>
                {this.props.RoleProp ? this.props.RoleProp.role === 'company' ? (
                    <ul className='sidebarList'>
                        <li className={`sidebarListFirstTwoLi ${active === 'dashboard' ? 'employeesactivecolor' : '' }`} onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                        <li className={`sidebarListFirstTwoLi ${active === 'employees' ? 'employeesactivecolor' : '' }`} onClick={this.ChangeRouteToEmployees}><i className="material-icons EmployeeIcon">supervisor_account</i>Employees</li>
                        <li className={`sidebarListFirstTwoLi ${active === 'attendance' ? 'employeesactivecolor' : '' }`} onClick={this.ChangeRouteToAttendance}><i className="material-icons EmployeeIcon">schedule</i>Attendance</li>
                        <li className={`sidebarListFirstTwoLi ${active === 'settings' ? 'employeesactivecolor' : '' }`} onClick={this.ChangeRouteToSettings}><i className="material-icons SettingsIcon">pie_chart</i>Salary Sheet</li>
                    </ul>) : this.props.RoleProp.role === 'employee' ? (
                        <ul className='sidebarList'>
                            <li className='sidebarListFirstTwoLi employeesactivecolor NoBorderBottom' onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                        </ul>) : null : null}
            </div>

        );
    }
}

export default Sidebar;
