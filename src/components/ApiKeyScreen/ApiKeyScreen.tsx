import { Card } from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';
import RaisedButton from 'material-ui/RaisedButton';
import * as React from 'react';
import Footer from '../footer/footer'
import './ApiKeyScreen.css';


type Props = Partial<{
    apikeysubmitbuttonProp: any,
    apikeyPropsFunc: any,
    CarMainDivOpacityProp: any,
    LoaderDisplayProp: any,
    errorVisibilityProp: any,
    errordisplayProp: any,

}>
class ApiKeyScreen extends React.Component<Props> {
    public render() {
        return (
            <div className='RegisterMainDiv backdiv'>
                <div className='RegisterMainDivChild1'>
                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }
                    <Card className='CardMainDiv'>
                        <div className='PayRollLogoDiv'>
                            <img src={require('../../images/payrolllogoLogin.png')} />
                        </div>
                        <div className='GreetingDiv'>
                            <p>Welcome. <span>...!</span></p>
                        </div>
                        <div className='GreetingDiv'>
                            <p>You've selected <b>DeskTime</b>, Please enter API Key.</p>
                        </div>
                        <div className='TextFieldDivs'>
                            <input type='text' placeholder='Enter Your DeskTime API Key.' className='ApiTextFieldsStyle' onChange={this.props.apikeyPropsFunc} />
                        </div>

                        <div className='smalldiv smallcolor apikeysmall' style={{ visibility: this.props.errorVisibilityProp }}>
                            <small>{this.props.errordisplayProp}</small>
                        </div>
                        <div className='TextFieldDivsRaisedButton'>
                            <RaisedButton label="DONE" fullWidth={true} disabled={(this.props.LoaderDisplayProp === 'hidden') ? false : true} backgroundColor='#008141' className='ResgisterTextFieldsStyleLastBtnRaisedButton registerbuttonstyle btnFontColor' onClick={this.props.apikeysubmitbuttonProp} />
                        </div>


                    </Card>

                </div>

               <Footer FooterClass='LoginRegiterFooter'/>

            </div>
        );
    }
}

export default ApiKeyScreen;
