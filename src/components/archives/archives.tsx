import { Card, CardMedia } from 'material-ui/Card';
import * as moment from 'moment'
import { browserHistory } from 'react-router'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'

import './archives.css';

import * as React from 'react';


type Props = Partial<{
    EmployeeArray: any,
    InviteBtnDisProp: any,
    RoleProp: any,
    changeRToArch: any,
    ArchiveFunc: any,
    archiveLoading: any,
}>

let CardStyleCounter: any = 0;
class Archives extends React.Component<Props> {
    constructor(props: any) {
        super(props)
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }

    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }



    public ChangeRouteToAttendance = (value: any) => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        const userid = value._id;
        browserHistory.push({
            pathname: `/employees/${userid}/attendance/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }

    public ChangeRouteToPayslip = (value: any) => {
        const userid = value._id;
        const obj = this.props.RoleProp;
        delete obj.CurrentMonthFormat
        browserHistory.push({
            pathname: `/employees/${userid}/payslip`,
            state: obj
        })

    }

    public ChangeRouteToAddNewEmployee = () => {
        browserHistory.push({
            pathname: '/AddEmployee',
            state: this.props.RoleProp
        })
    }

    public ChangeRouteToSingleEmployee = (value: any) => {
        const obj = this.props.RoleProp;

        browserHistory.push({
            pathname: `employees/${value._id}/dashboard`,
            state: obj
        })

    }
    public ChangeRouteToTakeAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public ChangeRouteToInvite = () => {
        browserHistory.push({
            pathname: '/invite',
            state: this.props.RoleProp
        })
    }
    public ChangeRouteToProfile = () => {
        // a
    }
    public render() {
        CardStyleCounter = 0;

        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} HideNavigationItemEmployees='toggler' />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null} ${this.props.InviteBtnDisProp ? ('DisableInviteDiv') : null}`}>
                    <Sidebar RoleProp={this.props.RoleProp} />
                    <div className='sideMainBody employessViewSideMainBodyPaddingBottom archiveheight'>

                        <div className='sideMainBodyonlyChild'>
                            <div className='sideMainBodyChild1 employeesviewMobileView'>
                                <h1 className='ViewEmployeesHeading ViewEmpHMob'>Archives</h1>
                            </div>
                            <div className='sideMainBodyChild2'>
                                {
                                    this.props.EmployeeArray === undefined ? (
                                        <div className='sideMainBodyChild2OnlyChild'>
                                            <div className='SalaryViewChildDiv1'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>
                                                            <div className="background-masker content-third-line dot">.</div>




                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv2'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv3'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv4'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>


                                        </div>
                                    ) : null
                                }


                                {
                                    this.props.EmployeeArray === undefined ? (
                                        <div className='sideMainBodyChild2OnlyChild'>
                                            <div className='SalaryViewChildDiv1'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>
                                                            <div className="background-masker content-third-line dot">.</div>




                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv2'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv3'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv4'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>


                                        </div>
                                    ) : null
                                }


                                {
                                    this.props.EmployeeArray === undefined ? (
                                        <div className='sideMainBodyChild2OnlyChild'>
                                            <div className='SalaryViewChildDiv1'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv2'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv3'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>
                                            <div className='SalaryViewChildDiv4'>
                                                <Card>


                                                    <div className="timeline-item">
                                                        <div className="animated-background facebook">

                                                            <div className="background-masker content-top dot">.</div>

                                                            <div className="background-masker content-second-line dot">.</div>

                                                            <div className="background-masker content-third-line dot">.</div>


                                                        </div>





                                                    </div>
                                                </Card>
                                            </div>


                                        </div>
                                    ) : null
                                }
                                <div className='sideMainBodyChild2OnlyChild'>
                                    {
                                        this.props.EmployeeArray ?
                                            this.props.EmployeeArray.map((value: any, index: any) => {

                                                CardStyleCounter = CardStyleCounter + 1;
                                                if (CardStyleCounter > 4) {
                                                    CardStyleCounter = 1;
                                                }

                                                return (
                                                    <div className={`SalaryViewChildDiv${CardStyleCounter} dynamicEmployeeClass`} key={index}>
                                                        <Card className='viewEmployeesCard'>
                                                            <CardMedia>
                                                                <span>
                                                                    <i className={`material-icons archiveIcon ${value.archiveLoading ? 'spinnerIconTrueArchive' : null}`} onClick={this.props.ArchiveFunc.bind(this, value._id, index)}>restore</i>
                                                                </span>
                                                                <img src={value.imageUrl ? `${value.imageUrl}` : require('../../images/avatar.jpg')} className={!value.imageUrl ? (`avatartImg`) : 'employeeDp'} onClick={this.ChangeRouteToSingleEmployee.bind(this, value)} />
                                                                <h1 className='CardHead mapCardHEd'>{value.name}</h1>
                                                                <p className='CardPara'>{value.group ? value.group : value.designation ? value.designation : '-'}</p>
                                                            </CardMedia>


                                                        </Card>
                                                    </div>
                                                )
                                            })
                                            : null
                                    }


                                </div>
                            </div>





                        </div>
                    </div>
                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>

        );
    }
}

export default Archives;
