
import { Card } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import TextField from 'material-ui/TextField';
import * as moment from 'moment'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'

import { browserHistory } from 'react-router'

import * as React from 'react';
import './AddEmployee.css';


const styles: any = {
    bottom: '0',
    cursor: 'pointer',
    exampleImageInput: {
        borderRadius: '0px',
        left: '0',
        opacity: '0',
        position: 'absolute',
        right: '0',
        top: '0',
        width: '100%',
    }
}


interface ISTATE {
    image: any
}


type Props = Partial<{
    AddEmployeFunProp: any,
    AddEmpBtnDis: any,
    FieldChange: any,
    RoleProp: any,
    clearImageState: any,
    femaleCheck: any,
    handleImageChange: any,
    imageProp: any,
    maleCheck: any,
    updateSuccess: any,
    updateErrorText: any,
    updateError: any,
    updateSuccesstext: any,
}>
class AddEmployeeComponent extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            image: null,

        }
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'AdminDashboard') {
            browserHistory.push({
                pathname: '/home',
                state: this.props.RoleProp
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            browserHistory.push({
                pathname: '/employees',
                state: this.props.RoleProp
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }




    public SignOut = () => {
        localStorage.removeItem('data')
        localStorage.removeItem('user')
        browserHistory.replace('/')
    }

    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }
    public render() {
        return (
            <div>

                <AppBarComponent RoleProp={this.props.RoleProp} HideNavigationItemEmployees='toggler' />
                <div className='DashboardMainBody'>
                <Sidebar RoleProp = {this.props.RoleProp}/>

                    <div className='sideMainBody'>
                        <div className='sideMainBodyChild1 employeesviewMobileView'>
                            <h1 className='ViewEmployeesHeading ViewEmpHMob'>Add Employee</h1>
                            <RaisedButton label="SAVE" backgroundColor='#008141' className='btnFontColor registerbuttonstyle ViewSalaryButton' disabled={this.props.AddEmpBtnDis} onClick={this.props.AddEmployeFunProp} />
                        </div>

                       
                        <div className='payslipchild2Parent AddEmployeeBody'>
                            <div className='AddNewEmpoyeeChild1'>
                                <Card className='cardStyle'>
                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Full Name</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                hintText="Enter Full Name"
                                                hintStyle={{ color: '#747c83' }}
                                                className='textFieldWidth' underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                onChange={this.props.FieldChange}
                                                name='name'
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>User Name</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                hintText="Enter User Name"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                                name='username'
                                                onChange={this.props.FieldChange}

                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Employee ID</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                name='employeeId'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Employee ID"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>




                                   


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Email Address</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                name='email'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Email Address"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Account Number</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                onChange={this.props.FieldChange}
                                                name='accNo'

                                                hintText="Enter Account Number"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Bank Title</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                name='banktit'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Bank Title"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Address</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                name='address'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Address"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Cell Number</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                name='cellno'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Cell Number"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Gender</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <label className='GenderFont'><input type='radio'
                                                onChange={this.props.FieldChange}
                                                checked={this.props.maleCheck}
                                                name='male'
                                            />Male</label>
                                            <label className='GenderFont'><input type='radio'
                                                onChange={this.props.FieldChange}
                                                checked={this.props.femaleCheck}
                                                name='female'
                                            />Female</label>

                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Designation</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='string'
                                                name='designation'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Designation"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141 ' }}
                                            />
                                        </div>

                                    </div>


                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1'>
                                            <p>Current Salary</p>
                                        </div>
                                        <div className='CardMainParentChild2'>
                                            <TextField
                                                type='number'
                                                name='salary'
                                                onChange={this.props.FieldChange}

                                                hintText="Enter Current Salary"
                                                className='textFieldWidth' hintStyle={{ color: '#747c83' }} underlineStyle={{ borderColor: '#3f4d58' }} underlineFocusStyle={{ borderColor: '#008141' }}
                                            />
                                        </div>

                                    </div>

                                    <div className='CardMainParent'>
                                        <div className='CardMainParentChild1SmallError'>
                                            {
                                                this.props.updateError ? (
                                                    <small className='UpdateErrorColor'>{this.props.updateErrorText}</small>
                                                ) : null
                                            }
                                        </div>
                                    </div>

                                   
                                </Card>
                            </div>

                            <div className='AddNewEmpoyeeChild2'>
                                <Card>
                                    <div className='ImageUploadMainDiv'>
                                        <div className='ImageUploadMainDivChild1'>
                                            <div className='AfterUploadImageDiv' style={{ backgroundImage: this.state.image }}>
                                                {
                                                    (this.props.imageProp) ? (

                                                        <img src={this.props.imageProp} />
                                                    ) :
                                                        null
                                                }
                                            </div>
                                        </div>
                                        <div className='ImageUploadMainDivChild2'>


                                            <RaisedButton label="BROWSE" backgroundColor='#008141 ' className='btnFontColor browsebtn registerbuttonstyle' style={{ color: 'white', width: '2px !important', display: 'inline-block' }} containerElement='label'>
                                                <input type="file" name='image' style={styles.exampleImageInput} onChange={this.props.handleImageChange} value={this.state.image === null ? '' : ''} />
                                            </RaisedButton><span className='addnewspan'>To Add New</span>
                                            <p>- OR -</p>
                                            <RaisedButton label="REMOVE PHOTO" backgroundColor='#008141 ' className='btnFontColor removephotobtn registerbuttonstyle' style={{ color: 'white', width: '2px !important', display: 'inline-block' }} onClick={this.props.clearImageState} />

                                        </div>


                                    </div>
                                </Card>
                            </div>
                        </div>
                    </div>
                    <Snackbar
                        open={this.props.updateSuccess}
                        message={this.props.updateSuccesstext}
                        contentStyle={{ textAlign: 'center' }}
                        autoHideDuration={2000}

                    />
                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>
        )
    }
}

export default AddEmployeeComponent;
