import { Card } from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';
import Footer from '../footer/footer'

import RaisedButton from 'material-ui/RaisedButton';
import * as React from 'react';
import './setPassword.css';
interface ISTATE {
    type: any
}
type Props = Partial<{
    EmailValue: any,
    CarMainDivOpacityProp: any,
    LoaderDisplayProp: any,
    PasswordValue: any,
    AgainPasswordValue: any,
    RegisterFuncProps: any,
    RegisterDisable: any,
    changeRouteToLogin: any,
    errordisplayProp: any,
    errorVisibilityProp: any,
    errorFlag: any,
    errorText: any
}>
class SetPassword extends React.Component<Props, ISTATE>{
    public render() {
        return (

            <div className='RegisterMainDiv backdiv'>

                <div className='RegisterMainDivChild1'>

                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }
                    {
                        this.props.errorFlag === false ? (
                            <Card className='CardMainDiv'>
                                <form>
                                    <div className='PayRollLogoDiv'>
                                        <img src={require('../../images/payrolllogoLogin.png')} />
                                    </div>



                                    <div className='TextFieldDivs'>
                                        <i className="material-icons TextFieldIcon">lock_outline</i>
                                        <input type='password' placeholder='Enter new password' className='ResgisterTextFieldsStyle' onChange={this.props.PasswordValue} />
                                    </div>
                                    <div className='TextFieldDivs'>
                                        <i className="material-icons TextFieldIcon">lock_outline</i>
                                        <input type='password' placeholder='Re-enter new Password' className='ResgisterTextFieldsStyle' onChange={this.props.AgainPasswordValue} />
                                    </div>

                                    <div className='smalldiv smallcolor' style={{ visibility: this.props.errorVisibilityProp }}>
                                        <small>{this.props.errordisplayProp}</small>
                                    </div>
                                    <div className='TextFieldDivsRaisedButton'>
                                        <RaisedButton label="UPDATE PASSWORD" disabled={this.props.RegisterDisable} fullWidth={true} backgroundColor='#008141' className='ResgisterTextFieldsStyleRaisedButton registerbuttonstyle registerbtnmargin btnFontColor' style={{ color: 'white' }} onClick={this.props.RegisterFuncProps} type="submit" />
                                    </div>

                                </form>



                            </Card>

                        ) : (

                                this.props.errorFlag === true ?

                                    (
                                        <Card className='CardMainDiv'>
                                            <div className='PayRollLogoDiv'>
                                                <img src={require('../../images/payrolllogoLogin.png')} />
                                            </div>


                                            <p className='errorparaAddPassword'>{this.props.errorText}</p>




                                        </Card>
                                    ) : (
                                        null
                                    )

                            )
                    }
                </div>

                <Footer FooterClass='LoginRegiterFooter' />

            </div>
        );
    }
}

export default SetPassword;
