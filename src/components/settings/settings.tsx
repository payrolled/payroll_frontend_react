import DropDownMenu from 'material-ui/DropDownMenu';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import * as React from 'react';
import { browserHistory } from 'react-router'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import SettingLoader from '../settingLoader/settingLoader'
import './settings.css';


type Props = Partial<{
    DecrementMonthPropFunc: any,
    IncrementMonthPropFunc: any,
    MonthProp: any,
    NextDisable: any,
    PreviousDisable: any,
    RoleProp: any,
    SettingsArrayProp: any,
    SettingEmptyTableProp: any,
    SettingLoaderProps: any,
    YearProp: any,
    changeRouteToUpdateEm: any,
    changeRouteToAttendanceMI: any,
    downloadSalDet: any,
    settingsArrayTotalProp: any,
}>
const firstRowStyle = {
    backgroundColor: 'rgb(228, 228, 228)',
    boxShadow: 'rgb(206, 206, 206) 3px 3px 5px',
    color: 'black',
    fontFamily: 'NexaLight',
    fontSize: '18px !important',

}

const RemainingRowStyle = {
    color: '#666666',
    fontFamily: 'NexaBold',
    fontSize: '10px !important'
}


interface ISTATE {
    value: any
}

class SettingsComponent extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props);
        this.state = { value: 1 };

    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public ChangeRouteToProfile = () => {
        //   ChangeRouteToProfile
    }
    public render() {
        const AccSett = <span>Edit</span>
        const LogoutIc = <span>Delete</span>
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <Sidebar RoleProp={this.props.RoleProp} active = 'settings'/>
                    <div className={`sideMainBody settingsideBody ${this.props.SettingEmptyTableProp ? ('EmptyTableSetDash') : null}`}>
                        <div className='sideMainBodyChild1'>
                            <h1 className='DashboardMainHeadND SettingsHeading'>Salary Sheet</h1>

                            <div className='DashDSwitcher'>
                                <button onClick={this.props.DecrementMonthPropFunc} className='NoPadddingND' disabled={this.props.PreviousDisable}><i className="material-icons" >keyboard_arrow_left</i></button>
                                <span className='salarygraphYear'>{`${this.props.MonthProp} ${this.props.YearProp}`}</span>
                                <button onClick={this.props.IncrementMonthPropFunc} className='NoPadddingND' disabled={this.props.NextDisable}><i className="material-icons" >keyboard_arrow_right</i></button>
                            </div>

                        </div>
                        <div className='hiddenDiv'>
                            <div>
                                .
                            </div>
                        </div>
                        {
                            !this.props.SettingLoaderProps ? (

                                <Table multiSelectable={false} selectable={false} className={`${this.props.settingsArrayTotalProp ? ('SettingTableMarginT') : ('NoRecordSetting')}`}>
                                    <TableBody displayRowCheckbox={false}>
                                        <TableRow style={firstRowStyle} >
                                            <TableRowColumn className="SNoClass">S.NO</TableRowColumn>
                                            <TableRowColumn>NAME</TableRowColumn>
                                            <TableRowColumn>CURRENT SALARY</TableRowColumn>

                                            <TableRowColumn>BONUS</TableRowColumn>
                                            <TableRowColumn>TAX</TableRowColumn>
                                            <TableRowColumn>DEDUCTION</TableRowColumn>

                                            <TableRowColumn>PAYABLE SALARY</TableRowColumn>
                                            <TableRowColumn className='DropDownTd'>
                                                <DropDownMenu
                                                    style={{ border: 'none', color: 'black' }}
                                                    value={this.state.value}
                                                    className='OptionDropDown'
                                                    menuItemStyle={{ fontSize: '12px', fontFamily: 'NexaBold' }}
                                                    labelStyle={{ color: 'black', fontFamily: 'NexaLight', fontSize: '13px' }}
                                                    selectedMenuItemStyle={{ color: '#008141' }}
                                                >
                                                    <MenuItem value={1} primaryText="PKR" />
                                                    <MenuItem value={2} primaryText="USD" />
                                                    {
                                                        this.props.SettingsArrayProp ? (
                                                            <MenuItem value={3} primaryText="Download" onClick={this.props.downloadSalDet} />
                                                        ) : null
                                                    }


                                                </DropDownMenu>
                                            </TableRowColumn>
                                        </TableRow>

                                        {
                                            this.props.SettingsArrayProp ?
                                                this.props.SettingsArrayProp.map((value: any, index: any) => {
                                                    return (

                                                        <TableRow style={RemainingRowStyle} key={index} className={index % 2 !== 0 ? 'grayRowSett' : 'whiteRowSett'}>
                                                            <TableRowColumn className='SNoClass SNoClassWithData'>{index + 1}</TableRowColumn>
                                                            <TableRowColumn className='NameClassSetting'>
                                                                {value.name}
                                                            </TableRowColumn>
                                                            <TableRowColumn className='CurrSalClassSetting'> {value.currentSalary}
                                                            </TableRowColumn>
                                                            <TableRowColumn className='TotalBonClassSetting'>{value.totalBonus.toLocaleString()}</TableRowColumn>
                                                            <TableRowColumn className='TotalTaxClassSetting'>{value.totalTax.toLocaleString()}</TableRowColumn>
                                                            <TableRowColumn className='TotalDedClassSetting'>{value.totalDeduction.toLocaleString()}</TableRowColumn>
                                                            <TableRowColumn className='TotalPSalClassSetting'>{value.payableSalary.toLocaleString()}</TableRowColumn>
                                                            <TableRowColumn className='SettTableLastTd'>
                                                                <IconMenu
                                                                    iconButtonElement={
                                                                        <i className="material-icons SettingToggler">more_horiz</i>}
                                                                    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                                                                    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                                                                    style={{ boxShadow: '0px !important' }} className='settingIconMenu'
                                                                    useLayerForClickAway={true}
                                                                >


                                                                    <MenuItem className='SettingTogItm' primaryText={AccSett} onClick={this.props.changeRouteToUpdateEm.bind(this, value)} />
                                                                    <MenuItem className='SettingTogItm' primaryText={LogoutIc} />





                                                                </IconMenu>
                                                            </TableRowColumn>


                                                        </TableRow>
                                                    )
                                                }) : null
                                        }

                                        {
                                            this.props.settingsArrayTotalProp ? (
                                                <TableRow className='SettTabLastRow'>
                                                    <TableRowColumn>.</TableRowColumn>
                                                    <TableRowColumn>TOTALS</TableRowColumn>
                                                    <TableRowColumn>{`${this.props.settingsArrayTotalProp[0].grandSalary.toLocaleString()}`}</TableRowColumn>
                                                    <TableRowColumn>{`${this.props.settingsArrayTotalProp[0].grandBonus.toLocaleString()}`}</TableRowColumn>
                                                    <TableRowColumn>{`${this.props.settingsArrayTotalProp[0].grandTax.toLocaleString()}`}</TableRowColumn>
                                                    <TableRowColumn>{`${this.props.settingsArrayTotalProp[0].grandTotalDeduction.toLocaleString()}`}</TableRowColumn>
                                                    <TableRowColumn>{`${this.props.settingsArrayTotalProp[0].grandPayableSalary.toLocaleString()}`}</TableRowColumn>
                                                    <TableRowColumn>.</TableRowColumn>


                                                </TableRow>
                                            ) : (
                                                    <TableRow className='SettTabLastRowEmpty'>
                                                        <TableRowColumn style={{ textAlign: 'center' }}
                                                            colSpan={8}
                                                        >No Record Found</TableRowColumn>


                                                    </TableRow>
                                                )
                                        }



                                    </TableBody>
                                </Table>
                            ) : (

                                    <SettingLoader SettingLoaderProp={this.props.SettingLoaderProps} />
                                )
                        }


                    </div>

                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>
        );
    }
}

export default SettingsComponent;