import * as React from 'react';
import './footer.css';
type Props = Partial<{
    FooterClass:any
}>
class Footer extends React.Component<Props>{
    public constructor(props: any) {
        super(props);

    }



    public render() {
        return (
            <footer className={this.props.FooterClass}>
                <p>A Product of AppBakerz. All Rights Reserved</p>
            </footer>
        );
    }
}

export default Footer;
