import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import * as moment from 'moment'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import Tab from '../tab/tab'

import * as React from 'react';
import { browserHistory } from 'react-router'// import './ApiKeyScreen.css';
import './employeePayslip.css';


type Props = Partial<{
    AnnualObjectProp: any,
    CurrMonthObjectProp: any,
    ChRouteToEmpD: any,
    EmailBtnProp: any,
    EmailFuncProp: any,
    EmailSuccessText: any,
    EmailSuccess: any,
    EmployeeObjectProp: any,
    PayslipLoaderProp: any,
    PayslipMonthChangeInC: any,
    PayslipMonthChangeProp: any,
    RoleProp: any,
    TabChDash: any,
    ChangeRouteToAttendanceComponentFunc: any,
    employeeDataProps: any
    currentMonProp: any,
    changeRouteToEditP: any,
    IncDateFunc: any,
    DecDateFunc: any,
    Month: any,
    Year: any

}>

class EmployeePayslip extends React.Component<Props> {
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.employeeName

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            delete obj.CurrentMonthFormat


            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }



    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <Sidebar RoleProp={this.props.RoleProp} />
                    <div className='sideMainBody payslipSideBody'>

                        {
                            this.props.employeeDataProps ? (
                                <div className='sideMainBodyChild1'>
                                    <h1 className='DashboardMainHeadND'>
                                        {this.props.employeeDataProps[0].name}
                                    </h1>
                                    <div className='DashDSwitcher'>
                                        <button onClick={this.props.PayslipMonthChangeProp} disabled={this.props.currentMonProp === 0 ? true : false} className='NoPadddingND'><i className="material-icons">keyboard_arrow_left</i></button>
                                        <span className='salarygraphYear payslipyear'>
                                            {
                                                this.props.currentMonProp === 0 ? 'JANUARY' : this.props.currentMonProp === 1 ? 'FEBRUARY' :
                                                    this.props.currentMonProp === 2 ? 'MARCH' :
                                                        this.props.currentMonProp === 3 ? 'APRIL' :
                                                            this.props.currentMonProp === 4 ? 'MAY' : this.props.currentMonProp === 5 ? 'JUNE' :
                                                                this.props.currentMonProp === 6 ? 'JULY' : this.props.currentMonProp === 7 ? 'AUGUST' :
                                                                    this.props.currentMonProp === 8 ? 'SEPTEMBER' : this.props.currentMonProp === 9 ? 'OCTOBER' :
                                                                        this.props.currentMonProp === 10 ? 'NOVEMBER' : this.props.currentMonProp === 11 ? 'DECEMBER' : null

                                            }
                                        </span><button onClick={this.props.PayslipMonthChangeInC} disabled={this.props.currentMonProp === 11 ? true : false} className='NoPadddingND'><i className="material-icons" >keyboard_arrow_right</i></button>
                                    </div>
                                </div>
                            ) : (
                                    <div className="animated-background-toggleLine EmpDetailLoader">
                                        .
                                        </div>
                                )
                        }


                        <Tab activeProfile={false} activeDash={false} activeAtt={false} activePay={true}
                            AttChPay={this.props.ChangeRouteToAttendanceComponentFunc}
                            TabChProfile={this.props.ChRouteToEmpD}
                            TabChDash={this.props.TabChDash}
                            TabChPay={null}
                        />
                        {
                            this.props.RoleProp ?
                                this.props.RoleProp.role === 'company' ? (
                                    <RaisedButton label="EMAIL PAYSLIP" backgroundColor='#008141' className='btnFontColor registerbuttonstyle ViewSalaryButton email-button' onClick={this.props.EmailFuncProp} disabled={this.props.EmailBtnProp} />
                                ) : null
                                : null
                        }


                        {
                            !this.props.employeeDataProps && !this.props.AnnualObjectProp && !this.props.CurrMonthObjectProp ? (
                                <div className={`DivNextToTab ${this.props.RoleProp ? this.props.RoleProp.role === 'company' ? 'dynamic-margin' : '' : ''}`}>
                                    <div className='DivNextToTabCh1 DivNextToTabCh1NoPadding'>
                                        <div className="animated-background PaySLoader">
                                            .
                                        </div>
                                    </div>
                                    <div className='DivNextToTabCh2 DivNextToTabCh1NoPadding'>
                                        <div className="animated-background PaySLoader">
                                            .
                                        </div>
                                    </div>

                                </div>
                            ) : (

                                    <div className={`DivNextToTab ${this.props.RoleProp ? this.props.RoleProp.role === 'company' ? 'dynamic-margin' : null : null}`}>
                                        <div className='DivNextToTabCh1'>{`${this.props.RoleProp ? this.props.RoleProp.role === 'employee' ? 'You have' : 'He has' : null} availed `}<span className='FMofAvailLeave'>{`${this.props.AnnualObjectProp ? this.props.AnnualObjectProp.paidLeavesAvailedInYear : 0}`}</span> of <span className='FMofAvailLeave'>{`${this.props.AnnualObjectProp ? this.props.AnnualObjectProp.noOfPaidLeavesInYear : 0}`}</span> annual paid leaves</div>
                                        <div className='DivNextToTabCh2'>{`${this.props.RoleProp ? this.props.RoleProp.role === 'employee' ? 'Your' : 'His' : null} total number of absents are `}<span className='FMofAvailLeave'>{`${this.props.AnnualObjectProp ? this.props.AnnualObjectProp.totalAbsentsInYear : 0}`}</span></div>

                                    </div>
                                )
                        }






















                        <div className='payslipchild2Parent lastParentMarginBottom'>
                            <div className='payslipchild2ParentChild1'>

                                {
                                    this.props.PayslipLoaderProp ? (
                                        <Table className='tableBoxShadow' multiSelectable={false} selectable={false}>

                                            <TableBody displayRowCheckbox={false}>
                                                <TableRow className='TableheaderBColor2'>
                                                    <TableRowColumn colSpan={2} className='tableheader'>DEDUCTIONS DETAILS</TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>

                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>

                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    ) : (

                                            <Table className='tableBoxShadow' multiSelectable={false} selectable={false}>

                                                <TableBody displayRowCheckbox={false}>
                                                    <TableRow className='TableheaderBColor2'>
                                                        <TableRowColumn colSpan={2} className='tableheader'>DEDUCTIONS DETAILS</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Advance Salary</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>-</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Unpaid Leaves</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.unPaidLeavesDeductions ? `PKR ${this.props.CurrMonthObjectProp.unPaidLeavesDeductions.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Absents</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.absentsDeductions ? `PKR ${this.props.CurrMonthObjectProp.absentsDeductions.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Late</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.lateDeductions ? `PKR ${this.props.CurrMonthObjectProp.lateDeductions.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>

                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Half Days</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.halfdayDeductions ? `PKR ${this.props.CurrMonthObjectProp.halfdayDeductions.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>

                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Tax Deduction</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.taxDeduction ? `PKR ${this.props.CurrMonthObjectProp.taxDeduction.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>

                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Total Deduction</TableRowColumn>
                                                        <TableRowColumn className='secondTD totalDedBaC'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.totalDeduction ? `PKR ${this.props.CurrMonthObjectProp.totalDeduction.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                </TableBody>
                                            </Table>
                                        )
                                }
                            </div>
                            <div className='payslipchild2ParentChild3'>
                                {
                                    this.props.PayslipLoaderProp ? (
                                        <Table className='tableBoxShadow' multiSelectable={false} selectable={false}>

                                            <TableBody displayRowCheckbox={false}>
                                                <TableRow className='TableheaderBColor2'>
                                                    <TableRowColumn colSpan={2} className='tableheader'>SALARY DETAILS</TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>

                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>

                                                <TableRow selectable={false}>
                                                    <TableRowColumn className='firstTD LoaderTd' colSpan={2} >
                                                        <div className="animated-background-ParaNextToImage Paysliploader1">
                                                            .
                                                            </div>
                                                    </TableRowColumn>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    ) : (

                                            <Table className='tableBoxShadow' multiSelectable={false} selectable={false}>

                                                <TableBody displayRowCheckbox={false}>
                                                    <TableRow className='TableheaderBColor2'>
                                                        <TableRowColumn colSpan={2} className='tableheader'>SALARY DETAILS</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Salary</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.currentSalary ? `PKR ${this.props.CurrMonthObjectProp.currentSalary.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Basic Salary</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>-</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Other Allowance</TableRowColumn>
                                                        <TableRowColumn className='secondTD'>-</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD paddingBottomOfLastTable'>Bonus/Incentive</TableRowColumn>
                                                        <TableRowColumn className='secondTD paddingBottomOfLastTable'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.totalBonus ? `PKR ${this.props.CurrMonthObjectProp.totalBonus.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                    <TableRow selectable={false}>
                                                        <TableRowColumn className='firstTD'>Payable Salary</TableRowColumn>
                                                        <TableRowColumn className='secondTD totalPAYBaC'>{this.props.CurrMonthObjectProp ? this.props.CurrMonthObjectProp.totalPaid ? `PKR ${this.props.CurrMonthObjectProp.totalPaid.toFixed(2)}` : '-' : '-'}</TableRowColumn>
                                                    </TableRow>
                                                </TableBody>
                                            </Table>
                                        )
                                }



                            </div>
                        </div>


                    </div >

                </div >
                <Snackbar
                    open={this.props.EmailSuccess}
                    message={this.props.EmailSuccessText}
                    contentStyle={{ textAlign: 'center' }}
                    autoHideDuration={2000}

                />
                <Footer FooterClass='ResgisterFooter' />
            </div >
        );
    }
}

export default EmployeePayslip;
