import { Card } from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';
import RaisedButton from 'material-ui/RaisedButton';
import * as React from 'react';
import { Link } from 'react-router'
import Footer from '../footer/footer'
import './LoginForm.css';

type Props = Partial<{
    errorVisibilityProp: any,
    errordisplayProp: any,
    EmailValue: any,
    PasswordValue: any,
    LoginFuncProps: any,
    LoaderDisplayProp: any,
    RegisterDisable: any,
    changeRouteToRegister: any
}>
class Login extends React.Component<Props>{
    public render() {
        return (
            <div className='RegisterMainDiv backdiv'>
                <div className='RegisterMainDivChild1'>
                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }
                    <Card className='CardMainDiv'>
                    <form>
                        <div className='PayRollLogoDiv'>
                            <img src={require('../../images/payrolllogoLogin.png')} />
                        </div>
                        <div className='TextFieldDivs'>
                            <i className="material-icons TextFieldIcon" >person_outline</i>
                            <input type='text' placeholder='Enter Your Email Address' className='ResgisterTextFieldsStyle' onChange={this.props.EmailValue} />
                        </div>


                        <div className='TextFieldDivs'>
                            <i className="material-icons TextFieldIcon">lock_outline</i>
                            <input type='password' placeholder='Enter Your Password' className='forgetpasswordinput' onChange={this.props.PasswordValue} />
                            <Link to='/resetpassword' className='forgetpLink'>Forget password ?</Link>

                        </div>
                        <div className='smalldiv smallcolor loginErrorDiv' style={{ visibility: this.props.errorVisibilityProp }}>
                            <small>{this.props.errordisplayProp}</small>
                        </div>
                        <div className='TextFieldDivsRaisedButton'>
                            <RaisedButton label="LOGIN" fullWidth={true} backgroundColor='#008141' className='ResgisterTextFieldsStyleRaisedButton registerbuttonstyle btnFontColor registerbtnmargin' style={{ color: 'white' }} onClick={this.props.LoginFuncProps} disabled={this.props.RegisterDisable} type="submit"/>
                        </div>

                        <div className='TextFieldDivsPara'>
                            <p>Not a registered user yet?</p>
                        </div>

                        <div className='TextFieldDivsRaisedButton'>
                            <RaisedButton label="REGISTER" fullWidth={true} backgroundColor='#008141' className='ResgisterTextFieldsStyleLastBtnRaisedButton registerbuttonstyle btnFontColor' onClick={this.props.changeRouteToRegister} disabled={(this.props.LoaderDisplayProp === 'hidden') ? false : true} />
                        </div>
                        </form>
                    </Card>

                </div>

                <Footer FooterClass='LoginRegiterFooter' />

            </div>
        );
    }
}

export default Login;
