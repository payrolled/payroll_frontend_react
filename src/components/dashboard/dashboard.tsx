import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import { Bar, BarChart, Cell, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import Tab from '../tab/tab'

import './dashboard.css';






type Props = Partial<{
    chngRtToPayEmp: any,
    chngRtToProfileEmp: any,
    DecDateFunc: any,
    IncDateFunc: any,
    Month: string,
    Year: number,
    MonthDetailObject: any,
    Loader: any,
    Loader2: any,
    Loader3: any,
    RoleProp: any,
    Salariesdata: any,
    SalaryDivDisplay: any,
    SalariesDecDateFunc: any,
    SalariesGraphComparisonYear: any,
    SalariesGraphIncDateFunc: any,
    SalariesGraphDecDateFunc: any,
    SalariesIncDateFunc: any,
    SalariesComparisonMonth: any,
    SalariesComparisonYear: any,
    LinerProps: any,
    chngRtToAttEmp: any,
}>

interface ISTATE {
    roll: any
}

class DashboardComponent extends React.Component<Props, ISTATE> {
    constructor(props: any) {
        super(props)
        this.state = {
            roll: undefined
        }
    }



    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'home') {
            browserHistory.push('/home')
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public componentDidMount() {
        let roll: any = localStorage.getItem('data')
        roll = JSON.parse(roll)
        if (roll) {
            this.setState({
                roll: roll.role
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }
    public ChangeRouteToProfile = () => {
        // a
    }

    public render() {
        return (
            <div>

                <AppBarComponent RoleProp={this.props.RoleProp} HideNavigationItemDashboard='toggler' />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <Sidebar RoleProp = {this.props.RoleProp} active = 'dashboard'/>
                    <div className={`sideMainBody  ${!this.props.RoleProp ? ('DashboardSideBodyheight') : 'DashboardHeight'}`}>
                        <div className='sideMainBodyChild1'>
                            <h1 className='DashboardMainHeadND'>
                                {
                                    this.props.RoleProp ? (this.props.RoleProp.name) : null
                                }
                            </h1>

                            <div className='DashDSwitcher'>
                                <button onClick={this.props.DecDateFunc} className='NoPadddingND'><i className="material-icons">keyboard_arrow_left</i></button>
                                <span className='salarygraphYear'>{`${this.props.Month} ${this.props.Year}`}</span>
                                <button onClick={this.props.IncDateFunc} className='NoPadddingND'><i className="material-icons" >keyboard_arrow_right</i></button>
                            </div>




                           
                        </div>

                        {
                            this.props.RoleProp ?
                                this.props.RoleProp.role === 'employee' ? (
                                    <Tab activeProfile={false} activeDash={true} activeAtt={false} activePay={false}
                                        AttChPay={this.props.chngRtToAttEmp}
                                        TabChProfile={this.props.chngRtToProfileEmp}
                                        TabChPay={this.props.chngRtToPayEmp}
                                        TabChDash={null}
                                    />
                                ) : null
                                : null
                        }
                        <div className='sideMainBodyChild2'>
                            {
                                this.props.RoleProp ?
                                    this.props.RoleProp.role === 'company' ? (
                                        <div className='sideMainBodyChild2OnlyChild'>
                                            <div className='sideMainBodyChild2Child1'>
                                                <div className='sideMainBodyChild2Child1Parent'>


                                                    <div className='sideMainBodyChild2Child1ParentChild1'>
                                                        <p>SALARIES</p>
                                                    </div>
                                                   

                                                    <div className='NewThemeParentChild3' >
                                                        <div className='NewThemeParentChild3Child1'>

                                                            <div className='NewThemeParentChild3Child111'>
                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                                <p>Basic</p>
                                                                                <h1 style={{ visibility: this.props.SalaryDivDisplay }} className='BasicColor'>{this.props.MonthDetailObject !== undefined ? (`PKR ${Number((this.props.MonthDetailObject.totalCurrentSalaries).toFixed(1)).toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1 salaryLoaderStyle NoMarginLeft'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                               
                                                                            </div>
                                                                        ) : null
                                                                    }

                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                                
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                                <p>Bonus</p>
                                                                                <h1 className='Bonuscolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (`PKR ${Number(this.props.MonthDetailObject.totalBonus.toFixed(1)).toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                            </div>

                                                                        ) : null
                                                                    }
                                                                </div>

                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1 salaryLoaderStyle NoMarginLeft'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                               
                                                                            </div>
                                                                        ) : null
                                                                    }

                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                                <p>Deductions</p>
                                                                                <h1 className='Deductioncolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number(this.props.MonthDetailObject.totalDeductions.toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }



                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                               
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (
                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                                <p>Tax</p>
                                                                                <h1 className='Taxcolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number((this.props.MonthDetailObject.totalTax).toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className='NewThemeParentChild3Child112'>

                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent'>
                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth      totalPaidWidth'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDTPHeight">.</div>
                                                                               
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (


                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth'>
                                                                                <p>Total Payable</p>

                                                                                <h1 style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number(((this.props.MonthDetailObject.totalBonus + this.props.MonthDetailObject.totalSalaries) - this.props.MonthDetailObject.totalTax).toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }
                                                                </div>
                                                            </div>

                                                        </div>




                                                    </div>
                                                </div>

                                            </div>

                                            <div className='sideMainBodyChild3'>

                                                <div className='sideMainBodyChild2Child1Parent'>

                                                    <div className='sideMainBodyChild2Child1ParentChild2'>
                                                        <p className='salariesGraph'>SALARIES GRAPH</p>
                                                        <div className='DateSwitcher'>
                                                            <button onClick={this.props.SalariesGraphDecDateFunc} className='NoPaddding'><i className="material-icons">keyboard_arrow_left</i></button><span className='salarygraphYearND'>{`${this.props.SalariesGraphComparisonYear}`}</span><button onClick={this.props.SalariesGraphIncDateFunc} className='NoPaddding'><i className="material-icons" >keyboard_arrow_right</i></button>
                                                        </div>
                                                    </div>


                                                    <div className='sideMainBodyChild2Child1ParentChild3 secondGraTTip' >
                                                       
                                                        {
                                                            this.props.Salariesdata ? (
                                                                <ResponsiveContainer width="98%" height={300}>

                                                                    <BarChart data={this.props.Salariesdata}
                                                                    >
                                                                        <XAxis dataKey="name" />
                                                                        <YAxis />

                                                                        <Tooltip itemStyle={{ padding: 0, margin: 0, fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                                            wrapperStyle={{ margin: 0, padding: 0, borderRadius: '6px' }}
                                                                            separator={' '}
                                                                            labelStyle={{ fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                                            cursor={{ fill: '#ebebeb' }}


                                                                        />
                                                                        <Bar dataKey="PKR" fill="black" cursor={'pointer'} >
                                                                            {
                                                                                this.props.Salariesdata.map((value: any, index: any) => {
                                                                                    return (

                                                                                        <Cell key={index} fill={index % 2 === 0 ? ("#e4b835") : ("#008141")} />
                                                                                    )
                                                                                })
                                                                            }
                                                                        </Bar>

                                                                    </BarChart>

                                                                </ResponsiveContainer>
                                                            ) : (
                                                                    <div className='GraphLoaderMainDiv'>


                                                                        <svg className="svg__loading" viewBox="0 0 311 120" version="1.1">
                                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                <path d="M0,0 L0,119 L74.6161228,119 L74.6161228,0 L0,0 Z M78.7946257,0 L78.7946257,119 L153.410749,119 L153.410749,0 L78.7946257,0 Z M157.589251,0 L157.589251,119 L232.205374,119 L232.205374,0 L157.589251,0 Z M236.383877,0 L236.383877,119 L311,119 L311,0 L236.383877,0 Z" id="Rectangle-1-copy" fill="#FAFAFA" />
                                                                                <rect className="svg__loading-new" x="0" y="74.618705" width="74.6161228" height="100%">
                                                                                    <animate id="animate__new-bar"
                                                                                        attributeName="y"
                                                                                        from="74.618705"
                                                                                        to="74.618705"
                                                                                        begin="0s"
                                                                                        dur="1.5s"
                                                                                        values="74.618705; 0; 74.618705; 74.618705; 74.618705"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>


                                                                                <rect className="svg__loading-open" x="78.7946257" y="37" width="74.6161228" height="100%">
                                                                                    <animate id="animate__open-bar"
                                                                                        attributeName="y"
                                                                                        from="37"
                                                                                        to="37"
                                                                                        begin="0.1s"
                                                                                        dur="1.5s"
                                                                                        values="37; 0; 37; 37; 37"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                                <rect className="svg__loading-won" x="157.589251" y="57.3021583" width="74.6161228" height="100%">
                                                                                    <animate id="animate__won-bar"
                                                                                        attributeName="y"
                                                                                        from="57.3021583"
                                                                                        to="57.3021583"
                                                                                        begin="0.2s"
                                                                                        dur="1.5s"
                                                                                        values="57.3021583; 0; 57.3021583; 57.3021583; 57.3021583"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                                <rect className="svg__loading-lost" x="236.383877" y="97.3093525" width="74.6161228" height="100%">
                                                                                    <animate id="animate__lost-bar"
                                                                                        attributeName="y"
                                                                                        from="97.3093525"
                                                                                        to="97.3093525"
                                                                                        begin="0.3s"
                                                                                        dur="1.5s"
                                                                                        values="97.3093525; 0; 97.3093525; 97.3093525; 97.3093525"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                            </g>
                                                                        </svg>
                                                                    </div>

                                                                )
                                                        }
                                                    </div>

                                                </div>






                                            </div>





                                        </div>
                                    ) : null : null
                            }



                            {
                                this.props.RoleProp ?
                                    this.props.RoleProp.role === 'employee' ? (
                                        <div className='sideMainBodyChild2OnlyChild'>


                                            <div className='sideMainBodyChild2Child1 employeesideMainBodyChild2Child1'>
                                                <div className='sideMainBodyChild2Child1Parent'>


                                                    <div className='sideMainBodyChild2Child1ParentChild1'>
                                                        <p>SALARIES</p>
                                                    </div>
                                                   

                                                    <div className='NewThemeParentChild3' >
                                                        <div className='NewThemeParentChild3Child1'>

                                                            <div className='NewThemeParentChild3Child111'>
                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                                <p>Basic</p>
                                                                                <h1 style={{ visibility: this.props.SalaryDivDisplay }} className='BasicColor'>{this.props.MonthDetailObject !== undefined ? (`PKR ${Number(this.props.MonthDetailObject.currentSalary.toFixed(1)).toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                                
                                                                            </div>
                                                                        ) : null
                                                                    }

                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                                <p>Bonus</p>
                                                                                <h1 className='Bonuscolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? (`PKR ${Number(this.props.MonthDetailObject.totalBonus.toFixed(1)).toLocaleString()}`) : ('PKR 0.00')}</h1>
                                                                            </div>

                                                                        ) : null
                                                                    }
                                                                </div>

                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent' >

                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                                
                                                                            </div>
                                                                        ) : null
                                                                    }

                                                                    {
                                                                        this.props.Loader === 'hidden' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild1'>
                                                                                <p>Deductions</p>
                                                                                <h1 className='Deductioncolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number(this.props.MonthDetailObject.totalDeductions.toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }



                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2 salaryLoaderStyle'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDLoaderHeigt">.</div>
                                                                                
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (
                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentChild2'>
                                                                                <p>Tax</p>
                                                                                <h1 className='Taxcolor' style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number((this.props.MonthDetailObject.totalTax).toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className='NewThemeParentChild3Child112'>

                                                                <div className='sideMainBodyChild2Child1ParentChild3Parent'>
                                                                    {
                                                                        this.props.Loader === 'unset' ? (

                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth      totalPaidWidth'>
                                                                                <div className="animated-backgroundDashboardTotalPaid NDTPHeight">.</div>
                                                                                
                                                                            </div>
                                                                        ) : null
                                                                    }


                                                                    {
                                                                        this.props.Loader === 'hidden' ? (


                                                                            <div className='sideMainBodyChild2Child1ParentChild3ParentFullWidth'>
                                                                                <p>Total Payable</p>

                                                                                <h1 style={{ visibility: this.props.SalaryDivDisplay }}>{this.props.MonthDetailObject !== undefined ? `PKR ${Number(((this.props.MonthDetailObject.totalBonus + this.props.MonthDetailObject.totalSalaries) - this.props.MonthDetailObject.totalTax).toFixed(1)).toLocaleString()}` : ('PKR 0.00')}</h1>
                                                                            </div>
                                                                        ) : null
                                                                    }
                                                                </div>
                                                            </div>

                                                        </div>




                                                    </div>

                                                </div>
                                            </div>
                                            <div className='sideMainBodyChild3'>

                                                <div className='sideMainBodyChild2Child1Parent'>

                                                    <div className='sideMainBodyChild2Child1ParentChild2'>
                                                        <p className='salariesGraph'>SALARIES GRAPH</p>
                                                        <div className='DateSwitcher'>
                                                            <button onClick={this.props.SalariesGraphDecDateFunc} className='NoPaddding'><i className="material-icons">keyboard_arrow_left</i></button><span className='salarygraphYearND'>{`${this.props.SalariesGraphComparisonYear}`}</span><button onClick={this.props.SalariesGraphIncDateFunc} className='NoPaddding'><i className="material-icons" >keyboard_arrow_right</i></button>
                                                        </div>
                                                    </div>


                                                    <div className='sideMainBodyChild2Child1ParentChild3 secondGraTTip' >
                                                       
                                                        {
                                                            this.props.Salariesdata ? (
                                                                <ResponsiveContainer width="98%" height={300}>

                                                                    <BarChart data={this.props.Salariesdata}
                                                                    >
                                                                        <XAxis dataKey="name" />
                                                                        <YAxis />

                                                                        <Tooltip itemStyle={{ padding: 0, margin: 0, fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                                            wrapperStyle={{ margin: 0, padding: 0, borderRadius: '6px' }}
                                                                            separator={' '}
                                                                            labelStyle={{ fontFamily: 'AcrobatBlack', color: '#ffffff' }}
                                                                            cursor={{ fill: '#ebebeb' }}


                                                                        />
                                                                        <Bar dataKey="PKR" fill="black" cursor={'pointer'} >
                                                                            {
                                                                                this.props.Salariesdata.map((value: any, index: any) => {
                                                                                    return (

                                                                                        <Cell key={index} fill={index % 2 === 0 ? ("#e4b835") : ("#008141")} />
                                                                                    )
                                                                                })
                                                                            }
                                                                        </Bar>

                                                                    </BarChart>

                                                                </ResponsiveContainer>
                                                            ) : (
                                                                    <div className='GraphLoaderMainDiv'>


                                                                        <svg className="svg__loading" viewBox="0 0 311 120" version="1.1">
                                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                <path d="M0,0 L0,119 L74.6161228,119 L74.6161228,0 L0,0 Z M78.7946257,0 L78.7946257,119 L153.410749,119 L153.410749,0 L78.7946257,0 Z M157.589251,0 L157.589251,119 L232.205374,119 L232.205374,0 L157.589251,0 Z M236.383877,0 L236.383877,119 L311,119 L311,0 L236.383877,0 Z" id="Rectangle-1-copy" fill="#FAFAFA" />
                                                                                <rect className="svg__loading-new" x="0" y="74.618705" width="74.6161228" height="100%">
                                                                                    <animate id="animate__new-bar"
                                                                                        attributeName="y"
                                                                                        from="74.618705"
                                                                                        to="74.618705"
                                                                                        begin="0s"
                                                                                        dur="1.5s"
                                                                                        values="74.618705; 0; 74.618705; 74.618705; 74.618705"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>


                                                                                <rect className="svg__loading-open" x="78.7946257" y="37" width="74.6161228" height="100%">
                                                                                    <animate id="animate__open-bar"
                                                                                        attributeName="y"
                                                                                        from="37"
                                                                                        to="37"
                                                                                        begin="0.1s"
                                                                                        dur="1.5s"
                                                                                        values="37; 0; 37; 37; 37"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                                <rect className="svg__loading-won" x="157.589251" y="57.3021583" width="74.6161228" height="100%">
                                                                                    <animate id="animate__won-bar"
                                                                                        attributeName="y"
                                                                                        from="57.3021583"
                                                                                        to="57.3021583"
                                                                                        begin="0.2s"
                                                                                        dur="1.5s"
                                                                                        values="57.3021583; 0; 57.3021583; 57.3021583; 57.3021583"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                                <rect className="svg__loading-lost" x="236.383877" y="97.3093525" width="74.6161228" height="100%">
                                                                                    <animate id="animate__lost-bar"
                                                                                        attributeName="y"
                                                                                        from="97.3093525"
                                                                                        to="97.3093525"
                                                                                        begin="0.3s"
                                                                                        dur="1.5s"
                                                                                        values="97.3093525; 0; 97.3093525; 97.3093525; 97.3093525"
                                                                                        keySplines=".75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99; .75 .06 .38 .99"
                                                                                        keyTimes="0; 0.25; 0.5; 0.75; 1"
                                                                                        calcMode="spline"
                                                                                        repeatCount="indefinite" />
                                                                                </rect>
                                                                            </g>
                                                                        </svg>
                                                                    </div>

                                                                )
                                                        }

                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    ) : null : null
                            }


                        </div>

                    </div>
                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>
        );
    }
}

export default DashboardComponent;
