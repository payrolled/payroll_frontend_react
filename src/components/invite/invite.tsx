import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'
import AppBarComponent from '../AppBar/AppBar';
import Sidebar from '../common/sidebar'
import Footer from '../footer/footer'
import './invite.css';

type Props = Partial<{
    InviteBtnDisProp: any,
    syncBtnDis: any,
    InviteEmpFun: any,
    InviteEmpProp: any,
    InviteSnackBar: any,
    InviteSnackBarText: any,
    RoleProp: any,
    SyncFunc: any,
    archiveNdUser: any
}>





const firstRowStyle = {
    backgroundColor: '#e4e4e4',
    boxShadow: '3px 3px 5px #cecece',
    color: 'black',
    fontFamily: 'NexaLight',
    fontSize: '18px !important',

}

const RemainingRowStyle = {
    color: 'black',
    fontFamily: 'NexaLight',
}

class Invite extends React.Component<Props> {
    constructor(props: any) {
        super(props);
    }
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'dashboard') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }


    public render() {
        return (
            <div>
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null} ${this.props.InviteBtnDisProp ? ('DisableInviteDiv') : null}`}>
                    <Sidebar RoleProp={this.props.RoleProp} />
                    <div className={`sideMainBody settingsideBody`}>

                        <div className='sideMainBodyChild1 employeesviewMobileView'>
                            <h1 className='ViewEmployeesHeading ViewEmpHMob'>Invite</h1>
                            <RaisedButton label="SYNC" backgroundColor='#008141' className='btnFontColor registerbuttonstyle ViewSalaryButton' disabled={this.props.syncBtnDis} onClick={this.props.SyncFunc} />
                        </div>

                        <div className='InviteTableWidth'>

                            <Table multiSelectable={false} selectable={false} className='SettingTableMarginT InviteTableMarginTop'>
                                <TableBody displayRowCheckbox={false}>
                                    <TableRow style={firstRowStyle} >
                                        <TableRowColumn >ID</TableRowColumn>
                                        <TableRowColumn>NAME</TableRowColumn>
                                        <TableRowColumn >STATUS</TableRowColumn>


                                    </TableRow>
                                    {
                                        this.props.InviteEmpProp ?
                                            this.props.InviteEmpProp.data.map((value: any, index: any) => {

                                                return (

                                                    <TableRow style={RemainingRowStyle} key={index} className={index % 2 !== 0 ? 'grayRowSett' : 'whiteRowSett'}>
                                                        <TableRowColumn className='idclass'>{index + 1}</TableRowColumn>
                                                        <TableRowColumn className='nameClassinv'>
                                                            {value.name}
                                                            {
                                                                value.status === 'nonDesktimeUser' ? (
                                                                    <RaisedButton label="Archive" backgroundColor='#008141' className='btnFontColor registerbuttonstyle ViewSalaryButton archiveBtn' onClick={this.props.archiveNdUser.bind(this, value._id, index)} disabled={value.archived} />
                                                                ) : null
                                                            }
                                                        </TableRowColumn>
                                                        <TableRowColumn className='statusClassinv'>

                                                            {
                                                                value.status === 'failed' ? (
                                                                    <div>
                                                                        <span>Failed
                                                                        </span>
                                                                        <span>

                                                                            <i className={`material-icons spinnerIcon ${value.loading ? 'spinnerIconTrue' : null}`} onClick={this.props.InviteEmpFun.bind(this, value._id, index, true)}>
                                                                                refresh
                                                                            </i>
                                                                        </span>
                                                                    </div>
                                                                ) : value.status === 'invited' ? (
                                                                    <div>
                                                                        <span>Waiting for Approval
                                                                        </span>
                                                                    </div>
                                                                ) : value.status === 'joined' ? (
                                                                    <div>
                                                                        <span className='acceptedInv'>Accepted Invitation</span>

                                                                    </div>
                                                                ) : value.status === 'expired' ? (
                                                                    <div>
                                                                        <span>Expired
                                                                        <i className={`material-icons spinnerIcon ${value.loading ? 'spinnerIconTrue' : null}`} onClick={this.props.InviteEmpFun.bind(this, value._id, index, true)}>
                                                                                refresh
                                                                            </i>
                                                                        </span>

                                                                    </div>
                                                                ) : value.status === 'nonDesktimeUser' ? (
                                                                    <div>
                                                                        <span className='acceptedInv'>Non Desktime User</span>

                                                                    </div>) : (
                                                                                        <div>
                                                                                            <span>Pending
                                                                                        <i className={`material-icons spinnerIcon ${value.loading ? 'spinnerIconTrue' : null}`} onClick={this.props.InviteEmpFun.bind(this, value._id, index, true)}>
                                                                                                    refresh
                                                                            </i>
                                                                                            </span>

                                                                                        </div>
                                                                                    )
                                                            }

                                                        </TableRowColumn>

                                                    </TableRow>
                                                )
                                            }) :

                                            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((value: any, index: any) => {

                                                return (
                                                    <TableRow style={RemainingRowStyle} className='InviteLoaderRow' key={index}>
                                                        <TableRowColumn className='idclass'>
                                                            <div className="animated-background-ParaNextToImage InviteLoader">
                                                                .
                                                                        </div></TableRowColumn>
                                                        <TableRowColumn className='nameClassinv'>
                                                            <div className="animated-background-ParaNextToImage InviteLoader">
                                                                .
                                                                        </div></TableRowColumn>
                                                        <TableRowColumn className='statusClassinv'>
                                                            <div className="animated-background-ParaNextToImage InviteLoader">
                                                                .
                                                                        </div>
                                                        </TableRowColumn>

                                                    </TableRow>
                                                )
                                            })

                                    }








                                    }
                                </TableBody>
                            </Table>
                            <Snackbar
                                open={this.props.InviteSnackBar}
                                message={this.props.InviteSnackBarText}
                                contentStyle={{ textAlign: 'center' }}
                                autoHideDuration={2000}


                            />
                        </div>

                    </div>

                </div>
                <Footer FooterClass='ResgisterFooter' />
            </div>
        );
    }
}

export default Invite;
