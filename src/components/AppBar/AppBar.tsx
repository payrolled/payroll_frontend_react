import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'

import './AppBar.css';



type Props = Partial<{
    HideNavigationItemDashboard: any,
    HideNavigationItemEmployees: any,
    RoleProp: any
}>

interface ISTATE {
    mySideNav: any
}
class AppBarComponent extends React.Component<Props, ISTATE>{
    public constructor(props: any) {
        super(props);
        this.state = {
            mySideNav: null
        }

    }


    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== '/employees') {
            browserHistory.push({
                pathname: '/employees',
                state: this.props.RoleProp
            })
        }
    }

    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'home') {
            const obj = this.props.RoleProp
            delete obj.employeeName
            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public SignOut = () => {
        localStorage.removeItem('data')
        localStorage.removeItem('user')
        sessionStorage.clear()
        browserHistory.replace('/')
    }

    public DrawerFunctionOpen = () => {
        this.setState({
            mySideNav: 'mysidenav'
        })
    }
    public DrawerFunctionClose = () => {
        this.setState({
            mySideNav: null
        })
    }

    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public ChangeRouteToAccSetting = () => {
        browserHistory.push({
            pathname: '/accountsettings',
            state: this.props.RoleProp
        })
    }
    public chRToHome=()=>{
        browserHistory.push({
            pathname: '/home',
            state: this.props.RoleProp
        })
    }
    public render() {
        const AccSett = <span><i className="material-icons MenuItm">account_box</i>Account Settings</span>
        const LogoutIc = <span><i className="material-icons MenuItm">exit_to_app</i>Log Out</span>
        return (
            <AppBar
                iconElementLeft={
                    <div>
                        {
                            this.props.RoleProp ?
                                this.props.RoleProp.role === 'company' ? (
                                    <div className={`sidenav ${this.state.mySideNav}`}>
                                        <a className="closebtn" onClick={this.DrawerFunctionClose}>&times;</a>
                                        <div className="DrawerProfilePicDiv">
                                            <img src={require('../../images/avatar.jpg')} className='drawerProfilePic' />
                                        </div>
                                        <a onClick={this.ChangeRouteToDashboard} className={`drawerItems ${window.location.pathname === '/home' ? ("activeclass") : null}`}><i className="material-icons">dashboard</i>Home</a>
                                        <a onClick={this.ChangeRouteToEmployees} className={`drawerItems ${window.location.pathname === '/employees' ? ("activeclass") : null}`}><i className="material-icons">people</i>Employees</a>
                                        <a onClick={this.ChangeRouteToAttendance} className={`drawerItems ${window.location.pathname === '/attendance' ? ("activeclass") : null}`}><i className="material-icons">schedule</i>Attendance</a>
                                        <a onClick={this.ChangeRouteToSettings} className={`drawerItems ${window.location.pathname.slice(0, 9) === "/salarysheet" ? ("activeclass") : null}`}><i className="material-icons">pie_chart</i>Salary Sheet</a>
                                        <a onClick={this.ChangeRouteToAccSetting} className={`drawerItems ${window.location.pathname === '/accountsettings' ? ("activeclass") : null}`}><i className="material-icons">account_circle</i>Account</a>
                                        <a className='drawerItems' onClick={this.SignOut}><i className="material-icons">input</i>Signout</a>

                                    </div>
                                ) :
                                    this.props.RoleProp.role === 'employee' ? (
                                        <div className={`sidenav ${this.state.mySideNav}`}>
                                            <a href="javascript:void(0)" className="closebtn" onClick={this.DrawerFunctionClose}>&times;</a>
                                            <div className="DrawerProfilePicDiv">
                                                <img src={this.props.RoleProp && this.props.RoleProp.imageUrl ? this.props.RoleProp.imageUrl : require('../../images/avatar.jpg')} className='drawerProfilePic' />
                                            </div>
                                            <a onClick={this.ChangeRouteToDashboard} className={`drawerItems ${window.location.pathname === '/home' ? ("activeclass") : null}`}><i className="material-icons">dashboard</i>Home</a>
                                            <a onClick={this.ChangeRouteToAccSetting} className={`drawerItems ${window.location.pathname === '/accountsettings' ? ("activeclass") : null}`}><i className="material-icons">account_circle</i>Account</a>
                                            <a className='drawerItems' onClick={this.SignOut}><i className="material-icons">input</i>Signout</a>

                                        </div>
                                    ) : null : null
                        }
                        <i className="material-icons drawerIcon" onClick={this.DrawerFunctionOpen}>
                            menu
                        </i>

                        <img src={require('../../images/payrolldashboardlogo.png'
                        )} className='payrolldashboardlogoMargin' onClick={this.chRToHome}/>
                    </div>
                }
                iconElementRight={<ul className='rightItemInNavBar'>

                    <li className='imageLi'> {
                        this.props.RoleProp ? "Welcome, " : null}
                        <span>
                            {
                                this.props.RoleProp ? (this.props.RoleProp.name) : null}
                        </span></li>
                    <li className='greetingLi'>
                        <IconMenu
                            iconButtonElement={
                                <img src={this.props.RoleProp && this.props.RoleProp.imageUrl ? this.props.RoleProp.imageUrl : require('../../images/avatar.jpg')} className='profileLogo' />}
                            targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                            style={{ boxShadow: '0px !important' }} className='IconMenuCustom'

                        >

                            <MenuItem primaryText={AccSett} className='signoutDropDown' onClick={this.ChangeRouteToAccSetting} />
                            <MenuItem primaryText={LogoutIc} className='signoutDropDown' onClick={this.SignOut} />

                        </IconMenu>

                    </li>
                </ul>}

                className='DashboardAppBar' />
        );
    }
}

export default AppBarComponent;
