import { Card } from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';

import RaisedButton from 'material-ui/RaisedButton';
import * as React from 'react';
import Footer from '../footer/footer'
import './registerForm.css';
interface ISTATE {
    type: any
}
type Props = Partial<{
    EmailValue: any,
    CarMainDivOpacityProp: any,
    LoaderDisplayProp: any,
    PasswordValue: any,
    AgainPasswordValue: any,
    RegisterFuncProps: any,
    RegisterDisable: any,
    changeRouteToLogin: any,
    errordisplayProp: any,
    errorVisibilityProp: any
}>
class RegisterForm extends React.Component<Props, ISTATE>{
    public render() {
        return (

            <div className='RegisterMainDiv backdiv'>

                <div className='RegisterMainDivChild1'>
                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }
                    <Card className='CardMainDiv'>
                        <form>
                            <div className='PayRollLogoDiv'>
                                <img src={require('../../images/payrolllogoLogin.png')} />
                            </div>
                            <div className='TextFieldDivs'>
                                <i className="material-icons TextFieldIcon" >person_outline</i>
                                <input type='email' placeholder='Enter Your Email Address' className='ResgisterTextFieldsStyle' onChange={this.props.EmailValue} />
                            </div>


                            <div className='TextFieldDivs'>
                                <i className="material-icons TextFieldIcon">lock_outline</i>
                                <input type='password' placeholder='Enter Your Password' className='ResgisterTextFieldsStyle' onChange={this.props.PasswordValue} />
                            </div>
                            <div className='TextFieldDivs'>
                                <i className="material-icons TextFieldIcon">lock_outline</i>
                                <input type='password' placeholder='Re-enter Your Password' className='ResgisterTextFieldsStyle' onChange={this.props.AgainPasswordValue} />
                            </div>

                            <div className='smalldiv smallcolor' style={{ visibility: this.props.errorVisibilityProp }}>
                                <small>{this.props.errordisplayProp}</small>
                            </div>
                            <div className='TextFieldDivsRaisedButton'>
                                <RaisedButton label="REGISTER" disabled={this.props.RegisterDisable} fullWidth={true} backgroundColor='#008141' className='ResgisterTextFieldsStyleRaisedButton registerbtnmargin registerbuttonstyle btnFontColor' style={{ color: 'white' }} onClick={this.props.RegisterFuncProps} type="submit" />
                            </div>

                            <div className='TextFieldDivsPara'>
                                <p className='AlreadyARegister'>Already a registered user?</p>
                            </div>

                            <div className='TextFieldDivsRaisedButton'>
                                <RaisedButton label="LOGIN" fullWidth={true} disabled={(this.props.LoaderDisplayProp === 'hidden') ? false : true} backgroundColor='#008141' className='ResgisterTextFieldsStyleLastBtnRaisedButton registerbuttonstyle btnFontColor' onClick={this.props.changeRouteToLogin} />
                            </div>
                        </form>
                    </Card>
                </div>

                <Footer FooterClass='LoginRegiterFooter' />
            </div>
        );
    }
}

export default RegisterForm;
