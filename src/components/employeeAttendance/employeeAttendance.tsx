
import { Card } from 'material-ui/Card';

import Drawer from 'material-ui/Drawer';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import AppBarComponent from '../AppBar/AppBar';
// import Sidebar from '../common/sidebar'
import Tab from '../tab/tab'

import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import * as moment from 'moment'
import * as React from 'react';
import { browserHistory } from 'react-router'


import Footer from '../footer/footer';
import './employeeAttendance.css';


const styles = {
    block: {
        maxWidth: 250,
    },
    radioButton: {
        marginBottom: 16,
    },
};


type Props = Partial<{
    abc: any,
    changeRouteToEditP: any
    dateforNDUProp: any,
    employeeDataProps: any,
    fifthWeekArrayProp: any,
    firstWeekArrayProp: any,
    forthWeekArrayProp: any,
    secondWeekArrayProp: any,
    sixthWeekArrayProp: any,
    statusobjprop: any,
    test: any,
    thirdWeekArrayProp: any,
    paramDate: any,
    valueprop: any,
    AddAttSuccess: any,
    AddAttSuccessText: any,
    ChangeRouteToPayslipComponentFunc: any,
    ChangeRouteToAttendanceComponentFunc: any,
    DecrementMonthPropFunc: any,
    ChRouteToEmpD: any,
    FinalUpdateFunc: any,
    IncrementMonthPropFunc: any,
    ModalCloseFunc: any,
    MonthProp: any,
    NextDisable: any,
    PreviousDisable: any,
    ShowModal: any
    RoleProp: any,
    TabChDash: any,
    UpdateAttendanceBtnDisplay: any,
    UpdateAttendanceCon: any,
    YearProp: any,
}>

interface ISTATE {
    fifthArrayLength: any,
    open: any,
    sixthArrayLength: any,
}


class EmployeeAttendance extends React.Component<Props, ISTATE> {

    constructor(props: any) {
        super(props);
        this.state = {
            fifthArrayLength: this.props.sixthWeekArrayProp.length,
            open: false,
            sixthArrayLength: this.props.fifthWeekArrayProp.length,

        }
    }
    public handleToggle = () => {

        this.setState({ open: true })
    }

    public handleClose = () => this.setState({ open: false });
    public ChangeRouteToDashboard = () => {
        if (window.location.pathname !== 'home') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat
            browserHistory.push({
                pathname: '/home',
                state: obj
            })
        }
    }

    public ChangeRouteToEmployees = () => {
        if (window.location.pathname !== 'employees') {
            const obj = this.props.RoleProp
            delete obj.CurrentMonthFormat

            browserHistory.push({
                pathname: '/employees',
                state: obj
            })
        }
    }
    public ChangeRouteToSettings = () => {
        const CurrentMonthFormat = moment().format("YYYY-MM-DD")
        const AttendanceRouteObject = this.props.RoleProp
        AttendanceRouteObject.CurrentMonthFormat = CurrentMonthFormat
        browserHistory.push({
            pathname: `/salarysheet/${CurrentMonthFormat}`,
            state: AttendanceRouteObject
        })
    }
    public ChangeRouteToAttendance = () => {
        browserHistory.push({
            pathname: '/attendance',
            state: this.props.RoleProp
        })
    }

    public render() {
        return (
            <div>
               
                <AppBarComponent RoleProp={this.props.RoleProp} />
                <div className={`DashboardMainBody ${!this.props.RoleProp ? ('MainBodyPaddingTopNone') : null}`}>
                    <div className={`sidebar attenSBanim ${this.props.ShowModal ? 'modalopenUiSideBar' : null}`}>
                        {
                            this.props.RoleProp ?
                                this.props.RoleProp.role === 'company' ?
                                    (
                                        <ul className='sidebarList'>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToEmployees}><i className="material-icons EmployeeIcon">supervisor_account</i>Employees</li>
                                            <li className='sidebarListFirstTwoLi' onClick={this.ChangeRouteToAttendance}><i className="material-icons EmployeeIcon">schedule</i>Attendance</li>
                                            <li className='sidebarListLastLi' onClick={this.ChangeRouteToSettings}><i className="material-icons SettingsIcon">pie_chart</i>Salary Sheet</li>
                                        </ul>
                                    )
                                    :

                                    this.props.RoleProp.role === 'employee' ?
                                        (
                                            <ul className='sidebarList'>
                                                <li className='sidebarListFirstTwoLi NoBorderBottom' onClick={this.ChangeRouteToDashboard}><i className="material-icons dashboardIcon">dashboard</i>Home</li>
                                            </ul>

                                        ) : null : null

                        }
                    </div>
                    <div className={`sideMainBody attendanceSideBody ${this.props.ShowModal ? 'modalopenUiMainBody' : null}`}>

                        {
                            this.props.employeeDataProps ? (
                                <div className='sideMainBodyChild1'>
                                    <h1 className='DashboardMainHeadND'>
                                        {this.props.employeeDataProps[0].name}
                                    </h1>
                                    <div className='DashDSwitcher'>
                                        <button onClick={this.props.DecrementMonthPropFunc} disabled={this.props.PreviousDisable} className='NoPadddingND'><i className="material-icons">keyboard_arrow_left</i></button>
                                        <span className='salarygraphYear'>{`${this.props.MonthProp} ${this.props.YearProp}`}</span>
                                        <button onClick={this.props.IncrementMonthPropFunc} disabled={this.props.NextDisable} className='NoPadddingND'><i className="material-icons" >keyboard_arrow_right</i></button>
                                       
                                    </div>
                                </div>
                            ) : (
                                    <div className="animated-background-toggleLine EmpDetailLoader">
                                        .
                                        </div>
                                )
                        }


                        <Tab activeProfile={false} activeDash={false} activeAtt={true} activePay={false}
                            AttChPay={null}
                            TabChProfile={this.props.ChRouteToEmpD}
                            TabChPay={this.props.ChangeRouteToPayslipComponentFunc}
                            TabChDash={this.props.TabChDash}
                        />

                        <div className='NOfAPMainDiv'>
                            <div className='NOfAPMainDiv1'>
                                <span>PRESENTS</span>
                                <span>{
                                    this.props.statusobjprop ?
                                        this.props.statusobjprop.totalPresents : 0
                                }</span>
                            </div>

                            <div className='NOfAPMainDiv1'>
                                <span>PAID LEAVES</span>
                                <span>{
                                    this.props.statusobjprop ?
                                        this.props.statusobjprop.totalPaidLeaves : 0
                                }</span>
                            </div>
                            <div className='NOfAPMainDiv2'>
                                <span>LATES</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalLates : 0
                                    }
                                </span>
                            </div>
                            <div className='NOfAPMainDiv3'>
                                <span>ABSENTS</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalAbsents : 0
                                    }
                                </span>
                            </div>
                            <div className='NOfAPMainDiv4'>
                                <span>HALF DAYS</span>
                                <span>
                                    {
                                        this.props.statusobjprop ?
                                            this.props.statusobjprop.totalHalfDays : 0
                                    }
                                </span>
                            </div>
                        </div>
                       

                        <Card className='AttendanceTableCard'>
                            <div className='tablediv'>


                                {
                                    this.props.firstWeekArrayProp.length !== 0 ? (
                                        <table className='AttendanceTable'>
                                            <tr className='DaysNameRow'>
                                                <td className='DNRow'>Sun</td>
                                                <td className='DNRow'>Mon</td>
                                                <td className='DNRow'>Tues</td>
                                                <td className='DNRow'>Wed</td>
                                                <td className='DNRow'>Thurs</td>
                                                <td className='DNRow'>Fri</td>
                                                <td className='DNRow'>Sat</td>

                                            </tr>
                                            <tr>

                                                {
                                                    this.props.firstWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'}  ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    } 
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `} onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 1) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }

                                            </tr>

                                            <tr>

                                                {
                                                    this.props.secondWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'}  
                                                            ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    } 
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `}
                                                                    onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 2) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }

                                            </tr>


                                            <tr>

                                                {
                                                    this.props.thirdWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'}  
                                                            ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    } 
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `}
                                                                    onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 3) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }


                                            </tr>


                                            <tr className={this.props.forthWeekArrayProp.length <= 7 && this.props.fifthWeekArrayProp.length === 0 ? ("LastRow") : ('NoLastRow')}>
                                                {
                                                    this.props.forthWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'} 
                                                            ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    }
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `}
                                                                    onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 4) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }


                                            </tr>


                                            <tr className={this.props.fifthWeekArrayProp.length <= 7 && this.props.sixthWeekArrayProp.length === 0 ? ("LastRow") : ('NoLastRow')}>
                                                {
                                                    this.props.fifthWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'}  
                                                            ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    }
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `}
                                                                    onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 5) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }


                                            </tr>

                                            <tr className={this.props.sixthWeekArrayProp.length !== 0 ? ("LastRow") : ('NoLastRow')}>
                                                {
                                                    this.props.sixthWeekArrayProp.map((value: any, index: any) => {

                                                        if (!value) {
                                                            return (
                                                                <td className='emptyTd' key={index}>.</td>
                                                            )
                                                        }

                                                        else {
                                                            return (
                                                                <td className={`${this.props.RoleProp.role === 'company' ? 'secondRowColAtten' : 'secondRowColAttenDef'}  
                                                            ${
                                                                    value.status === undefined ?
                                                                        this.props.employeeDataProps ?
                                                                            this.props.employeeDataProps[0].status === 'nonDesktimeUser' ?
                                                                                null :
                                                                                'DateWithNoneStatusBackCol' :
                                                                            null : null
                                                                    } 
                                                                    ${
                                                                    value.status === 'present' ? 'tdgreen' : value.status === 'leave' ? 'tdblue' :
                                                                        value.status === 'late' ? 'tdorange' :
                                                                            value.status === 'halfday' ? 'tdpurple' : value.status === 'absent' ? 'tdred' : value.status === 'vacation' ? 'tddarkorange'
                                                                                : value.status === 'unpaidleave' ? 'tdgray'
                                                                                    : value.status === 'paidleave' ? 'tdlightgreen'
                                                                                        : null
                                                                    }
                                                                    `}
                                                                    onClick={this.props.RoleProp.role === 'company' ? this.props.abc.bind(this, value, index, 6) : null} key={index}>
                                                                    <span className='mobileDaysName'>{index === 0 ? 'Sun' : index === 1 ? 'Mon' : index === 2 ? 'Tues' : index === 3 ? 'Wed' : index === 4 ? 'Thurs' : index === 5 ? 'Fri' : index === 6 ? 'Sat' : null}</span>

                                                                    {
                                                                        value.status === 'present' ? (

                                                                            <span className='green'>Present</span>
                                                                        ) : value.status === 'leave' ? (

                                                                            <span className='blue'>Leave</span>
                                                                        ) :
                                                                                value.status === 'late' ? (

                                                                                    <span className='orange'>Late</span>
                                                                                ) :
                                                                                    value.status === 'halfday' ? (

                                                                                        <span className='purple'>Half Day</span>
                                                                                    ) : value.status === 'absent' ? (

                                                                                        <span className='red'>Absent</span>
                                                                                    ) : value.status === 'vacation' ? (

                                                                                        <span className='darkorange'>Vacation</span>
                                                                                    )
                                                                                                : value.status === 'unpaidleave' ? (

                                                                                                    <span className='gray'>U Leave</span>
                                                                                                )
                                                                                                    : value.status === 'paidleave' ? (

                                                                                                        <span className='lightgreen'>P Leave</span>
                                                                                                    )
                                                                                                        : null
                                                                    }

                                                                    <span className={`DateWithOneStatus ${value.status === undefined ? "DateWithNoneStatus" : null} `}>{value.date ? (moment(value.date).date()) : (value)}</span>
                                                                </td>
                                                            )
                                                        }
                                                    })
                                                }


                                            </tr>

                                        </table>


                                    )
                                        :

                                        (
                                            <div className='AttendanceLoader'>
                                                <div className='AttendanceLoaderChild1'>
                                                    <div className="AttendanceLoaderTable attendanceloader1">
                                                        .
                                                    </div>
                                                </div>
                                                <div className='AttendanceLoaderChild2'>
                                                    <div className="AttendanceLoaderTable attendanceloader2">
                                                        .
                                                    </div>

                                                </div>
                                                <div className='AttendanceLoaderChild3'>
                                                    <div className="AttendanceLoaderTable attendanceloader3">
                                                        .
                                                    </div>

                                                </div>

                                                <div className='AttendanceLoaderChild3'>
                                                    <div className="AttendanceLoaderTable attendanceloader3">
                                                        .
                                                    </div>

                                                </div>

                                                <div className='AttendanceLoaderChild3 lastLoadermarginBottom'>
                                                    <div className="AttendanceLoaderTable attendanceloader3">
                                                        .
                                                    </div>

                                                </div>

                                            </div>
                                        )


                                }

                            </div>












                        </Card>


                    </div>
                    <div className={`${this.props.ShowModal ? 'MatDrawer' : null}`}>

                        <Drawer
                            docked={false}

                            open={this.props.ShowModal}
                            onRequestChange={this.props.ModalCloseFunc}
                            openSecondary={true}
                        >

                            {
                                this.props.ShowModal ? (
                                    <div>
                                        <p className='modalHeading'>{this.props.valueprop ? this.props.valueprop.status ? 'Update Attendance' : 'Add Attendance' : null}</p>
                                        <span className='UAttDate'>
                                            {
                                                typeof (this.props.dateforNDUProp) === 'number' ?
                                                    (moment(this.props.paramDate).date(this.props.dateforNDUProp).format("DD MMMM YYYY"))
                                                    :
                                                    this.props.valueprop ?
                                                        (moment(this.props.valueprop.date).format("DD MMMM YYYY"))
                                                        : null
                                            }
                                        </span>

                                        <RadioButtonGroup name="shipSpeed" defaultSelected="not_light" onChange={this.props.UpdateAttendanceCon}>

                                            <RadioButton
                                                value="absent"
                                                label="Absent"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'absent' ? true : false : false}
                                                className='testRad'
                                            />

                                            <RadioButton
                                                value="present"
                                                label="Present"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'present' ? true : false : false}
                                                className='testRad'

                                            />

                                            <RadioButton
                                                value="late"
                                                label="Late"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'late' ? true : false : false}
                                                className='testRad'

                                            />


                                            <RadioButton
                                                value="halfday"
                                                label="Half Day"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'halfday' ? true : false : false}
                                                className='testRad'

                                            />

                                            <RadioButton
                                                value="paidleave"
                                                label="Paid Leave"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'paidleave' ? true : false : false}
                                                className='testRad'
                                            />

                                            <RadioButton
                                                value="unpaidleave"
                                                label="Unpaid Leave"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'unpaidleave' ? true : false : false}
                                                className='testRad'

                                            />

                                            <RadioButton
                                                value="vacation"
                                                label="Vacation"
                                                style={styles.radioButton}
                                                disabled={this.props.valueprop ? this.props.valueprop.status === 'vacation' ? true : false : false}
                                                className='testRad lastVaca'

                                            />
                                        </RadioButtonGroup>
                                        <RaisedButton label="UPDATE" fullWidth={true} backgroundColor='#008141' className='UPAttBtnND'
                                            onClick={this.props.FinalUpdateFunc}
                                            disabled={this.props.UpdateAttendanceBtnDisplay}

                                            style={{ color: 'white' }} type="submit" />

                                    </div>
                                ) : null
                            }


                        </Drawer>

                    </div>

                </div>
                <Snackbar
                    open={this.props.AddAttSuccess}
                    message={this.props.AddAttSuccessText}
                    contentStyle={{ textAlign: 'center' }}
                    autoHideDuration={2000}

                />
                <Footer FooterClass='ResgisterFooter' />
            </div >

        );
    }
}

export default EmployeeAttendance;
