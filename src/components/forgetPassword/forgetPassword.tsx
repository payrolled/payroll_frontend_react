import { Card } from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router'

import * as React from 'react';
import Footer from '../footer/footer'
import './forgetPassword.css';

type Props = Partial<{
    errordisplayProp: any,
    errorVisibilityProp: any,
    ForgetpassChan: any,
    ForgetpassFunc: any,
    LoaderDisplayProp: any,
    RegisterDisable: any
}>
class ForgetPassword extends React.Component<Props>{
    public render() {
        return (
            <div className='RegisterMainDiv backdiv'>
                <div className='RegisterMainDivChild1'>
                    {
                        (
                            this.props.LoaderDisplayProp === 'unset'
                        ) ?
                            (
                                <div>
                                    <LinearProgress mode="indeterminate" style={{ visibility: 'unset', top: '4px' }} color={'#008141'} />
                                </div>
                            )
                            : (
                                <LinearProgress mode="indeterminate" style={{ visibility: 'hidden', top: '4px' }} color={'#008141'} />

                            )
                    }                    <Card className='CardMainDiv'>
                        <form>

                            <div className='PayRollLogoDiv'>
                                <img src={require('../../images/payrolllogoLogin.png')} />
                            </div>

                            <div className='TextFieldDivs'>
                                <i className="material-icons TextFieldIcon" >person_outline</i>
                                <input type='text' placeholder='Enter Your Email Address' className='ResgisterTextFieldsStyle' onChange={this.props.ForgetpassChan} />
                            </div>
                            <div className='ForGetPassWordText'>
                                <p>Enter your email address to received a<br /> link to reset your password.</p>
                            </div>
                            <div className='smalldiv smallcolor loginErrorDiv forgetpassworderr' style={{ visibility: this.props.errorVisibilityProp }}>
                                <small>{this.props.errordisplayProp}</small>
                            </div>
                            <div className='TextFieldDivsRaisedButton'>
                                <RaisedButton label="SEND" fullWidth={true} backgroundColor='#008141' className='ResgisterTextFieldsStyleLastBtnRaisedButton registerbuttonstyle btnFontColor' onClick={this.props.ForgetpassFunc} disabled={this.props.RegisterDisable} type="submit"/>
                            </div>

                            <div className='LinkParentDiv'>
                                <Link to='/' className='LinkClassForgetPassword'><i className="material-icons">arrow_back</i> Back to Login</Link>
                            </div>
                        </form>

                    </Card>

                </div>

                <Footer FooterClass='LoginRegiterFooter' />

            </div>
        );
    }
}

export default ForgetPassword;
