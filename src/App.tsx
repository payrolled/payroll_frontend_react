
import * as React from 'react';
import './App.css';
import Routes from './Routes/Routes'


class App extends React.Component {
  public render() {
    return (
      <Routes />
    );
  }
}

export default App;
