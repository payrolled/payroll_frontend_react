import axios from 'axios'
import { MuiThemeProvider } from "material-ui/styles";
import { black } from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import * as React from 'react';
import { browserHistory, Route, Router } from 'react-router'
import { Debugger } from 'ts-debug';
import ErrorScreen from '../components/404/errorScreen'
import AccountSettingCon from '../containers/accountsettings/accountsettings'
import AddEmployeeContainer from '../containers/AddEmployee/AddEmployee'
import ApiKeyScreenContainer from '../containers/ApiKeyScreen/ApiKeyScreen'
import ArchivesCon from '../containers/archives/archives'
import AttendanceCon from '../containers/attendance/attendance'
import DashboardContainer from '../containers/dashboard/dashboard'
import EmployeeDetailContainer from '../containers/emloyeeDetails/employeeDetails'
import EmployeeAttendanceContainer from '../containers/employeeAttendance/employeeAttendance'
import EmployeeDashboardCon from '../containers/employeedashboard/employeedashbaord'
import EmployeePayslipContainer from '../containers/employeePayslip/employeePayslip'
import EmployeesViewContainer from '../containers/employeesView/employeesView'
import ForgetPassword from '../containers/forgetPassword/forgetPassword'
import InviteCon from '../containers/invite/invite'
import LoginContainer from '../containers/Login/Login'
import Register from '../containers/register/register'
import SetPasswordContainer from '../containers/setPassword/setPassword'
import SetPasswordSuccessContainer from '../containers/setPasswordSuccess/setPasswordSuccess'
import SettingsContainer from '../containers/settings/settings'
import TimerToolContainer from '../containers/timerTool/timerTool'
import UpdateProfileCon from '../containers/updateprofile/updateprofile'
import RouteConfig from '../routeconfig'

const Config = { isProd: false };
const debug = new Debugger(console, !Config.isProd, '[DEBUG] ');
const MyProfileAPI: any = axios.create({ baseURL: `${RouteConfig}/api/users/` });

MyProfileAPI.interceptors.request.use((config: any) => {
    const token = localStorage.getItem("user");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})

const lightMuiTheme = getMuiTheme(
    {
        datePicker: {
            calendarTextColor: black,
            calendarYearBackgroundColor: 'white',
            color: black,
            headerColor: 'white',
            selectColor: '#008141',
            selectTextColor: black,
            textColor: black,
        },
        flatButton: {
            primaryTextColor: black,
        },
        timePicker: {
            accentColor: '#008141',
            color: black,
            headerColor: 'white',
            selectColor: '#008141',
            selectTextColor: black,
            textColor: black,
        },

    }
);

function requireAuth2(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
            state: 'test'
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {
            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()

}

function ServiceApiKeyLoginAuth(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        localStorage.removeItem('user')
        sessionStorage.clear()
        browserHistory.replace('/')
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else {
                if (!success.data.service) {

                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && success.data.apikey) {
                    const user = {
                        name: success.data.body.name,
                        role: success.data.role,
                    }
                    browserHistory.replace({
                        pathname: '/home',
                        state: user
                    })
                }
                else {
                    return callback()
                }
                debug.log(success.data, 'aklsdj')

            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
}

function ApiKeyLoginAuth(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        localStorage.removeItem('user')
        sessionStorage.clear()
        browserHistory.replace('/')
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else {
                if (success.data.service) {
                    if (success.data.apikey) {
                        const user = {
                            name: success.data.body.name,
                            role: success.data.role,
                        }
                        browserHistory.replace({
                            pathname: '/home',
                            state: user
                        })
                    }
                    else {
                        return callback()
                    }
                }
                else {
                    return callback()
                }
                debug.log(success.data, 'aklsdj')

            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
}

function LoginAuth(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        return callback()
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({
                    pathname: '/home',
                    state: user
                })
            }
            else {
                if (success.data.service) {
                    if (success.data.apikey) {
                        const user = {
                            name: success.data.body.name,
                            role: success.data.role,
                        }
                        browserHistory.replace({

                            pathname: '/home',
                            state: user
                        })
                    }
                    else {
                        browserHistory.push({
                            pathname: '/service/api_key',
                        })
                    }
                }
                else {
                    browserHistory.push({
                        pathname: '/service',
                    })
                }
                debug.log(success.data, 'aklsdj')

            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
}
function requireAuth3(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })
                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()

}

function requireAuth4(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }

        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()
}

function requireAuth5(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()
}

function requireAuth(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    MyProfileAPI.get('me').then((success: any) => {

        if (success.data.role === 'company') {
            if (!success.data.service) {
                browserHistory.replace({
                    pathname: '/service',
                })

            }
            else if (success.data.service && !success.data.apikey) {
                browserHistory.replace({
                    pathname: '/service/api_key',
                })
            }
            else {
                return callback()
            }
        }


    }).catch((err: any) => {
        if (err.response) {

            if (err.response.status === 401) {
                localStorage.removeItem('user')
                sessionStorage.clear()
                browserHistory.replace('/')
            }
        }
    })
    return callback()
}

function requireAuth6(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()
}

function requireAuthForEmp(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {
            if (success.data.role === 'company') {
                debug.log(success, 'asd')
                const user = {
                    name: success.data.body.name,
                    role: success.data.role,
                }
                browserHistory.replace({
                    pathname: '/home',
                    state: user
                })
            }
            else {
                return callback()
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()
}

function requireAuth7(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        MyProfileAPI.get('me').then((success: any) => {

            if (success.data.role === 'employee') {
                const user = {
                    imageUrl: success.data.user.imageUrl ? success.data.user.imageUrl : null,
                    name: success.data.user.name,
                    role: success.data.role,
                }
                browserHistory.replace({

                    pathname: '/home',
                    state: user
                })
            }
            else if (success.data.role === 'company') {
                if (!success.data.service) {
                    browserHistory.replace({
                        pathname: '/service',
                    })

                }
                else if (success.data.service && !success.data.apikey) {
                    browserHistory.replace({
                        pathname: '/service/api_key',
                    })
                }
                else {
                    return callback()
                }
            }


        }).catch((err: any) => {
            if (err.response) {

                if (err.response.status === 401) {
                    localStorage.removeItem('user')
                    sessionStorage.clear()
                    browserHistory.replace('/')
                }
            }
        })
    }
    return callback()
}

function AccSettRequireAuth(nextState: any, replace: any, callback: any) {
    const token = localStorage.getItem('user')
    if (!token) {
        replace({
            pathname: '/',
        })
    }
    else {
        return callback()
    }
    return callback()

}

class Routes extends React.Component {
    public render() {
        return (
            <MuiThemeProvider muiTheme={lightMuiTheme}>
                <Router history={browserHistory} >
                    <Route path="/" component={LoginContainer} onEnter={LoginAuth} />
                    <Route path="/register" component={Register} onEnter={LoginAuth} />
                    <Route path="/service" component={TimerToolContainer} onEnter={ApiKeyLoginAuth} />
                    <Route path="/service/api_key" component={ApiKeyScreenContainer} onEnter={ServiceApiKeyLoginAuth} />
                    <Route path="/resetpassword" component={ForgetPassword} onEnter={LoginAuth} />
                    <Route path="/home" component={DashboardContainer}
                        onEnter={requireAuth}
                    />
                    <Route path="/employees" component={EmployeesViewContainer}
                        onEnter={requireAuth2}
                    />
                    <Route path="/employees/:employeeId/attendance/:date" component={EmployeeAttendanceContainer} onEnter={requireAuth5} />
                    <Route path="/archives" component={ArchivesCon} onEnter={requireAuth5} />
                    <Route path="/attendance/:date" component={EmployeeAttendanceContainer} onEnter={requireAuthForEmp} />
                    <Route path="/employees/:employeeId/payslip" component={EmployeePayslipContainer} onEnter={requireAuth4} />
                    <Route path="/employees/:employeeId/dashboard" component={EmployeeDashboardCon} onEnter={requireAuth4} />
                    <Route path="/employees/:employeeId/editprofile" component={UpdateProfileCon} onEnter={requireAuth4} />
                    <Route path="/employees/:employeeId/editprofile/:date" component={UpdateProfileCon} onEnter={requireAuth4} />
                    <Route path="/AddEmployee" component={AddEmployeeContainer} onEnter={requireAuth3} />
                    <Route path="/employees/:employeeId" component={EmployeeDetailContainer}
                        onEnter={requireAuth3}
                    />
                    <Route path="/setpassword" component={SetPasswordContainer} />
                    <Route path="/forgetpassword" component={SetPasswordContainer} />
                    <Route path="/success" component={SetPasswordSuccessContainer} />
                    <Route path="/salarysheet/:date" component={SettingsContainer} onEnter={requireAuth6} />
                    <Route path="/attendance" component={AttendanceCon} onEnter={requireAuth7} />
                    <Route path="/invite" component={InviteCon} onEnter={requireAuth2} />
                    <Route path="/payslip" component={EmployeePayslipContainer} onEnter={requireAuthForEmp} />
                    <Route path="/profile" component={EmployeeDetailContainer} onEnter={requireAuthForEmp} />
                    <Route path="/accountsettings" component={AccountSettingCon} onEnter={AccSettRequireAuth} />
                    <Route component={ErrorScreen} path="*" />
                </Router >
            </MuiThemeProvider>

        );
    }
}

export default Routes;
